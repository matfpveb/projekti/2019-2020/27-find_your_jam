const http = require('http');
const app = require('./app');
const server = http.createServer(app);
const io = require('socket.io').listen(server, {origins:'localhost:* http://localhost:* http://www.localhost:*'});
const port = 3003;


io.sockets.on('connection', (socket) => {
  //kada dobije signal da je postavljen novi komentar
  //emituje signal svima da bi ucitali listu komentara ponovo kako bi videli i novi komentar
  socket.on('addCom', () => {
    io.sockets.emit('newCom');
  });

  //kada dobije signal da je neko ocenio pesmu
  //emituje signal svima da bi izracunali novu prosecnu ocenu
  socket.on('addRate', () => {
    io.sockets.emit('newScore');
  });

  //brisanje istorije
  let socketId = socket.id;
  socket.on('delete', () => {
    io.to(socketId).emit('empty');
  });

});


server.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});