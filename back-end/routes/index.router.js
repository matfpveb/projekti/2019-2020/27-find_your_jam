const express = require('express');
const router = express.Router();

const ctrlUser = require('../controllers/user.controller');
const ctrlAlbum = require('../controllers/album.controller');

router.post('/register', ctrlUser.register);

router.post('/login', ctrlUser.login);

router.post('/change/email', ctrlUser.changeEmail);
router.post('/change/password', ctrlUser.changePassword);
router.post('/change/username', ctrlUser.changeUsername);

router.post('/makeList', ctrlUser.makeList);
router.get('/getLists/:user', ctrlUser.getLists);
router.get('/getHearted/:user', ctrlUser.getHearted);
router.patch('/addSongToList', ctrlUser.addSongToList);
router.patch('/changeListPrivateField', ctrlUser.changeListPrivateField);
router.get('/users', ctrlUser.getUsers);
router.patch('/deleteList', ctrlUser.deleteList);

router.get('/user/:username',ctrlUser.getUserByUsername);
router.get('/user/listHistory/:username', ctrlUser.getHistory);
router.patch('/user/history', ctrlUser.addToHistory);
router.patch('/user/removeHistory', ctrlUser.removeHistory);
router.patch('/user/removeSongFromList', ctrlUser.removeFromList);
router.patch('/user/addToHearted', ctrlUser.addToHearted);
router.patch('/user/removeFromHearted', ctrlUser.removeFromHearted);

router.get('/album/:id', ctrlAlbum.aboutAlbum);
router.patch('/album/addComment', ctrlAlbum.addComment);
router.patch('/album/addRating', ctrlAlbum.addRating);

router.patch('/user/liked', ctrlUser.addToLiked);
router.get('/user/likedHistory/:username', ctrlUser.getLiked);


router.delete('/deleteUser/:user', ctrlUser.deleteUser);

module.exports = router;



