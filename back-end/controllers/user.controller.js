const User = require('../models/user.model');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const { use } = require('../routes/index.router');


module.exports.register = async (req, res) => {

    const userEmail = await User.findOne({email: req.body.email});
    if(userEmail){
        console.log('Email vec postoji!');
        return res.status(400).send('Email already exists');
    }
    const userUsername = await  User.findOne({username: req.body.username});
    if(userUsername)
        return res.status(400).send('Username already exists');
    
    
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    
    const user = new User({
        _id: mongoose.Types.ObjectId(),
        username: req.body.username,
        email: req.body.email,
        password: hashedPassword
    });

    
    user.save((err, doc) => {
        if (!err)
            res.send(doc);
        }
    ); 
}

module.exports.login = async (req, res) => {


    const user = await User.findOne({username: req.body.username});
    
    if(!user){
        console.log('Username nije tacan!');
        return res.status(400).send('Incorrect username');
    }
    
    const validPass = await bcrypt.compare(req.body.password, user.password);

    if(!validPass){
        return res.status(400).send('Password is not correct');
    }
    
    return res.status(200).send();  
}


module.exports.changeEmail = async (req, res) => {


    const user = await User.findOne({username: req.body.user});
    if(!user){
        //console.log('Ime nije tacno!');
        return res.status(400).send('Name is not correct'); 
    }

    const validPass = await bcrypt.compare(req.body.password, user.password);
    if(!validPass){
        return res.status(400).send('Password is not correct'); 
    }

    const userEmail = await User.findOne({email: req.body.newEmail});
    if(userEmail){
        //console.log('Email vec postoji!');
        return res.status(400).send('Email already exists');
    }
   
    user.email = req.body.newEmail;
    await user.save((err, doc) => {
        if (!err)
            res.send(doc);
        else
            console.log('greska');
        }
    ); 

      
}


module.exports.changePassword = async (req, res) => {


    const user = await User.findOne({username: req.body.user});
    if(!user){
        //console.log('Ime nije tacno!');
        return res.status(400).send('Name is not correct'); 
    }

    const validPass = await bcrypt.compare(req.body.password, user.password);
    if(!validPass){
        return res.status(400).send('Password is not correct'); 
    }

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.newPassword, salt);
 
    user.password = hashedPassword;
    await user.save((err, doc) => {
        if (!err)
            res.send(doc);
        else
            console.log('greska');
        }
    );
}

module.exports.changeUsername = async (req, res) => {


    const newUsername = await User.findOne({username: req.body.newUsername});
    if(newUsername){
        //console.log('Username postoji!');
        return res.status(400).send('Username already exists');
    }

    const user = await User.findOne({username: req.body.user});
    if(!user){
        //console.log('Ime nije tacno!');
        return res.status(400).send('Name is not correct');
    }

    const validPass = await bcrypt.compare(req.body.password, user.password);
    if(!validPass){
        return res.status(400).send('Password is not correct'); 
    }

    user.username = req.body.newUsername;
    await user.save((err, doc) => {
        if (!err)
            res.send(doc);
        else
            console.log('greska');
        }
    );

}


module.exports.removeFromList = async function (req, res, next){
    try{
        
        const user = await User.findOne({username:req.body.user}).exec();
        if(!user){
            return req.status(404).json({message: 'not found'});
        }
        
        const list = req.body.list;
        const id = req.body.id;

        user.lists.forEach(e=>{
            if(e.name==list){
                e.songs = e.songs.filter(s=>s!=id);
            }
        });

        const newList = user.save();
        return res.status(201).json(newList);

    }catch(err){
        next();
    }

}


module.exports.getUserByUsername=async function(req,res,next){
    try{
        const username=req.params.username;
        const user=await User.findOne({username:username}).exec();
        if(!user){
            return res.status(404).json({message:"Not found"});
        }
        res.status(201).json(user)

    }catch(err){
        next();
    }
}


module.exports.addToHistory = async function (req, res, next){
    try{
        const user = await User.findOne({username:req.body.user}).exec();
        if(!user){
            return req.status(404).json({message: 'not found'});
        }

        user.history.push(req.body.id);
        const newHis = user.save();
        return res.status(201).json(newHis);

    }catch(err){
        next();
    }
}

module.exports.getHistory=async function(req,res,next){
    try{
        const user=await User.findOne({username:req.params.username}).exec();
        if(!user){
            return res.status(404).json({message:"Not found"});
        }
        res.status(201).json(user.history)

    }catch(err){
        next();
    }
}

module.exports.removeHistory = async function(req, res, next){
    try{
        const user = await User.findOne({username:req.body.user}).exec();
        if(!user){
            return req.status(404).json({message: 'not found'});
        }

        user.history=[];
        const newHis = user.save();
        return res.status(201).json(newHis);

    }catch(err){
        next();
    }
}

module.exports.addToLiked = async function (req, res, next){
    try{
        const user = await User.findOne({username:req.body.user}).exec();
        if(!user){
            return req.status(404).json({message: 'not found'});
        }

        user.liked.push(req.body.id);
        const newLiked = user.save();
        return res.status(201).json(newLiked);

    }catch(err){
        next();
    }
}

module.exports.getLiked=async function(req,res,next){
    try{
        const user=await User.findOne({username:req.params.username}).exec();
        if(!user){
            return res.status(404).json({message:"Not found"});
        }
        res.status(201).json(user.liked)

    }catch(err){
        next();
    }
}

module.exports.makeList = async function(req, res, next){
    try{
        const user=await User.findOne({username:req.body.user}).exec();
        if(!user){
            return res.status(404).json({message:"Not found"});
        }
        
        const list = {name: req.body.name, songs: []};
        user.lists.push(list);
        const newList = user.save();
        return res.status(201).json(user.lists);

    }catch(err){
        next();
    }

}
module.exports.getLists = async function(req, res, next){
    try{
        const user=await User.findOne({username:req.params.user}).exec();
        if(!user){
            return res.status(404).json({message:"Not found"});
        }
        return res.status(201).json(user.lists);

    }catch(err){
        next();
    }

}
module.exports.addSongToList = async function (req, res, next){
    
    try{
        const user = await User.findOne({username:req.body.user}).exec();
        if(!user){
            return req.status(404).json({message: 'not found'});
        }

        const list = req.body.list;
        
        user.lists.forEach((e)=>{
            if(e.name==list)
                e.songs.push(req.body.id);
        });

        const newSong = user.save();
        return res.status(201).json(newSong);

    }catch(err){
        next();
    }
}

module.exports.changeListPrivateField = async function (req, res, next){
    
    try{
        const user = await User.findOne({username:req.body.user}).exec();
        if(!user){
            return req.status(404).json({message: 'not found'});
        }

        const list = req.body.list;
        
        user.lists.forEach((e)=>{
            if(e.name==list)
                if(e.private==false)
                    e.private=true;
                else
                    e.private=false;
        });

        const newPr = user.save();
        return res.status(201).json(newPr);

    }catch(err){
        next();
    }
}

module.exports.getUsers = async function (req, res, next){
    
    try{
        const users = await User.find().exec();
        if(!users){
            return req.status(404).json({message: 'not found'});
        }

        return res.status(201).json(users);

    }catch(err){
        next();
    }
}

module.exports.deleteList = async function (req, res, next){
    
    try{
        const user = await User.findOne({username:req.body.user}).exec();
        if(!user){
            return req.status(404).json({message: 'not found'});
        }

        const list = req.body.list;
        user.lists = user.lists.filter(e => e.name!=list);

        const newLists = user.save();
        return res.status(201).json(newLists);

    }catch(err){
        next();
    }
}

module.exports.deleteUser = async (req, res, next) => {
    try {
      const user = await User.find({username:req.params.user}).exec();
       
      if (user) {
        await User.findOneAndDelete({username:req.params.user}).exec();
        res.status(200).send();
      } else {
        res.status(404).send();
      }
    } catch (err) {
      next(err);
    }
  };

  module.exports.addToHearted = async function (req, res, next){
    try{
        const user = await User.findOne({username:req.body.user}).exec();
        
        if(!user){
            return req.status(404).json({message: 'not found'});
        }
        const list = req.body.list.list;

        user.hearted.push(list);
        
        const newHearted = user.save();
        return res.status(201).json(newHearted);

    }catch(err){
        next();
    }
}
module.exports.removeFromHearted = async function (req, res, next){
    
    try{
        const user = await User.findOne({username:req.body.user}).exec();
        if(!user){
            return req.status(404).json({message: 'not found'});
        }
        const list = req.body.list;
        user.hearted = user.hearted.filter(e=>e.name!=list);
        const newHearted = user.save();
        return res.status(201).json(newHearted);

    }catch(err){
        next();
    }
}
module.exports.getHearted = async function(req, res, next){
    try{
        const user=await User.findOne({username:req.params.user}).exec();
        if(!user){
            return res.status(404).json({message:"Not found"});
        }
        return res.status(201).json(user.hearted);

    }catch(err){
        next();
    }

}