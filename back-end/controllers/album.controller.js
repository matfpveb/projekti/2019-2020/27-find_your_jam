const mongoose = require('mongoose');
const Album = require('../models/album.model');
const io = require('../server');

module.exports.aboutAlbum = async function (req, res, next){

    const id = req.params.id;
    try{
        const album = await Album.findById(id).exec();
        
        if(!album){
            return res.status(200).json(null);
        }
        
        return res.status(200).json(album);
    }catch(err){
        next();
    }
}

module.exports.addComment = async function (req, res, next){
    
    try{
        const album = await Album.findById(req.body.id).exec();
        if(!album){
            const newAlb = new Album({
                _id: req.body.id,
                comments: [{
                    user: req.body.user,
                    comment: req.body.comment
                }]
            });
            
            const saved = newAlb.save();
            return res.status(201).json({message: 'added'});
        }
        
        const addedComment = {
            user: req.body.user,
            comment: req.body.comment
        };

        album.comments.push(addedComment);
        const newcom = album.save();
        return res.status(201).json(newcom);

    }catch(err){
        next();
    }

}

module.exports.addRating = async function (req, res, next){
    
    try{
        const album = await Album.findById(req.body.id).exec();
        if(!album){
            const newAlb = new Album({
                _id: req.body.id,
                ratings: [req.body.stars]
            });
            
            const saved = newAlb.save();
            return res.status(201).json({message: 'added'});
        }else{
        
        const stars = req.body.stars;

        album.ratings.push(stars);
        const newcom = album.save();
        return res.status(201).json(newcom);
        }
    }catch(err){
        next();
    }

}