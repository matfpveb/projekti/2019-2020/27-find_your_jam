const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

var userSchema = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
      },
    password: {
        type: String,
        required: true
    },
    lists: [{
        name: {
            type: String
        },
        songs: {
            type: Array
        },
        private: {
            type: Boolean,
            default: false
        }
    }],
    history: {
        type:Array
    },
    liked: {
        type:Array
    },
    hearted: [{
        name: {
            type: String
        },
        songs: {
            type: Array
        }
    }],
    saltSecret: String
});


// userSchema.pre('save', function (next) {
//     bcrypt.genSalt(10, (err, salt) => {
//         bcrypt.hash(this.password, salt, (err, hash) => {
//             this.password = hash;
//             this.saltSecret = salt;
//             next();
//         });
//     });
// });



module.exports = mongoose.model('User', userSchema);