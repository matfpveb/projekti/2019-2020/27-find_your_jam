const mongoose = require('mongoose');

var albumSchema =  new mongoose.Schema({
    _id: String,
    ratings: {
        type: Array,
        default: []
    },
    comments: [{
        comment: {
            type: String,
            default: ""
        },
        user: {
            type: String,
            required: true
        }
    }]
});

module.exports = mongoose.model('Album', albumSchema);