function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./src/app/album/album.component.ts":
  /*!******************************************!*\
    !*** ./src/app/album/album.component.ts ***!
    \******************************************/

  /*! exports provided: AlbumComponent */

  /***/
  function srcAppAlbumAlbumComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AlbumComponent", function () {
      return AlbumComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var socket_io_client__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! socket.io-client */
    "./node_modules/socket.io-client/lib/index.js");
    /* harmony import */


    var socket_io_client__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */


    var _service_deezer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../service/deezer.service */
    "./src/app/service/deezer.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _service_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../service/user.service */
    "./src/app/service/user.service.ts");
    /* harmony import */


    var _service_album_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../service/album.service */
    "./src/app/service/album.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function AlbumComponent_p_25_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Loading ...");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function AlbumComponent_div_26_div_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", ctx_r17.album.cover_medium, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
      }
    }

    function AlbumComponent_div_26_div_26_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Rating: ", ctx_r18.score, "");
      }
    }

    function AlbumComponent_div_26_div_50_Template(rf, ctx) {
      if (rf & 1) {
        var _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Preview Track");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AlbumComponent_div_26_div_50_Template_button_click_9_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r22);

          var track_r20 = ctx.$implicit;

          var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r21.addToList(track_r20.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Add to list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var track_r20 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", track_r20.track_position, ". ", track_r20.title, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("href", track_r20.preview, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
      }
    }

    function AlbumComponent_div_26_Template(rf, ctx) {
      if (rf & 1) {
        var _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "header", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, AlbumComponent_div_26_div_4_Template, 2, 1, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h4");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "h2", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "span", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "span", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "span", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "span", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, AlbumComponent_div_26_div_26_Template, 3, 1, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "h2", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Post your review");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "fieldset", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function AlbumComponent_div_26_Template_fieldset_change_29_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r24);

          var ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r23.rate($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "input", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "label", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "input", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "label", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "input", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "label", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "input", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "label", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "input", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "label", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "a", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, " View Album in Deezer");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "h2", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Album Track Listings:");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](50, AlbumComponent_div_26_div_50_Template, 12, 3, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r15.album.cover_medium.length > 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r15.artist == null ? null : ctx_r15.artist.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Album name: ", ctx_r15.album == null ? null : ctx_r15.album.title, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r15.album == null ? null : ctx_r15.album.nb_tracks, " track album");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r15.album == null ? null : ctx_r15.album.fans, " Fans");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Release Date: ", ctx_r15.album == null ? null : ctx_r15.album.release_date, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Duration: ", ctx_r15.album == null ? null : ctx_r15.album.duration, " sec");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Record Label: ", ctx_r15.album == null ? null : ctx_r15.album.label, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r15.alb !== null);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("href", ctx_r15.album.link, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r15.tracks);
      }
    }

    function AlbumComponent_div_29_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h4");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var obj_r26 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", obj_r26.user, ": ", obj_r26.comment, "");
      }
    }

    function AlbumComponent_div_29_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AlbumComponent_div_29_div_1_Template, 4, 2, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r16.hack(ctx_r16.alb.comments));
      }
    }

    var _c0 = function _c0() {
      return ["", "search"];
    };

    var _c1 = function _c1() {
      return ["/"];
    };

    var _c2 = function _c2() {
      return ["/change/username"];
    };

    var _c3 = function _c3() {
      return ["/change/password"];
    };

    var _c4 = function _c4() {
      return ["/change/email"];
    };

    var _c5 = function _c5() {
      return ["/myList"];
    }; //Prikaz albuma


    var AlbumComponent = /*#__PURE__*/function () {
      function AlbumComponent(_deezerApi, _route, userService, albumService, builder) {
        var _this = this;

        _classCallCheck(this, AlbumComponent);

        this._deezerApi = _deezerApi;
        this._route = _route;
        this.userService = userService;
        this.albumService = albumService;
        this.builder = builder;
        this.tracks = [];
        this.objectkeys = Object.keys;
        this.alb = null;
        this.rating = 0;
        this.user = userService.getUser();

        var sub = this._route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (param) {
          return param.get('id');
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (pId) {
          return _this.albumService.showComments(pId);
        })).subscribe(function (alb) {
          _this.alb = alb;

          if (_this.alb !== null) {
            _this.alb.ratings.forEach(function (element) {
              _this.rating = _this.rating + element;
            });

            _this.score = +(Math.round(_this.rating / _this.alb.ratings.length * 100) / 100).toFixed(2);
          }
        });

        this.forma = this.builder.group({
          comment: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100)]]
        });
      }

      _createClass(AlbumComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this2 = this;

          this._route.params.map(function (params) {
            return params['id'];
          }).subscribe(function (id) {
            _this2.loader = true;

            _this2._deezerApi.getArtist(id).subscribe(function (artist) {
              _this2.artist = artist;
              _this2.loader = false;
            });

            _this2._deezerApi.getTracks(id).subscribe(function (tracks) {
              _this2.tracks = tracks;
              _this2.loader = false;
            });

            _this2._deezerApi.getAlbum(id).subscribe(function (album) {
              _this2.album = album;
              _this2.loader = false;
            }, function (error) {
              _this2.loader = false;
              console.error(error);
            });
          });

          this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_3__();
          this.socket.on('newCom', function () {
            _this2.getCom();
          });
        }
      }, {
        key: "hack",
        value: function hack(val) {
          return Array.from(val);
        }
      }, {
        key: "rate",
        value: function rate(event) {
          var _this3 = this;

          this.stars = +document.querySelector('input[name="rating"]:checked').value;

          var sub = this._route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (param) {
            return param.get('id');
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (pId) {
            return _this3.albumService.addRating(pId, _this3.stars);
          })).subscribe(function (alb4) {
            return _this3.alb4 = alb4;
          });

          var sub2 = this._route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (param) {
            return param.get('id');
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (pId) {
            return _this3.albumService.showComments(pId);
          })).subscribe(function (alb) {
            _this3.alb = alb;
            _this3.rating = 0;

            if (_this3.alb !== null) {
              _this3.alb.ratings.forEach(function (element) {
                _this3.rating = _this3.rating + element;
              });

              _this3.score = +(Math.round(_this3.rating / _this3.alb.ratings.length * 100) / 100).toFixed(2);
            }
          });
        }
      }, {
        key: "getCom",
        value: function getCom() {
          var _this4 = this;

          var sub = this._route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (param) {
            return param.get('id');
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (pId) {
            return _this4.albumService.showComments(pId);
          })).subscribe(function (alb) {
            _this4.alb = alb;

            if (_this4.alb !== null) {
              _this4.alb.ratings.forEach(function (element) {
                _this4.rating = _this4.rating + element;
              });

              _this4.score = +(Math.round(_this4.rating / _this4.alb.ratings.length * 100) / 100).toFixed(2);
            }
          });

          this.forma = this.builder.group({
            comment: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(100)]]
          });
        }
      }, {
        key: "setCom",
        value: function setCom() {
          var _this5 = this;

          if (!this.forma.valid) {
            window.alert("Comment is not valid!");
          }

          if (this.alb == null) {
            var _sub = this._route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (param) {
              return param.get('id');
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (pId) {
              return _this5.albumService.firstComment(pId, _this5.user, _this5.forma.get('comment').value);
            })).subscribe(function (alb2) {
              return _this5.alb2 = alb2;
            });
          } else {
            var _sub2 = this._route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (param) {
              return param.get('id');
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (pId) {
              return _this5.albumService.addComment(pId, _this5.user, _this5.forma.get('comment').value);
            })).subscribe(function (alb3) {
              return _this5.alb3 = alb3;
            });
          }

          var sub = this._route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (param) {
            return param.get('id');
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (pId) {
            return _this5.albumService.showComments(pId);
          })).subscribe(function (alb) {
            return _this5.alb = alb;
          });

          this.forma.reset();
        }
      }, {
        key: "addToList",
        value: function addToList(trackId) {
          this.userService.addToList(String(trackId));
        }
      }]);

      return AlbumComponent;
    }();

    AlbumComponent.ɵfac = function AlbumComponent_Factory(t) {
      return new (t || AlbumComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_deezer_service__WEBPACK_IMPORTED_MODULE_4__["DeezerApi"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_album_service__WEBPACK_IMPORTED_MODULE_7__["AlbumService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]));
    };

    AlbumComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AlbumComponent,
      selectors: [["app-album"]],
      decls: 39,
      vars: 18,
      consts: [[1, "hero"], [1, "title-box"], [1, "back-to-search"], [1, "a", 3, "routerLink"], [1, "logout"], [1, "dropdown"], [1, "dropbtn"], [1, "dropdown-content"], [3, "routerLink"], ["target", "_self", 1, "dropbtn", 3, "routerLink"], ["align", "center", "class", "bot", 4, "ngIf"], [4, "ngIf"], [3, "formGroup", "ngSubmit"], [1, "form-group"], [1, "leave-comment-box"], ["rows", "4", "cols", "50", "formControlName", "comment", "placeholder", "Enter text here...", 1, "input-field"], ["type", "submit", 1, "submit-btn"], ["align", "center", 1, "bot"], [1, "album-header"], [1, "row"], [1, "col-md-4"], [1, "col-md-8"], [2, "color", "#494848"], [1, "bot"], [1, "album-extras"], [1, "rating", 3, "change"], ["type", "radio", "id", "star5", "name", "rating", "value", "5"], ["for", "star5", "title", "Awesome - 5 stars", 1, "full"], ["type", "radio", "id", "star4", "name", "rating", "value", "4"], ["for", "star4", "title", "Pretty good - 4 stars", 1, "full"], ["type", "radio", "id", "star3", "name", "rating", "value", "3"], ["for", "star3", "title", "Meh - 3 stars", 1, "full"], ["type", "radio", "id", "star2", "name", "rating", "value", "2"], ["for", "star2", "title", "Kinda bad - 2 stars", 1, "full"], ["type", "radio", "id", "star1", "name", "rating", "value", "1"], ["for", "star1", "title", "Sucks big time - 1 star", 1, "full"], ["target", "_self", 1, "btn", "a4", 3, "href"], [1, "a2"], [4, "ngFor", "ngForOf"], [1, "img", "img-thumbnail", "album-thumb", 3, "src"], [1, "col-md-12"], [1, "well", "album", 2, "background", "#f5b2b2", "border-radius", "0.5rem", "padding", "1em"], ["target", "_blank", 1, "btn", "a4", 3, "href"], [1, "btn", "a4", 3, "click"], [1, "comment-box"]],
      template: function AlbumComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "body");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Find your jam");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " Back to search page ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Log out");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Change username");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Change password");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Change e-mail");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "hr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "My list");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, AlbumComponent_p_25_Template, 2, 0, "p", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, AlbumComponent_div_26_Template, 51, 11, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Comments");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, AlbumComponent_div_29_Template, 2, 1, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "form", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function AlbumComponent_Template_form_ngSubmit_31_listener() {
            return ctx.setCom();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "textarea", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "button", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Submit");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.user, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](14, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](15, _c3));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](16, _c4));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](17, _c5));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.loader);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.album);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.alb !== null);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.forma);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Comment as ", ctx.user, "");
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"]],
      styles: ["html[_ngcontent-%COMP%], body[_ngcontent-%COMP%]\n{\n    margin: 0px;\n    overflow-x: hidden;\n    padding-top: 0px;\n    width: 100%;\n}\n    @font-face {\n    font-family: myFirstFont;\n    src: url('font3.ttf');\n    }\n    h1[_ngcontent-%COMP%] {\n    font-family: myFirstFont;\n    font-size: 85px;\n    bottom: 5px;\n    text-align : center;\n    position: relative;\n    color : #F08080;\n    }\n    @font-face {\n    font-family: myFont;\n    src: url('fontForm.ttf');\n    }\n    h2[_ngcontent-%COMP%]{\n        font-family: myFont;\n    }\n    h3[_ngcontent-%COMP%]{\n        color:rgb(243, 234, 224);\n        font-weight: bold;\n        font-family: myFont;\n    }\n    h4[_ngcontent-%COMP%]{\n        font-weight: bold;\n        font-family: myFont;\n        font-size: large;\n    }\n    body[_ngcontent-%COMP%]{\n    background-image: url('vanilla_clouds.jpg');\n    background-position: center;\n    position:relative;\n    }\n    .hero[_ngcontent-%COMP%]{\n    height:100%;\n    width: 100%;\n    background-position: center;\n    background-size: cover;\n    position:relative;\n    }\n    .title-box[_ngcontent-%COMP%]{\n        width: 97%;\n        height: 135px;\n        position:relative;\n        margin:5% auto;\n        background: #FFE4E1;\n        padding: 10px;\n        box-shadow: 0 0 20px 9px #ff61241f;\n        border: 10px outset #BC8F8F;\n        border-radius: 40px;\n    }\n    .back-to-search[_ngcontent-%COMP%] {\n        top: 11px;\n        left: 10px;\n        right: 87%;\n        position: absolute;\n        font-family: myFont;\n        margin-top: 5px;\n    }\n    .a[_ngcontent-%COMP%]{\n        color: #F08080;\n        text-decoration: none;\n        font-family: myFont;\n        font-size: 18px;\n    }\n    .dropbtn[_ngcontent-%COMP%] {\n        background-color: transparent;\n        top: 7px;\n        padding-right: 5px;\n        color: #F08080;\n        font-family: myFont;\n        font-size: 20px;\n        border: none;\n      }\n    \n    .dropdown[_ngcontent-%COMP%] {\n        position: relative;\n        display: inline-block;\n      }\n    .dropdown2[_ngcontent-%COMP%] {\n        position: relative;\n        display: inline-block;\n        margin-top: 10px;\n        margin-left: 30px;\n      }\n    \n    .dropdown-content[_ngcontent-%COMP%] {\n        display: none;\n        position: absolute;\n        background-color: #FFE4E1;\n        font-family: myFont;\n        min-width: 160px;\n        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n        z-index: 1;\n      }\n    \n    .dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n        color: #F08080;\n        padding: 16px;\n        text-decoration: none;\n        display: block;\n      }\n    .logout[_ngcontent-%COMP%] {\n        \n        top: 11px;\n        right: 0px;\n        left: 87%;\n        position: absolute;\n        font-family: myFont;\n    }\n    \n    .dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {background-color: rgb(83, 60, 60);}\n    \n    .dropdown[_ngcontent-%COMP%]:hover   .dropdown-content[_ngcontent-%COMP%] {display: block;}\n    \n    .dropdown[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {background-color: #FFE4E1;}\n    .comment-box[_ngcontent-%COMP%]{\n    width: 70%;\n    height: 45px;\n    position: relative;\n    margin: 2% auto;\n    background: #FFE4E1;\n    padding: 10px;\n    box-shadow: 0 0 10px 5px #ff61241f;\n    border: 3px outset #BC8F8F;\n    border-radius: 40px;\n    }\n    .leave-comment-box[_ngcontent-%COMP%]{\n        width: 70%;\n        height: 250px;\n        position: relative;\n        margin: 10% auto;\n        background: #FFE4E1;\n        padding: 10px;\n        box-shadow: 0 0 10px 5px #ff61241f;\n        border: 5px outset #BC8F8F;\n        border-radius: 40px;\n        }\n    .input-field[_ngcontent-%COMP%]{\n        width: 100%;\n        height: 45%;\n        padding:10px 0;\n        margin: 5px 0;\n        border-left:0;\n        border-right:0;\n        border-top: 0;\n        border-bottom: 1px solid #999;\n        outline: none;\n        background: transparent;\n        font-family: myFont;\n    }\n    .submit-btn[_ngcontent-%COMP%]{\n        width: 20%;\n        padding: 10px 30px;\n        cursor:pointer;\n        display: block;\n        margin: auto;\n        background:linear-gradient(to right, #CD5C5C,#FFE4C4);\n        border: 0;\n        outline: none;\n        border-radius:30px;\n        font-family: myFont;\n        margin-top: 20px;\n    }\n    .submit-btn[_ngcontent-%COMP%]:hover{\n        background:linear-gradient(to left, #CD5C5C,#FFE4C4);\n    }\n    .back-to-artist[_ngcontent-%COMP%] {\n    top: 11px;\n    right: 0px;\n    left: 87%;\n    position: absolute;\n    font-family: myFont;\n    }\n    .a4[_ngcontent-%COMP%]{\n    color: bisque;\n    text-decoration: none;\n    font-family: myFont;\n    font-size: 18px;\n    }\n    .btn[_ngcontent-%COMP%]{\n        display: inline-block;\n        padding: .75rem 1.25rem;\n        border-radius: 10rem;\n        transition: all .3s;\n        position: relative;\n        overflow: hidden;\n        z-index: 1;\n        font-family: myFont;\n        \n    }\n    .btn[_ngcontent-%COMP%]::after {\n        content: '';\n        position: absolute;\n        bottom: 0;\n        left: 0;\n        width: 100%;\n        height: 100%;\n        background-color:  #c4a6e0;\n        border-radius: 10rem;\n        z-index: -2;\n    }\n    .btn[_ngcontent-%COMP%]::before {\n        content: '';\n        position: absolute;\n        bottom: 0;\n        left: 0;\n        width: 0%;\n        height: 100%;\n        background-color:  #754c9b;\n        transition: all .3s;\n        border-radius: 10rem;\n        z-index: -1;\n    }\n    .btn[_ngcontent-%COMP%]:hover {\n    color: #fff;\n    }\n    .btn[_ngcontent-%COMP%]:hover::before {\n    width: 100%;\n    }\n    .bot[_ngcontent-%COMP%]{\n        font-family: myFont;\n        font-size: 35px;\n        bottom : 60%;\n        color : #F08080;\n    }\n    .album-extras[_ngcontent-%COMP%]{\n        color: #494848;\n        font-size: 18px;\n        font-family: myFont;\n    }\n    .a2[_ngcontent-%COMP%]{\n        color: #494848;\n        font-size: 25px;\n        font-family: myFont;\n    }\n    .a3[_ngcontent-%COMP%]{\n        color: #F08080;\n        text-decoration: none;\n        font-family: myFont;\n        font-size: 18px;\n    }\n    \n    .rating[_ngcontent-%COMP%] { \n    border: none;\n    float: left;\n  }\n    .rating[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%] { display: none; }\n    .rating[_ngcontent-%COMP%]    > label[_ngcontent-%COMP%]:before { \n    margin: 5px;\n    font-size: 1.25em;\n    font-family: FontAwesome;\n    display: inline-block;\n    content: \"\\f005\";\n  }\n    .rating[_ngcontent-%COMP%]    > label[_ngcontent-%COMP%] { \n    color: #BC8F8F; \n   float: right; \n  }\n    \n    .rating[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%], .rating[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%]:hover, .rating[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%]:hover    ~ label[_ngcontent-%COMP%] { color: #FFD700;  }\n    \n    .rating[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    + label[_ngcontent-%COMP%]:hover, .rating[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%]:hover, .rating[_ngcontent-%COMP%]    > label[_ngcontent-%COMP%]:hover    ~ input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%], .rating[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%]:hover    ~ label[_ngcontent-%COMP%] { color: #FFED85;  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWxidW0vYWxidW0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7SUFFSSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixXQUFXO0FBQ2Y7SUFDSTtJQUNBLHdCQUF3QjtJQUN4QixxQkFBNEI7SUFDNUI7SUFDQTtJQUNBLHdCQUF3QjtJQUN4QixlQUFlO0lBQ2YsV0FBVztJQUNYLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmO0lBRUE7SUFDQSxtQkFBbUI7SUFDbkIsd0JBQStCO0lBQy9CO0lBQ0E7UUFDSSxtQkFBbUI7SUFDdkI7SUFDQTtRQUNJLHdCQUF3QjtRQUN4QixpQkFBaUI7UUFDakIsbUJBQW1CO0lBQ3ZCO0lBQ0E7UUFDSSxpQkFBaUI7UUFDakIsbUJBQW1CO1FBQ25CLGdCQUFnQjtJQUNwQjtJQUNBO0lBQ0EsMkNBQW1EO0lBQ25ELDJCQUEyQjtJQUMzQixpQkFBaUI7SUFDakI7SUFDQTtJQUNBLFdBQVc7SUFDWCxXQUFXO0lBQ1gsMkJBQTJCO0lBQzNCLHNCQUFzQjtJQUN0QixpQkFBaUI7SUFDakI7SUFDQTtRQUNJLFVBQVU7UUFDVixhQUFhO1FBQ2IsaUJBQWlCO1FBQ2pCLGNBQWM7UUFDZCxtQkFBbUI7UUFDbkIsYUFBYTtRQUNiLGtDQUFrQztRQUNsQywyQkFBMkI7UUFDM0IsbUJBQW1CO0lBQ3ZCO0lBRUE7UUFDSSxTQUFTO1FBQ1QsVUFBVTtRQUNWLFVBQVU7UUFDVixrQkFBa0I7UUFDbEIsbUJBQW1CO1FBQ25CLGVBQWU7SUFDbkI7SUFFQTtRQUNJLGNBQWM7UUFDZCxxQkFBcUI7UUFDckIsbUJBQW1CO1FBQ25CLGVBQWU7SUFDbkI7SUFDQTtRQUNJLDZCQUE2QjtRQUM3QixRQUFRO1FBQ1Isa0JBQWtCO1FBQ2xCLGNBQWM7UUFDZCxtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLFlBQVk7TUFDZDtJQUVBLGtFQUFrRTtJQUNsRTtRQUNFLGtCQUFrQjtRQUNsQixxQkFBcUI7TUFDdkI7SUFDQTtRQUNFLGtCQUFrQjtRQUNsQixxQkFBcUI7UUFDckIsZ0JBQWdCO1FBQ2hCLGlCQUFpQjtNQUNuQjtJQUVBLHlDQUF5QztJQUN6QztRQUNFLGFBQWE7UUFDYixrQkFBa0I7UUFDbEIseUJBQXlCO1FBQ3pCLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIsNENBQTRDO1FBQzVDLFVBQVU7TUFDWjtJQUVBLDhCQUE4QjtJQUM5QjtRQUNFLGNBQWM7UUFDZCxhQUFhO1FBQ2IscUJBQXFCO1FBQ3JCLGNBQWM7TUFDaEI7SUFFQTtRQUNFLDBCQUEwQjtRQUMxQixTQUFTO1FBQ1QsVUFBVTtRQUNWLFNBQVM7UUFDVCxrQkFBa0I7UUFDbEIsbUJBQW1CO0lBQ3ZCO0lBRUUsNENBQTRDO0lBQzVDLDJCQUEyQixpQ0FBaUMsQ0FBQztJQUU3RCxvQ0FBb0M7SUFDcEMsbUNBQW1DLGNBQWMsQ0FBQztJQUVsRCwwRkFBMEY7SUFDMUYsMEJBQTBCLHlCQUF5QixDQUFDO0lBR3REO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isa0NBQWtDO0lBQ2xDLDBCQUEwQjtJQUMxQixtQkFBbUI7SUFDbkI7SUFDQTtRQUNJLFVBQVU7UUFDVixhQUFhO1FBQ2Isa0JBQWtCO1FBQ2xCLGdCQUFnQjtRQUNoQixtQkFBbUI7UUFDbkIsYUFBYTtRQUNiLGtDQUFrQztRQUNsQywwQkFBMEI7UUFDMUIsbUJBQW1CO1FBQ25CO0lBQ0o7UUFDSSxXQUFXO1FBQ1gsV0FBVztRQUNYLGNBQWM7UUFDZCxhQUFhO1FBQ2IsYUFBYTtRQUNiLGNBQWM7UUFDZCxhQUFhO1FBQ2IsNkJBQTZCO1FBQzdCLGFBQWE7UUFDYix1QkFBdUI7UUFDdkIsbUJBQW1CO0lBQ3ZCO0lBQ0E7UUFDSSxVQUFVO1FBQ1Ysa0JBQWtCO1FBQ2xCLGNBQWM7UUFDZCxjQUFjO1FBQ2QsWUFBWTtRQUNaLHFEQUFxRDtRQUNyRCxTQUFTO1FBQ1QsYUFBYTtRQUNiLGtCQUFrQjtRQUNsQixtQkFBbUI7UUFDbkIsZ0JBQWdCO0lBQ3BCO0lBQ0E7UUFDSSxvREFBb0Q7SUFDeEQ7SUFDQTtJQUNBLFNBQVM7SUFDVCxVQUFVO0lBQ1YsU0FBUztJQUNULGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkI7SUFDQTtJQUNBLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZjtJQUVBO1FBQ0kscUJBQXFCO1FBQ3JCLHVCQUF1QjtRQUN2QixvQkFBb0I7UUFDcEIsbUJBQW1CO1FBQ25CLGtCQUFrQjtRQUNsQixnQkFBZ0I7UUFDaEIsVUFBVTtRQUNWLG1CQUFtQjs7SUFFdkI7SUFDQTtRQUNJLFdBQVc7UUFDWCxrQkFBa0I7UUFDbEIsU0FBUztRQUNULE9BQU87UUFDUCxXQUFXO1FBQ1gsWUFBWTtRQUNaLDBCQUEwQjtRQUMxQixvQkFBb0I7UUFDcEIsV0FBVztJQUNmO0lBQ0E7UUFDSSxXQUFXO1FBQ1gsa0JBQWtCO1FBQ2xCLFNBQVM7UUFDVCxPQUFPO1FBQ1AsU0FBUztRQUNULFlBQVk7UUFDWiwwQkFBMEI7UUFDMUIsbUJBQW1CO1FBQ25CLG9CQUFvQjtRQUNwQixXQUFXO0lBQ2Y7SUFDQTtJQUNBLFdBQVc7SUFDWDtJQUNBO0lBQ0EsV0FBVztJQUNYO0lBQ0E7UUFDSSxtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLFlBQVk7UUFDWixlQUFlO0lBQ25CO0lBQ0E7UUFDSSxjQUFjO1FBQ2QsZUFBZTtRQUNmLG1CQUFtQjtJQUN2QjtJQUNBO1FBQ0ksY0FBYztRQUNkLGVBQWU7UUFDZixtQkFBbUI7SUFDdkI7SUFDQTtRQUNJLGNBQWM7UUFDZCxxQkFBcUI7UUFDckIsbUJBQW1CO1FBQ25CLGVBQWU7SUFDbkI7SUFHSixzQ0FBc0M7SUFFdEM7SUFDSSxZQUFZO0lBQ1osV0FBVztFQUNiO0lBRUEsa0JBQWtCLGFBQWEsRUFBRTtJQUNqQztJQUNFLFdBQVc7SUFDWCxpQkFBaUI7SUFDakIsd0JBQXdCO0lBQ3hCLHFCQUFxQjtJQUNyQixnQkFBZ0I7RUFDbEI7SUFHQTtJQUNFLGNBQWM7R0FDZixZQUFZO0VBQ2I7SUFFQSxrREFBa0Q7SUFFbEQ7O2dEQUU4QyxjQUFjLEdBQUc7SUFBRSxpQ0FBaUM7SUFFbEc7OztrREFHZ0QsY0FBYyxHQUFHIiwiZmlsZSI6InNyYy9hcHAvYWxidW0vYWxidW0uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImh0bWwsYm9keVxue1xuICAgIG1hcmdpbjogMHB4O1xuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuICAgIEBmb250LWZhY2Uge1xuICAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udDtcbiAgICBzcmM6IHVybCguLi9mb250cy9mb250My50dGYpO1xuICAgIH1cbiAgICBoMSB7XG4gICAgZm9udC1mYW1pbHk6IG15Rmlyc3RGb250O1xuICAgIGZvbnQtc2l6ZTogODVweDtcbiAgICBib3R0b206IDVweDtcbiAgICB0ZXh0LWFsaWduIDogY2VudGVyO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBjb2xvciA6ICNGMDgwODA7XG4gICAgfVxuICAgIFxuICAgIEBmb250LWZhY2Uge1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgc3JjOiB1cmwoLi4vZm9udHMvZm9udEZvcm0udHRmKTtcbiAgICB9ICBcbiAgICBoMntcbiAgICAgICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICB9XG4gICAgaDN7XG4gICAgICAgIGNvbG9yOnJnYigyNDMsIDIzNCwgMjI0KTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgfVxuICAgIGg0e1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICAgICAgZm9udC1zaXplOiBsYXJnZTtcbiAgICB9XG4gICAgYm9keXtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vaW1hZ2VzL3ZhbmlsbGFfY2xvdWRzLmpwZyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgIH1cbiAgICAuaGVyb3tcbiAgICBoZWlnaHQ6MTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgICB9IFxuICAgIC50aXRsZS1ib3h7XG4gICAgICAgIHdpZHRoOiA5NyU7XG4gICAgICAgIGhlaWdodDogMTM1cHg7XG4gICAgICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgICAgICBtYXJnaW46NSUgYXV0bztcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRTRFMTtcbiAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDIwcHggOXB4ICNmZjYxMjQxZjtcbiAgICAgICAgYm9yZGVyOiAxMHB4IG91dHNldCAjQkM4RjhGO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA0MHB4O1xuICAgIH1cbiAgICAgICAgIFxuICAgIC5iYWNrLXRvLXNlYXJjaCB7XG4gICAgICAgIHRvcDogMTFweDtcbiAgICAgICAgbGVmdDogMTBweDtcbiAgICAgICAgcmlnaHQ6IDg3JTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgfVxuICAgIFxuICAgIC5he1xuICAgICAgICBjb2xvcjogI0YwODA4MDtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgfVxuICAgIC5kcm9wYnRuIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICAgIHRvcDogN3B4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgICAgIGNvbG9yOiAjRjA4MDgwO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgLyogVGhlIGNvbnRhaW5lciA8ZGl2PiAtIG5lZWRlZCB0byBwb3NpdGlvbiB0aGUgZHJvcGRvd24gY29udGVudCAqL1xuICAgICAgLmRyb3Bkb3duIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICB9XG4gICAgICAuZHJvcGRvd24yIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICAgICAgfVxuICAgICAgXG4gICAgICAvKiBEcm9wZG93biBDb250ZW50IChIaWRkZW4gYnkgRGVmYXVsdCkgKi9cbiAgICAgIC5kcm9wZG93bi1jb250ZW50IHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZFNEUxO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgICAgICBtaW4td2lkdGg6IDE2MHB4O1xuICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcbiAgICAgICAgei1pbmRleDogMTtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgLyogTGlua3MgaW5zaWRlIHRoZSBkcm9wZG93biAqL1xuICAgICAgLmRyb3Bkb3duLWNvbnRlbnQgYSB7XG4gICAgICAgIGNvbG9yOiAjRjA4MDgwO1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgfVxuICAgIFxuICAgICAgLmxvZ291dCB7XG4gICAgICAgIC8qIGJhY2tncm91bmQ6ICAjQ0Q1QzVDOyAqL1xuICAgICAgICB0b3A6IDExcHg7XG4gICAgICAgIHJpZ2h0OiAwcHg7XG4gICAgICAgIGxlZnQ6IDg3JTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIH1cbiAgICAgIFxuICAgICAgLyogQ2hhbmdlIGNvbG9yIG9mIGRyb3Bkb3duIGxpbmtzIG9uIGhvdmVyICovXG4gICAgICAuZHJvcGRvd24tY29udGVudCBhOmhvdmVyIHtiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoODMsIDYwLCA2MCk7fVxuICAgICAgXG4gICAgICAvKiBTaG93IHRoZSBkcm9wZG93biBtZW51IG9uIGhvdmVyICovXG4gICAgICAuZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge2Rpc3BsYXk6IGJsb2NrO31cbiAgICAgIFxuICAgICAgLyogQ2hhbmdlIHRoZSBiYWNrZ3JvdW5kIGNvbG9yIG9mIHRoZSBkcm9wZG93biBidXR0b24gd2hlbiB0aGUgZHJvcGRvd24gY29udGVudCBpcyBzaG93biAqL1xuICAgICAgLmRyb3Bkb3duOmhvdmVyIC5kcm9wYnRuIHtiYWNrZ3JvdW5kLWNvbG9yOiAjRkZFNEUxO31cbiAgICBcbiAgICBcbiAgICAuY29tbWVudC1ib3h7XG4gICAgd2lkdGg6IDcwJTtcbiAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG1hcmdpbjogMiUgYXV0bztcbiAgICBiYWNrZ3JvdW5kOiAjRkZFNEUxO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgYm94LXNoYWRvdzogMCAwIDEwcHggNXB4ICNmZjYxMjQxZjtcbiAgICBib3JkZXI6IDNweCBvdXRzZXQgI0JDOEY4RjtcbiAgICBib3JkZXItcmFkaXVzOiA0MHB4O1xuICAgIH1cbiAgICAubGVhdmUtY29tbWVudC1ib3h7XG4gICAgICAgIHdpZHRoOiA3MCU7XG4gICAgICAgIGhlaWdodDogMjUwcHg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgbWFyZ2luOiAxMCUgYXV0bztcbiAgICAgICAgYmFja2dyb3VuZDogI0ZGRTRFMTtcbiAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDEwcHggNXB4ICNmZjYxMjQxZjtcbiAgICAgICAgYm9yZGVyOiA1cHggb3V0c2V0ICNCQzhGOEY7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gICAgICAgIH1cbiAgICAuaW5wdXQtZmllbGR7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDQ1JTtcbiAgICAgICAgcGFkZGluZzoxMHB4IDA7XG4gICAgICAgIG1hcmdpbjogNXB4IDA7XG4gICAgICAgIGJvcmRlci1sZWZ0OjA7XG4gICAgICAgIGJvcmRlci1yaWdodDowO1xuICAgICAgICBib3JkZXItdG9wOiAwO1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzk5OTtcbiAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgfVxuICAgIC5zdWJtaXQtYnRue1xuICAgICAgICB3aWR0aDogMjAlO1xuICAgICAgICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gICAgICAgIGN1cnNvcjpwb2ludGVyO1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI0NENUM1QywjRkZFNEM0KTtcbiAgICAgICAgYm9yZGVyOiAwO1xuICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgICAgICBib3JkZXItcmFkaXVzOjMwcHg7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgfVxuICAgIC5zdWJtaXQtYnRuOmhvdmVye1xuICAgICAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byBsZWZ0LCAjQ0Q1QzVDLCNGRkU0QzQpO1xuICAgIH1cbiAgICAuYmFjay10by1hcnRpc3Qge1xuICAgIHRvcDogMTFweDtcbiAgICByaWdodDogMHB4O1xuICAgIGxlZnQ6IDg3JTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICB9XG4gICAgLmE0e1xuICAgIGNvbG9yOiBiaXNxdWU7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIH1cblxuICAgIC5idG57XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgcGFkZGluZzogLjc1cmVtIDEuMjVyZW07XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcmVtO1xuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIHotaW5kZXg6IDE7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgICAgIFxuICAgIH1cbiAgICAuYnRuOjphZnRlciB7XG4gICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGJvdHRvbTogMDtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogICNjNGE2ZTA7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcmVtO1xuICAgICAgICB6LWluZGV4OiAtMjtcbiAgICB9XG4gICAgLmJ0bjo6YmVmb3JlIHtcbiAgICAgICAgY29udGVudDogJyc7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgYm90dG9tOiAwO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICB3aWR0aDogMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogICM3NTRjOWI7XG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcmVtO1xuICAgICAgICB6LWluZGV4OiAtMTtcbiAgICB9XG4gICAgLmJ0bjpob3ZlciB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgfVxuICAgIC5idG46aG92ZXI6OmJlZm9yZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICAgIC5ib3R7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMzVweDtcbiAgICAgICAgYm90dG9tIDogNjAlO1xuICAgICAgICBjb2xvciA6ICNGMDgwODA7XG4gICAgfVxuICAgIC5hbGJ1bS1leHRyYXN7XG4gICAgICAgIGNvbG9yOiAjNDk0ODQ4O1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgfVxuICAgIC5hMntcbiAgICAgICAgY29sb3I6ICM0OTQ4NDg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICB9XG4gICAgLmEze1xuICAgICAgICBjb2xvcjogI0YwODA4MDtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgfVxuXG5cbi8qKioqKiogU3R5bGUgU3RhciBSYXRpbmcgV2lkZ2V0ICoqKioqL1xuXG4ucmF0aW5nIHsgXG4gICAgYm9yZGVyOiBub25lO1xuICAgIGZsb2F0OiBsZWZ0O1xuICB9XG4gIFxuICAucmF0aW5nID4gaW5wdXQgeyBkaXNwbGF5OiBub25lOyB9IFxuICAucmF0aW5nID4gbGFiZWw6YmVmb3JlIHsgXG4gICAgbWFyZ2luOiA1cHg7XG4gICAgZm9udC1zaXplOiAxLjI1ZW07XG4gICAgZm9udC1mYW1pbHk6IEZvbnRBd2Vzb21lO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBjb250ZW50OiBcIlxcZjAwNVwiO1xuICB9XG4gIFxuICBcbiAgLnJhdGluZyA+IGxhYmVsIHsgXG4gICAgY29sb3I6ICNCQzhGOEY7IFxuICAgZmxvYXQ6IHJpZ2h0OyBcbiAgfVxuICBcbiAgLyoqKioqIENTUyBNYWdpYyB0byBIaWdobGlnaHQgU3RhcnMgb24gSG92ZXIgKioqKiovXG4gIFxuICAucmF0aW5nID4gaW5wdXQ6Y2hlY2tlZCB+IGxhYmVsLCAvKiBzaG93IGdvbGQgc3RhciB3aGVuIGNsaWNrZWQgKi9cbiAgLnJhdGluZzpub3QoOmNoZWNrZWQpID4gbGFiZWw6aG92ZXIsIC8qIGhvdmVyIGN1cnJlbnQgc3RhciAqL1xuICAucmF0aW5nOm5vdCg6Y2hlY2tlZCkgPiBsYWJlbDpob3ZlciB+IGxhYmVsIHsgY29sb3I6ICNGRkQ3MDA7ICB9IC8qIGhvdmVyIHByZXZpb3VzIHN0YXJzIGluIGxpc3QgKi9cbiAgXG4gIC5yYXRpbmcgPiBpbnB1dDpjaGVja2VkICsgbGFiZWw6aG92ZXIsIC8qIGhvdmVyIGN1cnJlbnQgc3RhciB3aGVuIGNoYW5naW5nIHJhdGluZyAqL1xuICAucmF0aW5nID4gaW5wdXQ6Y2hlY2tlZCB+IGxhYmVsOmhvdmVyLFxuICAucmF0aW5nID4gbGFiZWw6aG92ZXIgfiBpbnB1dDpjaGVja2VkIH4gbGFiZWwsIC8qIGxpZ2h0ZW4gY3VycmVudCBzZWxlY3Rpb24gKi9cbiAgLnJhdGluZyA+IGlucHV0OmNoZWNrZWQgfiBsYWJlbDpob3ZlciB+IGxhYmVsIHsgY29sb3I6ICNGRkVEODU7ICB9ICJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AlbumComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-album',
          templateUrl: './album.component.html',
          styleUrls: ['album.component.css']
        }]
      }], function () {
        return [{
          type: _service_deezer_service__WEBPACK_IMPORTED_MODULE_4__["DeezerApi"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]
        }, {
          type: _service_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"]
        }, {
          type: _service_album_service__WEBPACK_IMPORTED_MODULE_7__["AlbumService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var AppComponent = /*#__PURE__*/function () {
      function AppComponent() {
        _classCallCheck(this, AppComponent);

        this.title = 'Find your Jam';
      }

      _createClass(AppComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AppComponent;
    }();

    AppComponent.ɵfac = function AppComponent_Factory(t) {
      return new (t || AppComponent)();
    };

    AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AppComponent,
      selectors: [["app-root"]],
      decls: 1,
      vars: 0,
      template: function AppComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-root',
          templateUrl: './app.component.html',
          styleUrls: ['./app.component.css']
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _search_search_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./search/search.component */
    "./src/app/search/search.component.ts");
    /* harmony import */


    var _register_register_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./register/register.component */
    "./src/app/register/register.component.ts");
    /* harmony import */


    var _notfound_notfound_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./notfound/notfound.component */
    "./src/app/notfound/notfound.component.ts");
    /* harmony import */


    var _service_deezer_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./service/deezer.service */
    "./src/app/service/deezer.service.ts");
    /* harmony import */


    var _artist_artist_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./artist/artist.component */
    "./src/app/artist/artist.component.ts");
    /* harmony import */


    var _album_album_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./album/album.component */
    "./src/app/album/album.component.ts");
    /* harmony import */


    var _ch_username_ch_username_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./ch-username/ch-username.component */
    "./src/app/ch-username/ch-username.component.ts");
    /* harmony import */


    var _ch_email_ch_email_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./ch-email/ch-email.component */
    "./src/app/ch-email/ch-email.component.ts");
    /* harmony import */


    var _ch_password_ch_password_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./ch-password/ch-password.component */
    "./src/app/ch-password/ch-password.component.ts");
    /* harmony import */


    var _list_tracks_list_tracks_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./list-tracks/list-tracks.component */
    "./src/app/list-tracks/list-tracks.component.ts");

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
      type: AppModule,
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
    });
    AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
      factory: function AppModule_Factory(t) {
        return new (t || AppModule)();
      },
      providers: [_service_deezer_service__WEBPACK_IMPORTED_MODULE_10__["DeezerApi"]],
      imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_http__WEBPACK_IMPORTED_MODULE_5__["HttpModule"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot([{
        path: '',
        component: _register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"]
      }, {
        path: 'search',
        component: _search_search_component__WEBPACK_IMPORTED_MODULE_7__["SearchComponent"]
      }, {
        path: 'artist/:id',
        component: _artist_artist_component__WEBPACK_IMPORTED_MODULE_11__["ArtistComponent"]
      }, {
        path: 'album/:id',
        component: _album_album_component__WEBPACK_IMPORTED_MODULE_12__["AlbumComponent"]
      }, {
        path: 'change/username',
        component: _ch_username_ch_username_component__WEBPACK_IMPORTED_MODULE_13__["ChUsernameComponent"]
      }, //http://localhost:4200/change/email
      {
        path: 'change/email',
        component: _ch_email_ch_email_component__WEBPACK_IMPORTED_MODULE_14__["ChEmailComponent"]
      }, //http://localhost:4200/change/password
      {
        path: 'change/password',
        component: _ch_password_ch_password_component__WEBPACK_IMPORTED_MODULE_15__["ChPasswordComponent"]
      }, {
        path: 'myList',
        component: _list_tracks_list_tracks_component__WEBPACK_IMPORTED_MODULE_16__["ListTracksComponent"]
      }, {
        path: '**',
        component: _notfound_notfound_component__WEBPACK_IMPORTED_MODULE_9__["NotfoundComponent"]
      }])]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AppModule, {
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"], _search_search_component__WEBPACK_IMPORTED_MODULE_7__["SearchComponent"], _register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"], _notfound_notfound_component__WEBPACK_IMPORTED_MODULE_9__["NotfoundComponent"], _artist_artist_component__WEBPACK_IMPORTED_MODULE_11__["ArtistComponent"], _album_album_component__WEBPACK_IMPORTED_MODULE_12__["AlbumComponent"], _ch_username_ch_username_component__WEBPACK_IMPORTED_MODULE_13__["ChUsernameComponent"], _ch_email_ch_email_component__WEBPACK_IMPORTED_MODULE_14__["ChEmailComponent"], _ch_password_ch_password_component__WEBPACK_IMPORTED_MODULE_15__["ChPasswordComponent"], _list_tracks_list_tracks_component__WEBPACK_IMPORTED_MODULE_16__["ListTracksComponent"]],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_http__WEBPACK_IMPORTED_MODULE_5__["HttpModule"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"],
        args: [{
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"], _search_search_component__WEBPACK_IMPORTED_MODULE_7__["SearchComponent"], _register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"], _notfound_notfound_component__WEBPACK_IMPORTED_MODULE_9__["NotfoundComponent"], _artist_artist_component__WEBPACK_IMPORTED_MODULE_11__["ArtistComponent"], _album_album_component__WEBPACK_IMPORTED_MODULE_12__["AlbumComponent"], _ch_username_ch_username_component__WEBPACK_IMPORTED_MODULE_13__["ChUsernameComponent"], _ch_email_ch_email_component__WEBPACK_IMPORTED_MODULE_14__["ChEmailComponent"], _ch_password_ch_password_component__WEBPACK_IMPORTED_MODULE_15__["ChPasswordComponent"], _list_tracks_list_tracks_component__WEBPACK_IMPORTED_MODULE_16__["ListTracksComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_http__WEBPACK_IMPORTED_MODULE_5__["HttpModule"], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot([{
            path: '',
            component: _register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"]
          }, {
            path: 'search',
            component: _search_search_component__WEBPACK_IMPORTED_MODULE_7__["SearchComponent"]
          }, {
            path: 'artist/:id',
            component: _artist_artist_component__WEBPACK_IMPORTED_MODULE_11__["ArtistComponent"]
          }, {
            path: 'album/:id',
            component: _album_album_component__WEBPACK_IMPORTED_MODULE_12__["AlbumComponent"]
          }, {
            path: 'change/username',
            component: _ch_username_ch_username_component__WEBPACK_IMPORTED_MODULE_13__["ChUsernameComponent"]
          }, //http://localhost:4200/change/email
          {
            path: 'change/email',
            component: _ch_email_ch_email_component__WEBPACK_IMPORTED_MODULE_14__["ChEmailComponent"]
          }, //http://localhost:4200/change/password
          {
            path: 'change/password',
            component: _ch_password_ch_password_component__WEBPACK_IMPORTED_MODULE_15__["ChPasswordComponent"]
          }, {
            path: 'myList',
            component: _list_tracks_list_tracks_component__WEBPACK_IMPORTED_MODULE_16__["ListTracksComponent"]
          }, {
            path: '**',
            component: _notfound_notfound_component__WEBPACK_IMPORTED_MODULE_9__["NotfoundComponent"]
          }])],
          providers: [_service_deezer_service__WEBPACK_IMPORTED_MODULE_10__["DeezerApi"]],
          bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/artist/artist.component.ts":
  /*!********************************************!*\
    !*** ./src/app/artist/artist.component.ts ***!
    \********************************************/

  /*! exports provided: ArtistComponent */

  /***/
  function srcAppArtistArtistComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArtistComponent", function () {
      return ArtistComponent;
    });
    /* harmony import */


    var rxjs_Observable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs/Observable */
    "./node_modules/rxjs-compat/_esm2015/Observable.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs_add_observable_forkJoin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/add/observable/forkJoin */
    "./node_modules/rxjs-compat/_esm2015/add/observable/forkJoin.js");
    /* harmony import */


    var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/add/operator/map */
    "./node_modules/rxjs-compat/_esm2015/add/operator/map.js");
    /* harmony import */


    var _service_deezer_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../service/deezer.service */
    "./src/app/service/deezer.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _service_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../service/user.service */
    "./src/app/service/user.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function ArtistComponent_p_25_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Loading ...");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function ArtistComponent_div_26_div_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "img", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("src", ctx_r29.artist.picture_medium, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeUrl"]);
      }
    }

    function ArtistComponent_div_26_div_7_div_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "img", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "h3", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "a", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ArtistComponent_div_26_div_7_div_4_Template_a_click_5_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r35);

          var album_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          var ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r33.dodaj_u_istoriju(album_r31.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, " View Album");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var album_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("src", album_r31.cover_medium, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeUrl"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](album_r31.title);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate1"]("routerLink", "/album/", album_r31.id, "");
      }
    }

    function ArtistComponent_div_26_div_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, ArtistComponent_div_26_div_7_div_4_Template, 7, 3, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var album_r31 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", album_r31.cover.length > 0);
      }
    }

    function ArtistComponent_div_26_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "header", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ArtistComponent_div_26_div_2_Template, 2, 1, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "h2", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, ArtistComponent_div_26_div_7_Template, 6, 1, "div", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r28.artist.picture.length > 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Artist name: ", ctx_r28.artist == null ? null : ctx_r28.artist.name, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r28.albums);
      }
    }

    var _c0 = function _c0() {
      return ["", "search"];
    };

    var _c1 = function _c1() {
      return ["/"];
    };

    var _c2 = function _c2() {
      return ["/change/username"];
    };

    var _c3 = function _c3() {
      return ["/change/password"];
    };

    var _c4 = function _c4() {
      return ["/change/email"];
    };

    var _c5 = function _c5() {
      return ["/myList"];
    };

    var ArtistComponent = /*#__PURE__*/function () {
      function ArtistComponent(_deezerApi, _route, userService) {
        _classCallCheck(this, ArtistComponent);

        this._deezerApi = _deezerApi;
        this._route = _route;
        this.userService = userService;
        this.albums = [];
        this.user = null;
        this.user = this.userService.getUser();
      }

      _createClass(ArtistComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this6 = this;

          this._route.params.map(function (params) {
            return params['id'];
          }).subscribe(function (id) {
            _this6.loader = true;
            rxjs_Observable__WEBPACK_IMPORTED_MODULE_0__["Observable"].forkJoin([_this6._deezerApi.getArtist(id), _this6._deezerApi.getAlbums(id)]).subscribe(function (results) {
              _this6.artist = results[0];
              _this6.albums = results[1];
              _this6.loader = false;
            }, function (error) {
              console.log(error);
              _this6.loader = false;
            });
          });
        }
      }, {
        key: "dodaj_u_istoriju",
        value: function dodaj_u_istoriju(alb_id) {
          this.userService.addToHistory(this.user, alb_id);
        }
      }]);

      return ArtistComponent;
    }();

    ArtistComponent.ɵfac = function ArtistComponent_Factory(t) {
      return new (t || ArtistComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_deezer_service__WEBPACK_IMPORTED_MODULE_4__["DeezerApi"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"]));
    };

    ArtistComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: ArtistComponent,
      selectors: [["app-artist"]],
      decls: 27,
      vars: 15,
      consts: [[1, "hero"], [1, "title-box"], [1, "back-to-search"], [1, "a", 3, "routerLink"], [1, "logout"], [1, "dropdown"], [1, "dropbtn"], [1, "dropdown-content"], [3, "routerLink"], ["target", "_self", 1, "dropbtn", 3, "routerLink"], ["class", "bot", "align", "center", 4, "ngIf"], [4, "ngIf"], ["align", "center", 1, "bot"], [1, "artist-header"], [1, "bot", 2, "color", "#F08080"], [1, "artist-albums"], [4, "ngFor", "ngForOf"], [1, "artist-thumb", "img-circle", 3, "src"], [1, "row"], [1, "col-md-12"], [1, "well", "album", 2, "background", "#f5b2b2", "border-radius", "0.5rem", "padding", "1em"], [1, "img", "img-thumbnail", "album-thumb", 3, "src"], [2, "color", "bisque"], ["target", "_self", 1, "btn", "a", 2, "color", "bisque", 3, "routerLink", "click"]],
      template: function ArtistComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "html");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "body");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "Find your jam");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "a", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, " Back to search page ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "button", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "Log out");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Change username");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19, "Change password");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "Change e-mail");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](22, "hr");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "a", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24, "My list");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](25, ArtistComponent_p_25_Template, 2, 0, "p", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](26, ArtistComponent_div_26_Template, 8, 3, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](9, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.user, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](10, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](11, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](12, _c3));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](13, _c4));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](14, _c5));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.loader);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.artist);
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"]],
      styles: ["html[_ngcontent-%COMP%], body[_ngcontent-%COMP%]\n{\n    margin: 0px;\n    overflow-x: hidden;\n    padding-top: 0px;\n    width: 100%;\n}\n    @font-face {\n    font-family: myFirstFont;\n    src: url('font3.ttf');\n    }\n    h1[_ngcontent-%COMP%] {\n    font-family: myFirstFont;\n    font-size: 85px;\n    bottom: 5px;\n    text-align : center;\n    position: relative;\n    color : #F08080;\n    }\n    @font-face {\n    font-family: myFont;\n    src: url('fontForm.ttf');\n    }\n    h2[_ngcontent-%COMP%]{\n        font-family: myFont;\n    }\n    h3[_ngcontent-%COMP%]{\n        color:rgb(243, 234, 224);\n        font-weight: bold;\n        font-family: myFont;\n    }\n    h4[_ngcontent-%COMP%]{\n        font-weight: bold;\n        font-family: myFont;\n        font-size: large;\n    }\n    body[_ngcontent-%COMP%]{\n    background-image: url('vanilla_clouds.jpg');\n    background-position: center;\n    position:relative;\n    }\n    .hero[_ngcontent-%COMP%]{\n    height:100%;\n    width: 100%;\n    background-position: center;\n    background-size: cover;\n    position:relative;\n}\n    .title-box[_ngcontent-%COMP%]{\n    width: 97%;\n    height: 135px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    padding: 10px;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    border: 10px outset #BC8F8F;\n    border-radius: 40px;\n}\n    .back-to-search[_ngcontent-%COMP%] {\n    top: 11px;\n    left: 10px;\n    right: 87%;\n    position: absolute;\n    font-family: myFont;\n    margin-top: 5px;\n}\n    .a[_ngcontent-%COMP%]{\n    color: #F08080;\n    text-decoration: none;\n    font-family: myFont;\n    font-size: 18px;\n}\n    .dropbtn[_ngcontent-%COMP%] {\n    background-color: transparent;\n        top: 7px;\n        padding-right: 5px;\n        color: #F08080;\n        font-family: myFont;\n        font-size: 20px;\n        border: none;\n  }\n    \n    .dropdown[_ngcontent-%COMP%] {\n    position: relative;\n    display: inline-block;\n  }\n    .dropdown2[_ngcontent-%COMP%] {\n    position: relative;\n    display: inline-block;\n    margin-top: 10px;\n    margin-left: 30px;\n  }\n    \n    .dropdown-content[_ngcontent-%COMP%] {\n    display: none;\n    position: absolute;\n    background-color: #FFE4E1;\n    font-family: myFont;\n    min-width: 160px;\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    z-index: 1;\n  }\n    \n    .dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    color: #F08080;\n    padding: 16px;\n    text-decoration: none;\n    display: block;\n  }\n    .logout[_ngcontent-%COMP%] {\n    \n    top: 11px;\n    right: 0px;\n    left: 87%;\n    position: absolute;\n    font-family: myFont;\n}\n    \n    .dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {background-color: rgb(83, 60, 60);}\n    \n    .dropdown[_ngcontent-%COMP%]:hover   .dropdown-content[_ngcontent-%COMP%] {display: block;}\n    \n    .dropdown[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {background-color: #FFE4E1;}\n    .btn[_ngcontent-%COMP%]{\n    display: inline-block;\n    padding: .75rem 1.25rem;\n    border-radius: 10rem;\n    transition: all .3s;\n    position: relative;\n    overflow: hidden;\n    z-index: 1;\n    font-family: myFont;\n    \n}\n    .btn[_ngcontent-%COMP%]::after {\n    content: '';\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color:  #c4a6e0;\n    border-radius: 10rem;\n    z-index: -2;\n}\n    .btn[_ngcontent-%COMP%]::before {\n    content: '';\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    width: 0%;\n    height: 100%;\n    background-color:  #754c9b;\n    transition: all .3s;\n    border-radius: 10rem;\n    z-index: -1;\n}\n    .btn[_ngcontent-%COMP%]:hover {\ncolor: #fff;\n}\n    .btn[_ngcontent-%COMP%]:hover::before {\nwidth: 100%;\n}\n    .bot[_ngcontent-%COMP%]{\n    font-family: myFont;\n    font-size: 35px;\n    bottom : 60%;\n    color : #F08080;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXJ0aXN0L2FydGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLFdBQVc7QUFDZjtJQUNJO0lBQ0Esd0JBQXdCO0lBQ3hCLHFCQUE0QjtJQUM1QjtJQUNBO0lBQ0Esd0JBQXdCO0lBQ3hCLGVBQWU7SUFDZixXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2Y7SUFFQTtJQUNBLG1CQUFtQjtJQUNuQix3QkFBK0I7SUFDL0I7SUFDQTtRQUNJLG1CQUFtQjtJQUN2QjtJQUNBO1FBQ0ksd0JBQXdCO1FBQ3hCLGlCQUFpQjtRQUNqQixtQkFBbUI7SUFDdkI7SUFDQTtRQUNJLGlCQUFpQjtRQUNqQixtQkFBbUI7UUFDbkIsZ0JBQWdCO0lBQ3BCO0lBQ0E7SUFDQSwyQ0FBbUQ7SUFDbkQsMkJBQTJCO0lBQzNCLGlCQUFpQjtJQUNqQjtJQUNKO0lBQ0ksV0FBVztJQUNYLFdBQVc7SUFDWCwyQkFBMkI7SUFDM0Isc0JBQXNCO0lBQ3RCLGlCQUFpQjtBQUNyQjtJQUNBO0lBQ0ksVUFBVTtJQUNWLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isa0NBQWtDO0lBQ2xDLDJCQUEyQjtJQUMzQixtQkFBbUI7QUFDdkI7SUFFQTtJQUNJLFNBQVM7SUFDVCxVQUFVO0lBQ1YsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsZUFBZTtBQUNuQjtJQUVBO0lBQ0ksY0FBYztJQUNkLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsZUFBZTtBQUNuQjtJQUNBO0lBQ0ksNkJBQTZCO1FBQ3pCLFFBQVE7UUFDUixrQkFBa0I7UUFDbEIsY0FBYztRQUNkLG1CQUFtQjtRQUNuQixlQUFlO1FBQ2YsWUFBWTtFQUNsQjtJQUVBLGtFQUFrRTtJQUNsRTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7RUFDdkI7SUFDQTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtFQUNuQjtJQUVBLHlDQUF5QztJQUN6QztJQUNFLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsNENBQTRDO0lBQzVDLFVBQVU7RUFDWjtJQUVBLDhCQUE4QjtJQUM5QjtJQUNFLGNBQWM7SUFDZCxhQUFhO0lBQ2IscUJBQXFCO0lBQ3JCLGNBQWM7RUFDaEI7SUFFQTtJQUNFLDBCQUEwQjtJQUMxQixTQUFTO0lBQ1QsVUFBVTtJQUNWLFNBQVM7SUFDVCxrQkFBa0I7SUFDbEIsbUJBQW1CO0FBQ3ZCO0lBRUUsNENBQTRDO0lBQzVDLDJCQUEyQixpQ0FBaUMsQ0FBQztJQUU3RCxvQ0FBb0M7SUFDcEMsbUNBQW1DLGNBQWMsQ0FBQztJQUVsRCwwRkFBMEY7SUFDMUYsMEJBQTBCLHlCQUF5QixDQUFDO0lBS3REO0lBQ0kscUJBQXFCO0lBQ3JCLHVCQUF1QjtJQUN2QixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLG1CQUFtQjs7QUFFdkI7SUFDQTtJQUNJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsU0FBUztJQUNULE9BQU87SUFDUCxXQUFXO0lBQ1gsWUFBWTtJQUNaLDBCQUEwQjtJQUMxQixvQkFBb0I7SUFDcEIsV0FBVztBQUNmO0lBQ0E7SUFDSSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxPQUFPO0lBQ1AsU0FBUztJQUNULFlBQVk7SUFDWiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixXQUFXO0FBQ2Y7SUFDQTtBQUNBLFdBQVc7QUFDWDtJQUNBO0FBQ0EsV0FBVztBQUNYO0lBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLFlBQVk7SUFDWixlQUFlO0FBQ25CO0lBQ0EsY0FBYztJQUVkLGdCQUFnQiIsImZpbGUiOiJzcmMvYXBwL2FydGlzdC9hcnRpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImh0bWwsYm9keVxue1xuICAgIG1hcmdpbjogMHB4O1xuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuICAgIEBmb250LWZhY2Uge1xuICAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udDtcbiAgICBzcmM6IHVybCguLi9mb250cy9mb250My50dGYpO1xuICAgIH1cbiAgICBoMSB7XG4gICAgZm9udC1mYW1pbHk6IG15Rmlyc3RGb250O1xuICAgIGZvbnQtc2l6ZTogODVweDtcbiAgICBib3R0b206IDVweDtcbiAgICB0ZXh0LWFsaWduIDogY2VudGVyO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBjb2xvciA6ICNGMDgwODA7XG4gICAgfVxuICAgIFxuICAgIEBmb250LWZhY2Uge1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgc3JjOiB1cmwoLi4vZm9udHMvZm9udEZvcm0udHRmKTtcbiAgICB9ICBcbiAgICBoMntcbiAgICAgICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICB9XG4gICAgaDN7XG4gICAgICAgIGNvbG9yOnJnYigyNDMsIDIzNCwgMjI0KTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgfVxuICAgIGg0e1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICAgICAgZm9udC1zaXplOiBsYXJnZTtcbiAgICB9XG4gICAgYm9keXtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vaW1hZ2VzL3ZhbmlsbGFfY2xvdWRzLmpwZyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgIH1cbi5oZXJve1xuICAgIGhlaWdodDoxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xufSBcbi50aXRsZS1ib3h7XG4gICAgd2lkdGg6IDk3JTtcbiAgICBoZWlnaHQ6IDEzNXB4O1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgIG1hcmdpbjo1JSBhdXRvO1xuICAgIGJhY2tncm91bmQ6ICNGRkU0RTE7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBib3gtc2hhZG93OiAwIDAgMjBweCA5cHggI2ZmNjEyNDFmO1xuICAgIGJvcmRlcjogMTBweCBvdXRzZXQgI0JDOEY4RjtcbiAgICBib3JkZXItcmFkaXVzOiA0MHB4O1xufVxuICAgICBcbi5iYWNrLXRvLXNlYXJjaCB7XG4gICAgdG9wOiAxMXB4O1xuICAgIGxlZnQ6IDEwcHg7XG4gICAgcmlnaHQ6IDg3JTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG59XG5cbi5he1xuICAgIGNvbG9yOiAjRjA4MDgwO1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbn1cbi5kcm9wYnRuIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgICAgdG9wOiA3cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICAgICAgY29sb3I6ICNGMDgwODA7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgYm9yZGVyOiBub25lO1xuICB9XG4gIFxuICAvKiBUaGUgY29udGFpbmVyIDxkaXY+IC0gbmVlZGVkIHRvIHBvc2l0aW9uIHRoZSBkcm9wZG93biBjb250ZW50ICovXG4gIC5kcm9wZG93biB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgfVxuICAuZHJvcGRvd24yIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDMwcHg7XG4gIH1cbiAgXG4gIC8qIERyb3Bkb3duIENvbnRlbnQgKEhpZGRlbiBieSBEZWZhdWx0KSAqL1xuICAuZHJvcGRvd24tY29udGVudCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRTRFMTtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIG1pbi13aWR0aDogMTYwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XG4gICAgei1pbmRleDogMTtcbiAgfVxuICBcbiAgLyogTGlua3MgaW5zaWRlIHRoZSBkcm9wZG93biAqL1xuICAuZHJvcGRvd24tY29udGVudCBhIHtcbiAgICBjb2xvcjogI0YwODA4MDtcbiAgICBwYWRkaW5nOiAxNnB4O1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5sb2dvdXQge1xuICAgIC8qIGJhY2tncm91bmQ6ICAjQ0Q1QzVDOyAqL1xuICAgIHRvcDogMTFweDtcbiAgICByaWdodDogMHB4O1xuICAgIGxlZnQ6IDg3JTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbn1cbiAgXG4gIC8qIENoYW5nZSBjb2xvciBvZiBkcm9wZG93biBsaW5rcyBvbiBob3ZlciAqL1xuICAuZHJvcGRvd24tY29udGVudCBhOmhvdmVyIHtiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoODMsIDYwLCA2MCk7fVxuICBcbiAgLyogU2hvdyB0aGUgZHJvcGRvd24gbWVudSBvbiBob3ZlciAqL1xuICAuZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge2Rpc3BsYXk6IGJsb2NrO31cbiAgXG4gIC8qIENoYW5nZSB0aGUgYmFja2dyb3VuZCBjb2xvciBvZiB0aGUgZHJvcGRvd24gYnV0dG9uIHdoZW4gdGhlIGRyb3Bkb3duIGNvbnRlbnQgaXMgc2hvd24gKi9cbiAgLmRyb3Bkb3duOmhvdmVyIC5kcm9wYnRuIHtiYWNrZ3JvdW5kLWNvbG9yOiAjRkZFNEUxO31cblxuXG5cblxuLmJ0bntcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgcGFkZGluZzogLjc1cmVtIDEuMjVyZW07XG4gICAgYm9yZGVyLXJhZGl1czogMTByZW07XG4gICAgdHJhbnNpdGlvbjogYWxsIC4zcztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB6LWluZGV4OiAxO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgXG59XG4uYnRuOjphZnRlciB7XG4gICAgY29udGVudDogJyc7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAgI2M0YTZlMDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHJlbTtcbiAgICB6LWluZGV4OiAtMjtcbn1cbi5idG46OmJlZm9yZSB7XG4gICAgY29udGVudDogJyc7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogICM3NTRjOWI7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4zcztcbiAgICBib3JkZXItcmFkaXVzOiAxMHJlbTtcbiAgICB6LWluZGV4OiAtMTtcbn1cbi5idG46aG92ZXIge1xuY29sb3I6ICNmZmY7XG59XG4uYnRuOmhvdmVyOjpiZWZvcmUge1xud2lkdGg6IDEwMCU7XG59XG4uYm90e1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgZm9udC1zaXplOiAzNXB4O1xuICAgIGJvdHRvbSA6IDYwJTtcbiAgICBjb2xvciA6ICNGMDgwODA7XG59XG4vKkxpa2UgYnV0dG9uKi9cblxuLyogL0xpa2UgYnV0dG9uKi8gIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ArtistComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-artist',
          templateUrl: './artist.component.html',
          styleUrls: ['artist.component.css']
        }]
      }], function () {
        return [{
          type: _service_deezer_service__WEBPACK_IMPORTED_MODULE_4__["DeezerApi"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]
        }, {
          type: _service_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/ch-email/ch-email.component.ts":
  /*!************************************************!*\
    !*** ./src/app/ch-email/ch-email.component.ts ***!
    \************************************************/

  /*! exports provided: ChEmailComponent */

  /***/
  function srcAppChEmailChEmailComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChEmailComponent", function () {
      return ChEmailComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _service_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../service/user.service */
    "./src/app/service/user.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function ChEmailComponent_p_26_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "E-mail already exists");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function ChEmailComponent_p_27_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "E-mail is not valid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function ChEmailComponent_p_29_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Password is not valid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var _c0 = function _c0() {
      return ["", "search"];
    };

    var _c1 = function _c1() {
      return ["/"];
    };

    var _c2 = function _c2() {
      return ["/change/username"];
    };

    var _c3 = function _c3() {
      return ["/change/password"];
    };

    var ChEmailComponent = /*#__PURE__*/function () {
      function ChEmailComponent(userService, formBuilder, router, http) {
        _classCallCheck(this, ChEmailComponent);

        this.userService = userService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.http = http;
        this.user = null;
        this.password_ = true;
        this.newEmail_ = true;
        this.user = userService.getUser();
        this.changeForm = this.formBuilder.group({
          newEmail: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
          password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]]
        });
      }

      _createClass(ChEmailComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "change",
        value: function change(data) {
          var _this7 = this;

          if (!this.changeForm.valid) {
            window.alert("Not valid!");
            return;
          }

          this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/change/email', data).subscribe(function (data) {
            _this7.router.navigateByUrl("/search");

            _this7.changeForm.reset();
          }, function (err) {
            console.log(err.error);
            _this7.password_ = true;
            _this7.newEmail_ = true;

            if (err.error == "Email already exists") {
              _this7.newEmail_ = false;
            }

            if (err.error == "Password is not correct") {
              _this7.password_ = false;
            }
          });
        }
      }, {
        key: "newEmail",
        value: function newEmail() {
          return this.changeForm.get('newEmail');
        }
      }]);

      return ChEmailComponent;
    }();

    ChEmailComponent.ɵfac = function ChEmailComponent_Factory(t) {
      return new (t || ChEmailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]));
    };

    ChEmailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ChEmailComponent,
      selectors: [["app-ch-email"]],
      decls: 32,
      vars: 14,
      consts: [[1, "hero"], [1, "title-box"], [1, "back-to-search"], [1, "a", 3, "routerLink"], [1, "logout"], [1, "dropdown"], [1, "dropbtn"], [1, "dropdown-content"], [3, "routerLink"], [1, "form-box"], [1, "button-box"], [1, "p"], ["id", "change", 1, "input-group", 3, "formGroup", "ngSubmit"], ["type", "email", "name", "newEmail", "formControlName", "newEmail", "placeholder", "new email", "required", "", 1, "input-field"], ["class", "error-msg", 4, "ngIf"], ["type", "password", "name", "password", "formControlName", "password", "placeholder", "password", "required", "", 1, "input-field"], ["type", "submit", 1, "submit-btn", 3, "disabled"], [1, "error-msg"]],
      template: function ChEmailComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "body");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Find your jam");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " Back to search page ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Log out");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Change username");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Change password");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Change e-mail");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "form", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ChEmailComponent_Template_form_ngSubmit_24_listener() {
            return ctx.change(ctx.changeForm.value);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, ChEmailComponent_p_26_Template, 2, 0, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, ChEmailComponent_p_27_Template, 2, 0, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "input", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, ChEmailComponent_p_29_Template, 2, 0, "p", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Change");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.user, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c3));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.changeForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.newEmail_);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.newEmail().errors == null ? null : ctx.newEmail().errors.email);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.password_);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.changeForm.valid);
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
      styles: ["*[_ngcontent-%COMP%]{\n    margin: 0;\n    padding: 0;\n}\n\n@font-face {\n    font-family: myFirstFont;\n    src: url('font3.ttf');\n}\n\nh1[_ngcontent-%COMP%] {\n    font-family: myFirstFont;\n    font-size: 85px;\n    bottom: 5px;\n    text-align : center;\n    position: relative;\n    color : #F08080;\n\n}\n\n@font-face {\n    font-family: myFont;\n    src: url('fontForm.ttf');\n}\n\nbody[_ngcontent-%COMP%]{\n    background-image: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)),url('note.jpg');\n    background-position: center;\n    background-size: auto;\n    position:relative;\n\n}\n\n.hero[_ngcontent-%COMP%]{\n    height:100%;\n    width: 100%;\n    background-position: center;\n    background-size: cover;\n    position:relative;\n}\n\n.title-box[_ngcontent-%COMP%]{\n    width: 97%;\n    height: 115px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    padding: 10px;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    border: 10px outset #BC8F8F;\n    border-radius: 40px;\n}\n\n.back-to-search[_ngcontent-%COMP%] {\n    top: 11px;\n    left: 10px;\n    right: 87%;\n    position: absolute;\n    font-family: myFont;\n    margin-top: 5px;\n}\n\n.a[_ngcontent-%COMP%]{\n    color: #F08080;\n    text-decoration: none;\n    font-family: myFont;\n    font-size: 18px;\n}\n\n.dropbtn[_ngcontent-%COMP%] {\n    background-color: transparent;\n    top: 11px;\n    right: 10px;\n    left: 87%;\n    color: #F08080;\n    font-family: myFont;\n    padding: 4px;\n    font-size: 20px;\n    border: none;\n  }\n\n\n\n.dropdown[_ngcontent-%COMP%] {\n    position: relative;\n    display: inline-block;\n    margin-top: 25px;\n    margin-right: 20px;\n  }\n\n\n\n.dropdown-content[_ngcontent-%COMP%] {\n    display: none;\n    position: absolute;\n    background-color: #FFE4E1;\n    font-family: myFont;\n    min-width: 160px;\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    z-index: 1;\n  }\n\n\n\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    color: #F08080;\n    padding: 16px;\n    text-decoration: none;\n    display: block;\n  }\n\n.logout[_ngcontent-%COMP%] {\n    \n    top: 11px;\n    right: 0px;\n    left: 87%;\n    position: absolute;\n    font-family: myFont;\n}\n\n\n\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {background-color: rgb(83, 60, 60);}\n\n\n\n.dropdown[_ngcontent-%COMP%]:hover   .dropdown-content[_ngcontent-%COMP%] {display: block;}\n\n\n\n.dropdown[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {background-color: #FFE4E1;}\n\n\n\n.p[_ngcontent-%COMP%]{\n    color: #F08080;\n    font-family: myFont;\n    margin-left: 40px;;\n}\n\n.form-box[_ngcontent-%COMP%]{\n    width: 380px;\n    height: 300px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    padding: 5px;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    font-family: myFont;\n    background-size: auto;\n    border-radius: 25px;\n    overflow: hidden;\n}\n\n.button-box[_ngcontent-%COMP%]{\n    width: 220px;\n    margin: 35px auto;\n    position: relative;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    border-radius: 30px;\n    font-family: myFont;\n}\n\n\n\n.input-group[_ngcontent-%COMP%]{\n    top:100px;\n    width:280px;\n    position: absolute;\n    transition: .5s;\n    font-family: myFont;\n\n}\n\n.input-field[_ngcontent-%COMP%]{\n    width: 100%;\n    padding:10px 0;\n    margin: 5px 0;\n    border-left:0;\n    border-right:0;\n    border-top: 0;\n    border-bottom: 1px solid #999;\n    outline: none;\n    background: transparent;\n    font-family: myFont;\n    margin-left: 40px;\n}\n\n.submit-btn[_ngcontent-%COMP%]{\n    width: 40%;\n    padding: 10px 30px;\n    cursor:pointer;\n    display: block;\n    margin: auto;\n    background:linear-gradient(to right, #CD5C5C,#FFE4C4);\n    border: 0;\n    outline: none;\n    border-radius:30px;\n    font-family: myFont;\n    margin-top: 20px;\n    margin-left: 130px;\n}\n\n.submit-btn[_ngcontent-%COMP%]:hover{\n    background:linear-gradient(to left, #CD5C5C,#FFE4C4);\n}\n\n.error-msg[_ngcontent-%COMP%]{\n    color:firebrick;\n    font-size: 10px;\n    margin-left: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2gtZW1haWwvY2gtZW1haWwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFNBQVM7SUFDVCxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIscUJBQTRCO0FBQ2hDOztBQUNBO0lBQ0ksd0JBQXdCO0lBQ3hCLGVBQWU7SUFDZixXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlOztBQUVuQjs7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQix3QkFBK0I7QUFDbkM7O0FBQ0E7SUFDSSxrRkFBMEY7SUFDMUYsMkJBQTJCO0lBQzNCLHFCQUFxQjtJQUNyQixpQkFBaUI7O0FBRXJCOztBQUNBO0lBQ0ksV0FBVztJQUNYLFdBQVc7SUFDWCwyQkFBMkI7SUFDM0Isc0JBQXNCO0lBQ3RCLGlCQUFpQjtBQUNyQjs7QUFDQTtJQUNJLFVBQVU7SUFDVixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLGtDQUFrQztJQUNsQywyQkFBMkI7SUFDM0IsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksU0FBUztJQUNULFVBQVU7SUFDVixVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0FBQ25COztBQUVBO0lBQ0ksY0FBYztJQUNkLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsZUFBZTtBQUNuQjs7QUFDQTtJQUNJLDZCQUE2QjtJQUM3QixTQUFTO0lBQ1QsV0FBVztJQUNYLFNBQVM7SUFDVCxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixlQUFlO0lBQ2YsWUFBWTtFQUNkOztBQUVBLGtFQUFrRTs7QUFDbEU7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQixrQkFBa0I7RUFDcEI7O0FBR0EseUNBQXlDOztBQUN6QztJQUNFLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsNENBQTRDO0lBQzVDLFVBQVU7RUFDWjs7QUFFQSw4QkFBOEI7O0FBQzlCO0lBQ0UsY0FBYztJQUNkLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsY0FBYztFQUNoQjs7QUFFQTtJQUNFLDBCQUEwQjtJQUMxQixTQUFTO0lBQ1QsVUFBVTtJQUNWLFNBQVM7SUFDVCxrQkFBa0I7SUFDbEIsbUJBQW1CO0FBQ3ZCOztBQUVFLDRDQUE0Qzs7QUFDNUMsMkJBQTJCLGlDQUFpQyxDQUFDOztBQUU3RCxvQ0FBb0M7O0FBQ3BDLG1DQUFtQyxjQUFjLENBQUM7O0FBRWxELDBGQUEwRjs7QUFDMUYsMEJBQTBCLHlCQUF5QixDQUFDOztBQUl0RCxtQkFBbUI7O0FBQ25CO0lBQ0ksY0FBYztJQUNkLG1CQUFtQjtJQUNuQixpQkFBaUI7QUFDckI7O0FBQ0E7SUFDSSxZQUFZO0lBQ1osYUFBYTtJQUNiLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixrQ0FBa0M7SUFDbEMsbUJBQW1CO0lBQ25CLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsa0NBQWtDO0lBQ2xDLG1CQUFtQjtJQUNuQixtQkFBbUI7QUFDdkI7O0FBRUE7Ozs7Ozs7OztHQVNHOztBQUNIO0lBQ0ksU0FBUztJQUNULFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLG1CQUFtQjs7QUFFdkI7O0FBQ0E7SUFDSSxXQUFXO0lBQ1gsY0FBYztJQUNkLGFBQWE7SUFDYixhQUFhO0lBQ2IsY0FBYztJQUNkLGFBQWE7SUFDYiw2QkFBNkI7SUFDN0IsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsaUJBQWlCO0FBQ3JCOztBQUNBO0lBQ0ksVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsY0FBYztJQUNkLFlBQVk7SUFDWixxREFBcUQ7SUFDckQsU0FBUztJQUNULGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixrQkFBa0I7QUFDdEI7O0FBQ0M7SUFDRyxvREFBb0Q7QUFDeEQ7O0FBQ0E7SUFDSSxlQUFlO0lBQ2YsZUFBZTtJQUNmLGlCQUFpQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL2NoLWVtYWlsL2NoLWVtYWlsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqe1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgICBmb250LWZhbWlseTogbXlGaXJzdEZvbnQ7XG4gICAgc3JjOiB1cmwoLi4vZm9udHMvZm9udDMudHRmKTtcbn1cbmgxIHtcbiAgICBmb250LWZhbWlseTogbXlGaXJzdEZvbnQ7XG4gICAgZm9udC1zaXplOiA4NXB4O1xuICAgIGJvdHRvbTogNXB4O1xuICAgIHRleHQtYWxpZ24gOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGNvbG9yIDogI0YwODA4MDtcblxufVxuQGZvbnQtZmFjZSB7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBzcmM6IHVybCguLi9mb250cy9mb250Rm9ybS50dGYpO1xufVxuYm9keXtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQocmdiYSgwLDAsMCwwLjQpLHJnYmEoMCwwLDAsMC40KSksdXJsKC4uL2ltYWdlcy9ub3RlLmpwZyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogYXV0bztcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcblxufVxuLmhlcm97XG4gICAgaGVpZ2h0OjEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgcG9zaXRpb246cmVsYXRpdmU7XG59XG4udGl0bGUtYm94e1xuICAgIHdpZHRoOiA5NyU7XG4gICAgaGVpZ2h0OiAxMTVweDtcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgICBtYXJnaW46NSUgYXV0bztcbiAgICBiYWNrZ3JvdW5kOiAjRkZFNEUxO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgYm94LXNoYWRvdzogMCAwIDIwcHggOXB4ICNmZjYxMjQxZjtcbiAgICBib3JkZXI6IDEwcHggb3V0c2V0ICNCQzhGOEY7XG4gICAgYm9yZGVyLXJhZGl1czogNDBweDtcbn1cbiAgICAgXG4uYmFjay10by1zZWFyY2gge1xuICAgIHRvcDogMTFweDtcbiAgICBsZWZ0OiAxMHB4O1xuICAgIHJpZ2h0OiA4NyU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4uYXtcbiAgICBjb2xvcjogI0YwODA4MDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBmb250LXNpemU6IDE4cHg7XG59XG4uZHJvcGJ0biB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgdG9wOiAxMXB4O1xuICAgIHJpZ2h0OiAxMHB4O1xuICAgIGxlZnQ6IDg3JTtcbiAgICBjb2xvcjogI0YwODA4MDtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIHBhZGRpbmc6IDRweDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgYm9yZGVyOiBub25lO1xuICB9XG4gIFxuICAvKiBUaGUgY29udGFpbmVyIDxkaXY+IC0gbmVlZGVkIHRvIHBvc2l0aW9uIHRoZSBkcm9wZG93biBjb250ZW50ICovXG4gIC5kcm9wZG93biB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xuICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgfVxuICBcbiAgXG4gIC8qIERyb3Bkb3duIENvbnRlbnQgKEhpZGRlbiBieSBEZWZhdWx0KSAqL1xuICAuZHJvcGRvd24tY29udGVudCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRTRFMTtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIG1pbi13aWR0aDogMTYwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XG4gICAgei1pbmRleDogMTtcbiAgfVxuICBcbiAgLyogTGlua3MgaW5zaWRlIHRoZSBkcm9wZG93biAqL1xuICAuZHJvcGRvd24tY29udGVudCBhIHtcbiAgICBjb2xvcjogI0YwODA4MDtcbiAgICBwYWRkaW5nOiAxNnB4O1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5sb2dvdXQge1xuICAgIC8qIGJhY2tncm91bmQ6ICAjQ0Q1QzVDOyAqL1xuICAgIHRvcDogMTFweDtcbiAgICByaWdodDogMHB4O1xuICAgIGxlZnQ6IDg3JTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbn1cbiAgXG4gIC8qIENoYW5nZSBjb2xvciBvZiBkcm9wZG93biBsaW5rcyBvbiBob3ZlciAqL1xuICAuZHJvcGRvd24tY29udGVudCBhOmhvdmVyIHtiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoODMsIDYwLCA2MCk7fVxuICBcbiAgLyogU2hvdyB0aGUgZHJvcGRvd24gbWVudSBvbiBob3ZlciAqL1xuICAuZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge2Rpc3BsYXk6IGJsb2NrO31cbiAgXG4gIC8qIENoYW5nZSB0aGUgYmFja2dyb3VuZCBjb2xvciBvZiB0aGUgZHJvcGRvd24gYnV0dG9uIHdoZW4gdGhlIGRyb3Bkb3duIGNvbnRlbnQgaXMgc2hvd24gKi9cbiAgLmRyb3Bkb3duOmhvdmVyIC5kcm9wYnRuIHtiYWNrZ3JvdW5kLWNvbG9yOiAjRkZFNEUxO31cblxuXG5cbi8qIGRyb3AtZG93biBtZW51ICovXG4ucHtcbiAgICBjb2xvcjogI0YwODA4MDtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIG1hcmdpbi1sZWZ0OiA0MHB4Oztcbn1cbi5mb3JtLWJveHtcbiAgICB3aWR0aDogMzgwcHg7XG4gICAgaGVpZ2h0OiAzMDBweDtcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgICBtYXJnaW46NSUgYXV0bztcbiAgICBiYWNrZ3JvdW5kOiAjRkZFNEUxO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3gtc2hhZG93OiAwIDAgMjBweCA5cHggI2ZmNjEyNDFmO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBhdXRvO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5idXR0b24tYm94e1xuICAgIHdpZHRoOiAyMjBweDtcbiAgICBtYXJnaW46IDM1cHggYXV0bztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgYm94LXNoYWRvdzogMCAwIDIwcHggOXB4ICNmZjYxMjQxZjtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG59XG5cbi8qICNidG57XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6MDtcbiAgICBwb3NpdGlvbjphYnNvbHV0ZTtcbiAgICB3aWR0aDoxMTBweDtcbiAgICBoZWlnaHQ6MTAwJTtcbiAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI0NENUM1QywjRkZFNEM0KTtcbiAgICBib3JkZXItcmFkaXVzOjMwcHg7XG4gICAgdHJhbnNpdGlvbjogLjVzO1xufSAqL1xuLmlucHV0LWdyb3Vwe1xuICAgIHRvcDoxMDBweDtcbiAgICB3aWR0aDoyODBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdHJhbnNpdGlvbjogLjVzO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG5cbn1cbi5pbnB1dC1maWVsZHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOjEwcHggMDtcbiAgICBtYXJnaW46IDVweCAwO1xuICAgIGJvcmRlci1sZWZ0OjA7XG4gICAgYm9yZGVyLXJpZ2h0OjA7XG4gICAgYm9yZGVyLXRvcDogMDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzk5OTtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgbWFyZ2luLWxlZnQ6IDQwcHg7XG59XG4uc3VibWl0LWJ0bntcbiAgICB3aWR0aDogNDAlO1xuICAgIHBhZGRpbmc6IDEwcHggMzBweDtcbiAgICBjdXJzb3I6cG9pbnRlcjtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtYXJnaW46IGF1dG87XG4gICAgYmFja2dyb3VuZDpsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNDRDVDNUMsI0ZGRTRDNCk7XG4gICAgYm9yZGVyOiAwO1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czozMHB4O1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBtYXJnaW4tbGVmdDogMTMwcHg7XG59XG4gLnN1Ym1pdC1idG46aG92ZXJ7XG4gICAgYmFja2dyb3VuZDpsaW5lYXItZ3JhZGllbnQodG8gbGVmdCwgI0NENUM1QywjRkZFNEM0KTtcbn0gXG4uZXJyb3ItbXNne1xuICAgIGNvbG9yOmZpcmVicmljaztcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDQwcHg7XG59XG5cblxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ChEmailComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-ch-email',
          templateUrl: './ch-email.component.html',
          styleUrls: ['./ch-email.component.css']
        }]
      }], function () {
        return [{
          type: _service_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/ch-password/ch-password.component.ts":
  /*!******************************************************!*\
    !*** ./src/app/ch-password/ch-password.component.ts ***!
    \******************************************************/

  /*! exports provided: ChPasswordComponent */

  /***/
  function srcAppChPasswordChPasswordComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChPasswordComponent", function () {
      return ChPasswordComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _service_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../service/user.service */
    "./src/app/service/user.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function ChPasswordComponent_p_27_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Password is not correct");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function ChPasswordComponent_p_29_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Password must be at least 5 characters long");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var _c0 = function _c0() {
      return ["", "search"];
    };

    var _c1 = function _c1() {
      return ["/"];
    };

    var _c2 = function _c2() {
      return ["/change/username"];
    };

    var _c3 = function _c3() {
      return ["/change/email"];
    };

    var ChPasswordComponent = /*#__PURE__*/function () {
      function ChPasswordComponent(userService, formBuilder, router, http) {
        _classCallCheck(this, ChPasswordComponent);

        this.userService = userService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.http = http;
        this.user = null;
        this.newPassword_ = true;
        this.oldPassword_ = true;
        this.user = userService.getUser();
        this.changeForm = this.formBuilder.group({
          newPassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5)]],
          oldPassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]]
        });
      }

      _createClass(ChPasswordComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "change",
        value: function change(data) {
          var _this8 = this;

          if (!this.changeForm.valid) {
            window.alert("Not valid!");
            return;
          }

          this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/change/password', data).subscribe(function (data) {
            _this8.router.navigateByUrl("/search");

            _this8.changeForm.reset();
          }, function (err) {
            console.log(err.error);
            _this8.newPassword_ = true;
            _this8.oldPassword_ = true;

            if (err.error == "Password is not correct") {
              _this8.oldPassword_ = false;
            }
          });
        }
      }, {
        key: "oldPassword",
        value: function oldPassword() {
          return this.changeForm.get('oldPassword');
        }
      }, {
        key: "newPassword",
        value: function newPassword() {
          return this.changeForm.get('newPassword');
        }
      }]);

      return ChPasswordComponent;
    }();

    ChPasswordComponent.ɵfac = function ChPasswordComponent_Factory(t) {
      return new (t || ChPasswordComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]));
    };

    ChPasswordComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ChPasswordComponent,
      selectors: [["app-ch-password"]],
      decls: 32,
      vars: 13,
      consts: [[1, "hero"], [1, "title-box"], [1, "back-to-search"], [1, "a", 3, "routerLink"], [1, "logout"], [1, "dropdown"], [1, "dropbtn"], [1, "dropdown-content"], [3, "routerLink"], [1, "form-box"], [1, "button-box"], [1, "p"], ["id", "change", 1, "input-group", 3, "formGroup", "ngSubmit"], ["type", "password", "name", "oldPassword", "formControlName", "oldPassword", "placeholder", "old password", "required", "", 1, "input-field"], ["newUsername", ""], ["class", "error-msg", 4, "ngIf"], ["type", "password", "name", "newPassword", "formControlName", "newPassword", "placeholder", "new password", "required", "", 1, "input-field"], ["type", "submit", 1, "submit-btn", 3, "disabled"], [1, "error-msg"]],
      template: function ChPasswordComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "body");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Find your jam");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " Back to search page ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Log out");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Change username");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Change e-mail");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Change password");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "form", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ChPasswordComponent_Template_form_ngSubmit_24_listener() {
            return ctx.change(ctx.changeForm.value);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 13, 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, ChPasswordComponent_p_27_Template, 2, 0, "p", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "input", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, ChPasswordComponent_p_29_Template, 2, 0, "p", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Change");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](9, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.user, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c3));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.changeForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.oldPassword_);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.newPassword().errors == null ? null : ctx.newPassword().errors.minlength);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.changeForm.valid);
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
      styles: ["*[_ngcontent-%COMP%]{\n    margin: 0;\n    padding: 0;\n}\n\n@font-face {\n    font-family: myFirstFont;\n    src: url('font3.ttf');\n}\n\nh1[_ngcontent-%COMP%] {\n    font-family: myFirstFont;\n    font-size: 85px;\n    bottom: 5px;\n    text-align : center;\n    position: relative;\n    color : #F08080;\n\n}\n\n@font-face {\n    font-family: myFont;\n    src: url('fontForm.ttf');\n}\n\nbody[_ngcontent-%COMP%]{\n    background-image: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)),url('note.jpg');\n    background-position: center;\n    background-size: auto;\n    position:relative;\n\n}\n\n.hero[_ngcontent-%COMP%]{\n    height:100%;\n    width: 100%;\n    background-position: center;\n    background-size: cover;\n    position:relative;\n}\n\n.title-box[_ngcontent-%COMP%]{\n    width: 97%;\n    height: 115px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    padding: 10px;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    border: 10px outset #BC8F8F;\n    border-radius: 40px;\n}\n\n.back-to-search[_ngcontent-%COMP%] {\n    top: 11px;\n    left: 10px;\n    right: 87%;\n    position: absolute;\n    font-family: myFont;\n    margin-top: 5px;\n}\n\n.a[_ngcontent-%COMP%]{\n    color: #F08080;\n    text-decoration: none;\n    font-family: myFont;\n    font-size: 18px;\n}\n\n.dropbtn[_ngcontent-%COMP%] {\n    background-color: transparent;\n    top: 11px;\n    right: 10px;\n    left: 87%;\n    color: #F08080;\n    font-family: myFont;\n    padding: 4px;\n    font-size: 20px;\n    border: none;\n  }\n\n\n\n.dropdown[_ngcontent-%COMP%] {\n    position: relative;\n    display: inline-block;\n    margin-top: 25px;\n    margin-right: 20px;\n  }\n\n\n\n.dropdown-content[_ngcontent-%COMP%] {\n    display: none;\n    position: absolute;\n    background-color: #FFE4E1;\n    font-family: myFont;\n    min-width: 160px;\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    z-index: 1;\n  }\n\n\n\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    color: #F08080;\n    padding: 16px;\n    text-decoration: none;\n    display: block;\n  }\n\n.logout[_ngcontent-%COMP%] {\n    \n    top: 11px;\n    right: 0px;\n    left: 87%;\n    position: absolute;\n    font-family: myFont;\n}\n\n\n\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {background-color: rgb(83, 60, 60);}\n\n\n\n.dropdown[_ngcontent-%COMP%]:hover   .dropdown-content[_ngcontent-%COMP%] {display: block;}\n\n\n\n.dropdown[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {background-color: #FFE4E1;}\n\n\n\n.p[_ngcontent-%COMP%]{\n    color: #F08080;\n    font-family: myFont;\n    margin-left: 40px;;\n}\n\n.form-box[_ngcontent-%COMP%]{\n    width: 380px;\n    height: 300px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    padding: 5px;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    font-family: myFont;\n    background-size: auto;\n    border-radius: 25px;\n    overflow: hidden;\n}\n\n.button-box[_ngcontent-%COMP%]{\n    width: 220px;\n    margin: 35px auto;\n    position: relative;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    border-radius: 30px;\n    font-family: myFont;\n}\n\n\n\n.input-group[_ngcontent-%COMP%]{\n    top:100px;\n    width:280px;\n    position: absolute;\n    transition: .5s;\n    font-family: myFont;\n\n}\n\n.input-field[_ngcontent-%COMP%]{\n    width: 100%;\n    padding:10px 0;\n    margin: 5px 0;\n    border-left:0;\n    border-right:0;\n    border-top: 0;\n    border-bottom: 1px solid #999;\n    outline: none;\n    background: transparent;\n    font-family: myFont;\n    margin-left: 40px;\n}\n\n.submit-btn[_ngcontent-%COMP%]{\n    width: 40%;\n    padding: 10px 30px;\n    cursor:pointer;\n    display: block;\n    margin: auto;\n    background:linear-gradient(to right, #CD5C5C,#FFE4C4);\n    border: 0;\n    outline: none;\n    border-radius:30px;\n    font-family: myFont;\n    margin-top: 20px;\n    margin-left: 130px;\n}\n\n.submit-btn[_ngcontent-%COMP%]:hover{\n    background:linear-gradient(to left, #CD5C5C,#FFE4C4);\n}\n\n.error-msg[_ngcontent-%COMP%]{\n    color:firebrick;\n    font-size: 10px;\n    margin-left: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2gtcGFzc3dvcmQvY2gtcGFzc3dvcmQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFNBQVM7SUFDVCxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIscUJBQTRCO0FBQ2hDOztBQUNBO0lBQ0ksd0JBQXdCO0lBQ3hCLGVBQWU7SUFDZixXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlOztBQUVuQjs7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQix3QkFBK0I7QUFDbkM7O0FBQ0E7SUFDSSxrRkFBMEY7SUFDMUYsMkJBQTJCO0lBQzNCLHFCQUFxQjtJQUNyQixpQkFBaUI7O0FBRXJCOztBQUNBO0lBQ0ksV0FBVztJQUNYLFdBQVc7SUFDWCwyQkFBMkI7SUFDM0Isc0JBQXNCO0lBQ3RCLGlCQUFpQjtBQUNyQjs7QUFDQTtJQUNJLFVBQVU7SUFDVixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLGtDQUFrQztJQUNsQywyQkFBMkI7SUFDM0IsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksU0FBUztJQUNULFVBQVU7SUFDVixVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0FBQ25COztBQUVBO0lBQ0ksY0FBYztJQUNkLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsZUFBZTtBQUNuQjs7QUFDQTtJQUNJLDZCQUE2QjtJQUM3QixTQUFTO0lBQ1QsV0FBVztJQUNYLFNBQVM7SUFDVCxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixlQUFlO0lBQ2YsWUFBWTtFQUNkOztBQUVBLGtFQUFrRTs7QUFDbEU7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQixrQkFBa0I7RUFDcEI7O0FBR0EseUNBQXlDOztBQUN6QztJQUNFLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsNENBQTRDO0lBQzVDLFVBQVU7RUFDWjs7QUFFQSw4QkFBOEI7O0FBQzlCO0lBQ0UsY0FBYztJQUNkLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsY0FBYztFQUNoQjs7QUFFQTtJQUNFLDBCQUEwQjtJQUMxQixTQUFTO0lBQ1QsVUFBVTtJQUNWLFNBQVM7SUFDVCxrQkFBa0I7SUFDbEIsbUJBQW1CO0FBQ3ZCOztBQUVFLDRDQUE0Qzs7QUFDNUMsMkJBQTJCLGlDQUFpQyxDQUFDOztBQUU3RCxvQ0FBb0M7O0FBQ3BDLG1DQUFtQyxjQUFjLENBQUM7O0FBRWxELDBGQUEwRjs7QUFDMUYsMEJBQTBCLHlCQUF5QixDQUFDOztBQUt0RCxtQkFBbUI7O0FBQ25CO0lBQ0ksY0FBYztJQUNkLG1CQUFtQjtJQUNuQixpQkFBaUI7QUFDckI7O0FBQ0E7SUFDSSxZQUFZO0lBQ1osYUFBYTtJQUNiLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixrQ0FBa0M7SUFDbEMsbUJBQW1CO0lBQ25CLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsa0NBQWtDO0lBQ2xDLG1CQUFtQjtJQUNuQixtQkFBbUI7QUFDdkI7O0FBRUE7Ozs7Ozs7OztHQVNHOztBQUNIO0lBQ0ksU0FBUztJQUNULFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLG1CQUFtQjs7QUFFdkI7O0FBQ0E7SUFDSSxXQUFXO0lBQ1gsY0FBYztJQUNkLGFBQWE7SUFDYixhQUFhO0lBQ2IsY0FBYztJQUNkLGFBQWE7SUFDYiw2QkFBNkI7SUFDN0IsYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsaUJBQWlCO0FBQ3JCOztBQUNBO0lBQ0ksVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsY0FBYztJQUNkLFlBQVk7SUFDWixxREFBcUQ7SUFDckQsU0FBUztJQUNULGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixrQkFBa0I7QUFDdEI7O0FBQ0E7SUFDSSxvREFBb0Q7QUFDeEQ7O0FBQ0E7SUFDSSxlQUFlO0lBQ2YsZUFBZTtJQUNmLGlCQUFpQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL2NoLXBhc3N3b3JkL2NoLXBhc3N3b3JkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqe1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nOiAwO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgICBmb250LWZhbWlseTogbXlGaXJzdEZvbnQ7XG4gICAgc3JjOiB1cmwoLi4vZm9udHMvZm9udDMudHRmKTtcbn1cbmgxIHtcbiAgICBmb250LWZhbWlseTogbXlGaXJzdEZvbnQ7XG4gICAgZm9udC1zaXplOiA4NXB4O1xuICAgIGJvdHRvbTogNXB4O1xuICAgIHRleHQtYWxpZ24gOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGNvbG9yIDogI0YwODA4MDtcblxufVxuQGZvbnQtZmFjZSB7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBzcmM6IHVybCguLi9mb250cy9mb250Rm9ybS50dGYpO1xufVxuYm9keXtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQocmdiYSgwLDAsMCwwLjQpLHJnYmEoMCwwLDAsMC40KSksdXJsKC4uL2ltYWdlcy9ub3RlLmpwZyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogYXV0bztcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcblxufVxuLmhlcm97XG4gICAgaGVpZ2h0OjEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgcG9zaXRpb246cmVsYXRpdmU7XG59XG4udGl0bGUtYm94e1xuICAgIHdpZHRoOiA5NyU7XG4gICAgaGVpZ2h0OiAxMTVweDtcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgICBtYXJnaW46NSUgYXV0bztcbiAgICBiYWNrZ3JvdW5kOiAjRkZFNEUxO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgYm94LXNoYWRvdzogMCAwIDIwcHggOXB4ICNmZjYxMjQxZjtcbiAgICBib3JkZXI6IDEwcHggb3V0c2V0ICNCQzhGOEY7XG4gICAgYm9yZGVyLXJhZGl1czogNDBweDtcbn1cbiAgICAgXG4uYmFjay10by1zZWFyY2gge1xuICAgIHRvcDogMTFweDtcbiAgICBsZWZ0OiAxMHB4O1xuICAgIHJpZ2h0OiA4NyU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4uYXtcbiAgICBjb2xvcjogI0YwODA4MDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBmb250LXNpemU6IDE4cHg7XG59XG4uZHJvcGJ0biB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgdG9wOiAxMXB4O1xuICAgIHJpZ2h0OiAxMHB4O1xuICAgIGxlZnQ6IDg3JTtcbiAgICBjb2xvcjogI0YwODA4MDtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIHBhZGRpbmc6IDRweDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgYm9yZGVyOiBub25lO1xuICB9XG4gIFxuICAvKiBUaGUgY29udGFpbmVyIDxkaXY+IC0gbmVlZGVkIHRvIHBvc2l0aW9uIHRoZSBkcm9wZG93biBjb250ZW50ICovXG4gIC5kcm9wZG93biB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xuICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgfVxuICBcbiAgXG4gIC8qIERyb3Bkb3duIENvbnRlbnQgKEhpZGRlbiBieSBEZWZhdWx0KSAqL1xuICAuZHJvcGRvd24tY29udGVudCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRTRFMTtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIG1pbi13aWR0aDogMTYwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XG4gICAgei1pbmRleDogMTtcbiAgfVxuICBcbiAgLyogTGlua3MgaW5zaWRlIHRoZSBkcm9wZG93biAqL1xuICAuZHJvcGRvd24tY29udGVudCBhIHtcbiAgICBjb2xvcjogI0YwODA4MDtcbiAgICBwYWRkaW5nOiAxNnB4O1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5sb2dvdXQge1xuICAgIC8qIGJhY2tncm91bmQ6ICAjQ0Q1QzVDOyAqL1xuICAgIHRvcDogMTFweDtcbiAgICByaWdodDogMHB4O1xuICAgIGxlZnQ6IDg3JTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbn1cbiAgXG4gIC8qIENoYW5nZSBjb2xvciBvZiBkcm9wZG93biBsaW5rcyBvbiBob3ZlciAqL1xuICAuZHJvcGRvd24tY29udGVudCBhOmhvdmVyIHtiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoODMsIDYwLCA2MCk7fVxuICBcbiAgLyogU2hvdyB0aGUgZHJvcGRvd24gbWVudSBvbiBob3ZlciAqL1xuICAuZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge2Rpc3BsYXk6IGJsb2NrO31cbiAgXG4gIC8qIENoYW5nZSB0aGUgYmFja2dyb3VuZCBjb2xvciBvZiB0aGUgZHJvcGRvd24gYnV0dG9uIHdoZW4gdGhlIGRyb3Bkb3duIGNvbnRlbnQgaXMgc2hvd24gKi9cbiAgLmRyb3Bkb3duOmhvdmVyIC5kcm9wYnRuIHtiYWNrZ3JvdW5kLWNvbG9yOiAjRkZFNEUxO31cblxuXG5cblxuLyogZHJvcC1kb3duIG1lbnUgKi9cbi5we1xuICAgIGNvbG9yOiAjRjA4MDgwO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgbWFyZ2luLWxlZnQ6IDQwcHg7O1xufVxuLmZvcm0tYm94e1xuICAgIHdpZHRoOiAzODBweDtcbiAgICBoZWlnaHQ6IDMwMHB4O1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgIG1hcmdpbjo1JSBhdXRvO1xuICAgIGJhY2tncm91bmQ6ICNGRkU0RTE7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4IDlweCAjZmY2MTI0MWY7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGF1dG87XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmJ1dHRvbi1ib3h7XG4gICAgd2lkdGg6IDIyMHB4O1xuICAgIG1hcmdpbjogMzVweCBhdXRvO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBib3gtc2hhZG93OiAwIDAgMjBweCA5cHggI2ZmNjEyNDFmO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbn1cblxuLyogI2J0bntcbiAgICB0b3A6IDA7XG4gICAgbGVmdDowO1xuICAgIHBvc2l0aW9uOmFic29sdXRlO1xuICAgIHdpZHRoOjExMHB4O1xuICAgIGhlaWdodDoxMDAlO1xuICAgIGJhY2tncm91bmQ6bGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjQ0Q1QzVDLCNGRkU0QzQpO1xuICAgIGJvcmRlci1yYWRpdXM6MzBweDtcbiAgICB0cmFuc2l0aW9uOiAuNXM7XG59ICovXG4uaW5wdXQtZ3JvdXB7XG4gICAgdG9wOjEwMHB4O1xuICAgIHdpZHRoOjI4MHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0cmFuc2l0aW9uOiAuNXM7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcblxufVxuLmlucHV0LWZpZWxke1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6MTBweCAwO1xuICAgIG1hcmdpbjogNXB4IDA7XG4gICAgYm9yZGVyLWxlZnQ6MDtcbiAgICBib3JkZXItcmlnaHQ6MDtcbiAgICBib3JkZXItdG9wOiAwO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjOTk5O1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBtYXJnaW4tbGVmdDogNDBweDtcbn1cbi5zdWJtaXQtYnRue1xuICAgIHdpZHRoOiA0MCU7XG4gICAgcGFkZGluZzogMTBweCAzMHB4O1xuICAgIGN1cnNvcjpwb2ludGVyO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI0NENUM1QywjRkZFNEM0KTtcbiAgICBib3JkZXI6IDA7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOjMwcHg7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMzBweDtcbn1cbi5zdWJtaXQtYnRuOmhvdmVye1xuICAgIGJhY2tncm91bmQ6bGluZWFyLWdyYWRpZW50KHRvIGxlZnQsICNDRDVDNUMsI0ZGRTRDNCk7XG59IFxuLmVycm9yLW1zZ3tcbiAgICBjb2xvcjpmaXJlYnJpY2s7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiA0MHB4O1xufVxuXG5cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ChPasswordComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-ch-password',
          templateUrl: './ch-password.component.html',
          styleUrls: ['./ch-password.component.css']
        }]
      }], function () {
        return [{
          type: _service_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/ch-username/ch-username.component.ts":
  /*!******************************************************!*\
    !*** ./src/app/ch-username/ch-username.component.ts ***!
    \******************************************************/

  /*! exports provided: ChUsernameComponent */

  /***/
  function srcAppChUsernameChUsernameComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChUsernameComponent", function () {
      return ChUsernameComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _service_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../service/user.service */
    "./src/app/service/user.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    var _c0 = ["newUsername"];

    function ChUsernameComponent_p_27_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Username already exists");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function ChUsernameComponent_p_29_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Password is not valid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var _c1 = function _c1() {
      return ["", "search"];
    };

    var _c2 = function _c2() {
      return ["/"];
    };

    var _c3 = function _c3() {
      return ["/change/password"];
    };

    var _c4 = function _c4() {
      return ["/change/email"];
    };

    var ChUsernameComponent = /*#__PURE__*/function () {
      function ChUsernameComponent(userService, formBuilder, router, http) {
        _classCallCheck(this, ChUsernameComponent);

        this.userService = userService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.http = http;
        this.user = null;
        this.password_ = true;
        this.newUsername_ = true;
        this.user = userService.getUser();
        this.changeForm = this.formBuilder.group({
          newUsername: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
          password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]]
        });
      }

      _createClass(ChUsernameComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "change",
        value: function change(data) {
          var _this9 = this;

          if (!this.changeForm.valid) {
            window.alert("Not valid!");
            return;
          }

          this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/change/username', data).subscribe(function (data) {
            var user = _this9.newUsernameInput.nativeElement.value;

            _this9.userService.setUser(user);

            _this9.router.navigateByUrl("/search");

            _this9.changeForm.reset();
          }, function (err) {
            console.log(err.error);
            _this9.password_ = true;
            _this9.newUsername_ = true;

            if (err.error == "Username already exists") {
              _this9.newUsername_ = false;
            }

            if (err.error == "Password is not correct") {
              _this9.password_ = false;
            }
          });
        }
      }, {
        key: "newUsername",
        value: function newUsername() {
          return this.changeForm.get('newUsername');
        }
      }, {
        key: "password",
        value: function password() {
          return this.changeForm.get('password');
        }
      }]);

      return ChUsernameComponent;
    }();

    ChUsernameComponent.ɵfac = function ChUsernameComponent_Factory(t) {
      return new (t || ChUsernameComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]));
    };

    ChUsernameComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ChUsernameComponent,
      selectors: [["app-ch-username"]],
      viewQuery: function ChUsernameComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.newUsernameInput = _t.first);
        }
      },
      decls: 32,
      vars: 13,
      consts: [[1, "hero"], [1, "title-box"], [1, "back-to-search"], [1, "a", 3, "routerLink"], [1, "logout"], [1, "dropdown"], [1, "dropbtn"], [1, "dropdown-content"], [3, "routerLink"], [1, "form-box"], [1, "button-box"], [1, "p"], ["id", "change", 1, "input-group", 3, "formGroup", "ngSubmit"], ["type", "text", "name", "newUsername", "formControlName", "newUsername", "placeholder", "new username", "required", "", 1, "input-field"], ["newUsername", ""], ["class", "error-msg", 4, "ngIf"], ["type", "password", "name", "password", "formControlName", "password", "placeholder", "password", "required", "", 1, "input-field"], ["type", "submit", 1, "submit-btn", 3, "disabled"], [1, "error-msg"]],
      template: function ChUsernameComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "body");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Find your jam");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " Back to search page ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Log out");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Change password");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Change e-mail");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Change username");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "form", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ChUsernameComponent_Template_form_ngSubmit_24_listener() {
            return ctx.change(ctx.changeForm.value);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 13, 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, ChUsernameComponent_p_27_Template, 2, 0, "p", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "input", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, ChUsernameComponent_p_29_Template, 2, 0, "p", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Change");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](9, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.user, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c3));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c4));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.changeForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.newUsername_);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.password_);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.changeForm.valid);
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
      styles: ["*[_ngcontent-%COMP%]{\n    margin: 0;\n    padding: 0;\n}\n\n@font-face {\n    font-family: myFirstFont;\n    src: url('font3.ttf');\n}\n\nh1[_ngcontent-%COMP%] {\n    font-family: myFirstFont;\n    font-size: 85px;\n    bottom: 5px;\n    text-align : center;\n    position: relative;\n    color : #F08080;\n}\n\n@font-face {\n    font-family: myFont;\n    src: url('fontForm.ttf');\n}\n\nbody[_ngcontent-%COMP%]{\n    background-image: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)),url('note.jpg');\n    background-position: center;\n    background-size: auto;\n    position:relative;\n\n}\n\n.hero[_ngcontent-%COMP%]{\n    height:100%;\n    width: 100%;\n    background-position: center;\n    background-size: cover;\n    position:relative;\n}\n\n.title-box[_ngcontent-%COMP%]{\n    width: 97%;\n    height: 115px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    padding: 10px;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    border: 10px outset #BC8F8F;\n    border-radius: 40px;\n}\n\n.back-to-search[_ngcontent-%COMP%] {\n    top: 11px;\n    left: 10px;\n    right: 87%;\n    position: absolute;\n    font-family: myFont;\n    margin-top: 5px;\n}\n\n.a[_ngcontent-%COMP%]{\n    color: #F08080;\n    text-decoration: none;\n    font-family: myFont;\n    font-size: 18px;\n}\n\n.dropbtn[_ngcontent-%COMP%] {\n    background-color: transparent;\n    top: 11px;\n    right: 10px;\n    left: 87%;\n    color: #F08080;\n    font-family: myFont;\n    padding: 4px;\n    font-size: 20px;\n    border: none;\n  }\n\n\n\n.dropdown[_ngcontent-%COMP%] {\n    position: relative;\n    display: inline-block;\n    margin-top: 25px;\n    margin-right: 20px;\n  }\n\n\n\n.dropdown-content[_ngcontent-%COMP%] {\n    display: none;\n    position: absolute;\n    background-color: #FFE4E1;\n    font-family: myFont;\n    min-width: 160px;\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    z-index: 1;\n  }\n\n\n\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    color: #F08080;\n    padding: 16px;\n    text-decoration: none;\n    display: block;\n  }\n\n.logout[_ngcontent-%COMP%] {\n    \n    top: 11px;\n    right: 0px;\n    left: 87%;\n    position: absolute;\n    font-family: myFont;\n}\n\n\n\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {background-color: rgb(83, 60, 60);}\n\n\n\n.dropdown[_ngcontent-%COMP%]:hover   .dropdown-content[_ngcontent-%COMP%] {display: block;}\n\n\n\n.dropdown[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {background-color: #FFE4E1;}\n\n\n\n\n\n.p[_ngcontent-%COMP%]{\n    color: #F08080;\n    font-family: myFont;\n    margin-left: 40px;;\n}\n\n.form-box[_ngcontent-%COMP%]{\n    width: 380px;\n    height: 300px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    padding: 5px;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    font-family: myFont;\n    background-size: auto;\n    border-radius: 25px;\n    overflow: hidden;\n}\n\n.button-box[_ngcontent-%COMP%]{\n    width: 220px;\n    margin: 35px auto;\n    position: relative;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    border-radius: 30px;\n    font-family: myFont;\n}\n\n\n\n.input-group[_ngcontent-%COMP%]{\n    top:100px;\n    width:280px;\n    position: absolute;\n    transition: .5s;\n    font-family: myFont;\n\n}\n\n.input-field[_ngcontent-%COMP%]{\n    width: 100%;\n    padding:10px 0;\n    margin: 5px 0;\n    border-left:0;\n    border-right:0;\n    border-top: 0;\n    border-bottom: 1px solid #999;\n    outline: none;\n    background: transparent;\n    font-family: myFont;\n    margin-left: 40px;\n}\n\n.submit-btn[_ngcontent-%COMP%]{\n    width: 40%;\n    padding: 10px 30px;\n    cursor:pointer;\n    display: block;\n    margin: auto;\n    background:linear-gradient(to right, #CD5C5C,#FFE4C4);\n    border: 0;\n    outline: none;\n    border-radius:30px;\n    font-family: myFont;\n    margin-top: 20px;\n    margin-left: 130px;\n}\n\n.submit-btn[_ngcontent-%COMP%]:hover{\n    background:linear-gradient(to left, #CD5C5C,#FFE4C4);\n}\n\n.error-msg[_ngcontent-%COMP%]{\n    color:firebrick;\n    font-size: 10px;\n    margin-left: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2gtdXNlcm5hbWUvY2gtdXNlcm5hbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFNBQVM7SUFDVCxVQUFVO0FBQ2Q7O0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIscUJBQTRCO0FBQ2hDOztBQUNBO0lBQ0ksd0JBQXdCO0lBQ3hCLGVBQWU7SUFDZixXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlO0FBQ25COztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLHdCQUErQjtBQUNuQzs7QUFDQTtJQUNJLGtGQUEwRjtJQUMxRiwyQkFBMkI7SUFDM0IscUJBQXFCO0lBQ3JCLGlCQUFpQjs7QUFFckI7O0FBQ0E7SUFDSSxXQUFXO0lBQ1gsV0FBVztJQUNYLDJCQUEyQjtJQUMzQixzQkFBc0I7SUFDdEIsaUJBQWlCO0FBQ3JCOztBQUNBO0lBQ0ksVUFBVTtJQUNWLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isa0NBQWtDO0lBQ2xDLDJCQUEyQjtJQUMzQixtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxTQUFTO0lBQ1QsVUFBVTtJQUNWLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxjQUFjO0lBQ2QscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixlQUFlO0FBQ25COztBQUNBO0lBQ0ksNkJBQTZCO0lBQzdCLFNBQVM7SUFDVCxXQUFXO0lBQ1gsU0FBUztJQUNULGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGVBQWU7SUFDZixZQUFZO0VBQ2Q7O0FBRUEsa0VBQWtFOztBQUNsRTtJQUNFLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtFQUNwQjs7QUFHQSx5Q0FBeUM7O0FBQ3pDO0lBQ0UsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQiw0Q0FBNEM7SUFDNUMsVUFBVTtFQUNaOztBQUVBLDhCQUE4Qjs7QUFDOUI7SUFDRSxjQUFjO0lBQ2QsYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixjQUFjO0VBQ2hCOztBQUVBO0lBQ0UsMEJBQTBCO0lBQzFCLFNBQVM7SUFDVCxVQUFVO0lBQ1YsU0FBUztJQUNULGtCQUFrQjtJQUNsQixtQkFBbUI7QUFDdkI7O0FBRUUsNENBQTRDOztBQUM1QywyQkFBMkIsaUNBQWlDLENBQUM7O0FBRTdELG9DQUFvQzs7QUFDcEMsbUNBQW1DLGNBQWMsQ0FBQzs7QUFFbEQsMEZBQTBGOztBQUMxRiwwQkFBMEIseUJBQXlCLENBQUM7O0FBR3RELG1CQUFtQjs7QUFLbkIsbUJBQW1COztBQUNuQjtJQUNJLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsaUJBQWlCO0FBQ3JCOztBQUNBO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osa0NBQWtDO0lBQ2xDLG1CQUFtQjtJQUNuQixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGdCQUFnQjtBQUNwQjs7QUFDQTtJQUNJLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGtDQUFrQztJQUNsQyxtQkFBbUI7SUFDbkIsbUJBQW1CO0FBQ3ZCOztBQUVBOzs7Ozs7Ozs7R0FTRzs7QUFDSDtJQUNJLFNBQVM7SUFDVCxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixtQkFBbUI7O0FBRXZCOztBQUNBO0lBQ0ksV0FBVztJQUNYLGNBQWM7SUFDZCxhQUFhO0lBQ2IsYUFBYTtJQUNiLGNBQWM7SUFDZCxhQUFhO0lBQ2IsNkJBQTZCO0lBQzdCLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGlCQUFpQjtBQUNyQjs7QUFDQTtJQUNJLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGNBQWM7SUFDZCxZQUFZO0lBQ1oscURBQXFEO0lBQ3JELFNBQVM7SUFDVCxhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0FBQ3RCOztBQUNDO0lBQ0csb0RBQW9EO0FBQ3hEOztBQUNBO0lBQ0ksZUFBZTtJQUNmLGVBQWU7SUFDZixpQkFBaUI7QUFDckIiLCJmaWxlIjoic3JjL2FwcC9jaC11c2VybmFtZS9jaC11c2VybmFtZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKntcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbn1cblxuQGZvbnQtZmFjZSB7XG4gICAgZm9udC1mYW1pbHk6IG15Rmlyc3RGb250O1xuICAgIHNyYzogdXJsKC4uL2ZvbnRzL2ZvbnQzLnR0Zik7XG59XG5oMSB7XG4gICAgZm9udC1mYW1pbHk6IG15Rmlyc3RGb250O1xuICAgIGZvbnQtc2l6ZTogODVweDtcbiAgICBib3R0b206IDVweDtcbiAgICB0ZXh0LWFsaWduIDogY2VudGVyO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBjb2xvciA6ICNGMDgwODA7XG59XG5cbkBmb250LWZhY2Uge1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgc3JjOiB1cmwoLi4vZm9udHMvZm9udEZvcm0udHRmKTtcbn1cbmJvZHl7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHJnYmEoMCwwLDAsMC40KSxyZ2JhKDAsMCwwLDAuNCkpLHVybCguLi9pbWFnZXMvbm90ZS5qcGcpO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGF1dG87XG4gICAgcG9zaXRpb246cmVsYXRpdmU7XG5cbn1cbi5oZXJve1xuICAgIGhlaWdodDoxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xufVxuLnRpdGxlLWJveHtcbiAgICB3aWR0aDogOTclO1xuICAgIGhlaWdodDogMTE1cHg7XG4gICAgcG9zaXRpb246cmVsYXRpdmU7XG4gICAgbWFyZ2luOjUlIGF1dG87XG4gICAgYmFja2dyb3VuZDogI0ZGRTRFMTtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4IDlweCAjZmY2MTI0MWY7XG4gICAgYm9yZGVyOiAxMHB4IG91dHNldCAjQkM4RjhGO1xuICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XG59XG4gICAgIFxuLmJhY2stdG8tc2VhcmNoIHtcbiAgICB0b3A6IDExcHg7XG4gICAgbGVmdDogMTBweDtcbiAgICByaWdodDogODclO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLmF7XG4gICAgY29sb3I6ICNGMDgwODA7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgZm9udC1zaXplOiAxOHB4O1xufVxuLmRyb3BidG4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIHRvcDogMTFweDtcbiAgICByaWdodDogMTBweDtcbiAgICBsZWZ0OiA4NyU7XG4gICAgY29sb3I6ICNGMDgwODA7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBwYWRkaW5nOiA0cHg7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgfVxuICBcbiAgLyogVGhlIGNvbnRhaW5lciA8ZGl2PiAtIG5lZWRlZCB0byBwb3NpdGlvbiB0aGUgZHJvcGRvd24gY29udGVudCAqL1xuICAuZHJvcGRvd24ge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXRvcDogMjVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gIH1cbiAgXG4gIFxuICAvKiBEcm9wZG93biBDb250ZW50IChIaWRkZW4gYnkgRGVmYXVsdCkgKi9cbiAgLmRyb3Bkb3duLWNvbnRlbnQge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGRkU0RTE7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBtaW4td2lkdGg6IDE2MHB4O1xuICAgIGJveC1zaGFkb3c6IDBweCA4cHggMTZweCAwcHggcmdiYSgwLDAsMCwwLjIpO1xuICAgIHotaW5kZXg6IDE7XG4gIH1cbiAgXG4gIC8qIExpbmtzIGluc2lkZSB0aGUgZHJvcGRvd24gKi9cbiAgLmRyb3Bkb3duLWNvbnRlbnQgYSB7XG4gICAgY29sb3I6ICNGMDgwODA7XG4gICAgcGFkZGluZzogMTZweDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gIH1cblxuICAubG9nb3V0IHtcbiAgICAvKiBiYWNrZ3JvdW5kOiAgI0NENUM1QzsgKi9cbiAgICB0b3A6IDExcHg7XG4gICAgcmlnaHQ6IDBweDtcbiAgICBsZWZ0OiA4NyU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG59XG4gIFxuICAvKiBDaGFuZ2UgY29sb3Igb2YgZHJvcGRvd24gbGlua3Mgb24gaG92ZXIgKi9cbiAgLmRyb3Bkb3duLWNvbnRlbnQgYTpob3ZlciB7YmFja2dyb3VuZC1jb2xvcjogcmdiKDgzLCA2MCwgNjApO31cbiAgXG4gIC8qIFNob3cgdGhlIGRyb3Bkb3duIG1lbnUgb24gaG92ZXIgKi9cbiAgLmRyb3Bkb3duOmhvdmVyIC5kcm9wZG93bi1jb250ZW50IHtkaXNwbGF5OiBibG9jazt9XG4gIFxuICAvKiBDaGFuZ2UgdGhlIGJhY2tncm91bmQgY29sb3Igb2YgdGhlIGRyb3Bkb3duIGJ1dHRvbiB3aGVuIHRoZSBkcm9wZG93biBjb250ZW50IGlzIHNob3duICovXG4gIC5kcm9wZG93bjpob3ZlciAuZHJvcGJ0biB7YmFja2dyb3VuZC1jb2xvcjogI0ZGRTRFMTt9XG5cblxuLyogZHJvcC1kb3duIG1lbnUgKi9cblxuXG5cblxuLyogZHJvcC1kb3duIG1lbnUgKi9cbi5we1xuICAgIGNvbG9yOiAjRjA4MDgwO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgbWFyZ2luLWxlZnQ6IDQwcHg7O1xufVxuLmZvcm0tYm94e1xuICAgIHdpZHRoOiAzODBweDtcbiAgICBoZWlnaHQ6IDMwMHB4O1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgIG1hcmdpbjo1JSBhdXRvO1xuICAgIGJhY2tncm91bmQ6ICNGRkU0RTE7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4IDlweCAjZmY2MTI0MWY7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGF1dG87XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmJ1dHRvbi1ib3h7XG4gICAgd2lkdGg6IDIyMHB4O1xuICAgIG1hcmdpbjogMzVweCBhdXRvO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBib3gtc2hhZG93OiAwIDAgMjBweCA5cHggI2ZmNjEyNDFmO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbn1cblxuLyogI2J0bntcbiAgICB0b3A6IDA7XG4gICAgbGVmdDowO1xuICAgIHBvc2l0aW9uOmFic29sdXRlO1xuICAgIHdpZHRoOjExMHB4O1xuICAgIGhlaWdodDoxMDAlO1xuICAgIGJhY2tncm91bmQ6bGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjQ0Q1QzVDLCNGRkU0QzQpO1xuICAgIGJvcmRlci1yYWRpdXM6MzBweDtcbiAgICB0cmFuc2l0aW9uOiAuNXM7XG59ICovXG4uaW5wdXQtZ3JvdXB7XG4gICAgdG9wOjEwMHB4O1xuICAgIHdpZHRoOjI4MHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0cmFuc2l0aW9uOiAuNXM7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcblxufVxuLmlucHV0LWZpZWxke1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6MTBweCAwO1xuICAgIG1hcmdpbjogNXB4IDA7XG4gICAgYm9yZGVyLWxlZnQ6MDtcbiAgICBib3JkZXItcmlnaHQ6MDtcbiAgICBib3JkZXItdG9wOiAwO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjOTk5O1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBtYXJnaW4tbGVmdDogNDBweDtcbn1cbi5zdWJtaXQtYnRue1xuICAgIHdpZHRoOiA0MCU7XG4gICAgcGFkZGluZzogMTBweCAzMHB4O1xuICAgIGN1cnNvcjpwb2ludGVyO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI0NENUM1QywjRkZFNEM0KTtcbiAgICBib3JkZXI6IDA7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOjMwcHg7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMzBweDtcbn1cbiAuc3VibWl0LWJ0bjpob3ZlcntcbiAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byBsZWZ0LCAjQ0Q1QzVDLCNGRkU0QzQpO1xufSBcbi5lcnJvci1tc2d7XG4gICAgY29sb3I6ZmlyZWJyaWNrO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBtYXJnaW4tbGVmdDogNDBweDtcbn1cblxuXG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ChUsernameComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-ch-username',
          templateUrl: './ch-username.component.html',
          styleUrls: ['./ch-username.component.css']
        }]
      }], function () {
        return [{
          type: _service_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
        }];
      }, {
        newUsernameInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['newUsername', {
            "static": false
          }]
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/list-tracks/list-tracks.component.ts":
  /*!******************************************************!*\
    !*** ./src/app/list-tracks/list-tracks.component.ts ***!
    \******************************************************/

  /*! exports provided: ListTracksComponent */

  /***/
  function srcAppListTracksListTracksComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ListTracksComponent", function () {
      return ListTracksComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _service_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../service/user.service */
    "./src/app/service/user.service.ts");
    /* harmony import */


    var _service_deezer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../service/deezer.service */
    "./src/app/service/deezer.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function ListTracksComponent_div_22_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "No tracks in list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function ListTracksComponent_ng_template_23_div_0_Template(rf, ctx) {
      if (rf & 1) {
        var _r52 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Preview Track");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ListTracksComponent_ng_template_23_div_0_Template_button_click_9_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r52);

          var track_r50 = ctx.$implicit;

          var ctx_r51 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r51.removeTrack(track_r50.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Remove from list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var track_r50 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", track_r50.title, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("href", track_r50.preview, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
      }
    }

    function ListTracksComponent_ng_template_23_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, ListTracksComponent_ng_template_23_div_0_Template, 11, 2, "div", 13);
      }

      if (rf & 2) {
        var ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r48.tr);
      }
    }

    var _c0 = function _c0() {
      return ["", "search"];
    };

    var _c1 = function _c1() {
      return ["/"];
    };

    var _c2 = function _c2() {
      return ["/change/username"];
    };

    var _c3 = function _c3() {
      return ["/change/password"];
    };

    var _c4 = function _c4() {
      return ["/change/email"];
    };

    var ListTracksComponent = /*#__PURE__*/function () {
      function ListTracksComponent(userService, _deezerApi) {
        var _this10 = this;

        _classCallCheck(this, ListTracksComponent);

        this.userService = userService;
        this._deezerApi = _deezerApi;
        this.user = null;
        this.listIds = [];
        this.tr = [];
        this.user = this.userService.getUser();
        this.listTracks = this.userService.getList();
        this.listTracks.subscribe(function (l) {
          l.forEach(function (p) {
            _this10._deezerApi.getTrack(String(p)).subscribe(function (tr) {
              _this10.tr.push(tr);
            });
          });
        });
      }

      _createClass(ListTracksComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "removeTrack",
        value: function removeTrack(id) {
          var _this11 = this;

          this.listTracks.subscribe(function (l) {
            _this11.userService.removeFromList(l.filter(function (p) {
              return p !== String(id);
            }));
          });
          this.listTracks = this.userService.getList();
        }
      }]);

      return ListTracksComponent;
    }();

    ListTracksComponent.ɵfac = function ListTracksComponent_Factory(t) {
      return new (t || ListTracksComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_deezer_service__WEBPACK_IMPORTED_MODULE_2__["DeezerApi"]));
    };

    ListTracksComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ListTracksComponent,
      selectors: [["app-list-tracks"]],
      decls: 25,
      vars: 13,
      consts: [[1, "hero"], [1, "title-box"], [1, "back-to-search"], [1, "a", 3, "routerLink"], [1, "logout"], [1, "dropdown"], [1, "dropbtn"], [1, "dropdown-content"], [3, "routerLink"], ["style", "background:#f5b2b2; border-radius: 1.5rem; padding: 1em; box-shadow: 0 0 20px 9px #ff61241f;margin-top:10px;margin-bottom: 30px;margin-right:10px;position:center;margin-left: 10px;", 4, "ngIf", "ngIfElse"], ["noList", ""], [2, "background", "#f5b2b2", "border-radius", "1.5rem", "padding", "1em", "box-shadow", "0 0 20px 9px #ff61241f", "margin-top", "10px", "margin-bottom", "30px", "margin-right", "10px", "position", "center", "margin-left", "10px"], [1, "well", "album", 2, "background", "#f5b2b2", "border-radius", "1.5rem", "padding", "1em", "box-shadow", "0 0 20px 9px #ff61241f", "margin-top", "10px", "margin-bottom", "30px", "margin-right", "10px", "position", "center", "margin-left", "10px"], [4, "ngFor", "ngForOf"], [1, "row"], [1, "col-md-12"], ["target", "_blank", 1, "btn", "a4", 3, "href"], [1, "btn", "a4", 3, "click"]],
      template: function ListTracksComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "body");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Find your jam");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " Back to search page ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Log out");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Change username");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Change password");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Change e-mail");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, ListTracksComponent_div_22_Template, 4, 0, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, ListTracksComponent_ng_template_23_Template, 1, 1, "ng-template", null, 10, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r47 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](8, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.user, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](9, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c3));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c4));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.tr === null)("ngIfElse", _r47);
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"]],
      styles: ["html[_ngcontent-%COMP%], body[_ngcontent-%COMP%]\n{\n    margin: 0px;\n    overflow-x: hidden;\n    padding-top: 0px;\n    width: 100%;\n}\n    @font-face {\n    font-family: myFirstFont;\n    src: url('font3.ttf');\n    }\n    h1[_ngcontent-%COMP%] {\n    font-family: myFirstFont;\n    font-size: 85px;\n    bottom: 5px;\n    text-align : center;\n    position: relative;\n    color : #F08080;\n    }\n    @font-face {\n    font-family: myFont;\n    src: url('fontForm.ttf');\n    }\n    h2[_ngcontent-%COMP%]{\n        font-family: myFont;\n    }\n    h3[_ngcontent-%COMP%]{\n        color:rgb(243, 234, 224);\n        font-weight: bold;\n        font-family: myFont;\n    }\n    h4[_ngcontent-%COMP%]{\n        font-weight: bold;\n        font-family: myFont;\n        font-size: large;\n    }\n    body[_ngcontent-%COMP%]{\n    background-image: url('vanilla_clouds.jpg');\n    background-position: center;\n    position:relative;\n    }\n    .hero[_ngcontent-%COMP%]{\n        height:100%;\n        width: 100%;\n        background-position: center;\n        background-size: cover;\n        margin: 0px;\n        padding: 0px;\n        overflow-x: hidden;\n        position:relative;\n    }\n    .title-box[_ngcontent-%COMP%]{\n        width: 97%;\n        height: 115px;\n        position:relative;\n        margin:5% auto;\n        background: #FFE4E1;\n        padding: 10px;\n        box-shadow: 0 0 20px 9px #ff61241f;\n        border: 10px outset #BC8F8F;\n        border-radius: 40px;\n    }\n    .back-to-search[_ngcontent-%COMP%] {\n        top: 11px;\n        left: 10px;\n        right: 87%;\n        position: absolute;\n        font-family: myFont;\n        margin-top: 5px;\n    }\n    .a[_ngcontent-%COMP%]{\n        color: #F08080;\n        text-decoration: none;\n        font-family: myFont;\n        font-size: 18px;\n    }\n    .dropbtn[_ngcontent-%COMP%] {\n        background-color: transparent;\n        top: 11px;\n        right: 10px;\n        left: 87%;\n        color: #F08080;\n        font-family: myFont;\n        padding: 4px;\n        font-size: 20px;\n        border: none;\n      }\n    \n    .dropdown[_ngcontent-%COMP%] {\n        position: relative;\n        display: inline-block;\n      }\n    .dropdown2[_ngcontent-%COMP%] {\n        position: relative;\n        display: inline-block;\n        margin-top: 10px;\n        margin-left: 30px;\n      }\n    \n    .dropdown-content[_ngcontent-%COMP%] {\n        display: none;\n        position: absolute;\n        background-color: #FFE4E1;\n        font-family: myFont;\n        min-width: 160px;\n        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n        z-index: 1;\n      }\n    \n    .dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n        color: #F08080;\n        padding: 16px;\n        text-decoration: none;\n        display: block;\n      }\n    .logout[_ngcontent-%COMP%] {\n        \n        top: 11px;\n        right: 0px;\n        left: 87%;\n        position: absolute;\n        font-family: myFont;\n    }\n    \n    .dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {background-color: rgb(83, 60, 60);}\n    \n    .dropdown[_ngcontent-%COMP%]:hover   .dropdown-content[_ngcontent-%COMP%] {display: block;}\n    \n    .dropdown[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {background-color: #FFE4E1;}\n    .submit-btn[_ngcontent-%COMP%]{\n        width: 20%;\n        padding: 10px 30px;\n        cursor:pointer;\n        display: block;\n        margin: auto;\n        background:linear-gradient(to right, #CD5C5C,#FFE4C4);\n        border: 0;\n        outline: none;\n        border-radius:30px;\n        font-family: myFont;\n        margin-top: 20px;\n    }\n    .submit-btn[_ngcontent-%COMP%]:hover{\n        background:linear-gradient(to left, #CD5C5C,#FFE4C4);\n    }\n    .a4[_ngcontent-%COMP%]{\n    color: bisque;\n    text-decoration: none;\n    font-family: myFont;\n    font-size: 18px;\n    }\n    .btn[_ngcontent-%COMP%]{\n        display: inline-block;\n        padding: .75rem 1.25rem;\n        border-radius: 10rem;\n        transition: all .3s;\n        position: relative;\n        overflow: hidden;\n        z-index: 1;\n        font-family: myFont;\n        \n    }\n    .btn[_ngcontent-%COMP%]::after {\n        content: '';\n        position: absolute;\n        bottom: 0;\n        left: 0;\n        width: 100%;\n        height: 100%;\n        background-color:  #c4a6e0;\n        border-radius: 10rem;\n        z-index: -2;\n    }\n    .btn[_ngcontent-%COMP%]::before {\n        content: '';\n        position: absolute;\n        bottom: 0;\n        left: 0;\n        width: 0%;\n        height: 100%;\n        background-color:  #754c9b;\n        transition: all .3s;\n        border-radius: 10rem;\n        z-index: -1;\n    }\n    .btn[_ngcontent-%COMP%]:hover {\n    color: #fff;\n    }\n    .btn[_ngcontent-%COMP%]:hover::before {\n    width: 100%;\n    }\n    .bot[_ngcontent-%COMP%]{\n        font-family: myFont;\n        font-size: 35px;\n        bottom : 60%;\n        color : #F08080;\n    }\n    .a2[_ngcontent-%COMP%]{\n        color: #494848;\n        font-size: 25px;\n        font-family: myFont;\n    }\n    .a3[_ngcontent-%COMP%]{\n        color: #F08080;\n        text-decoration: none;\n        font-family: myFont;\n        font-size: 18px;\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGlzdC10cmFja3MvbGlzdC10cmFja3MuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7SUFFSSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixXQUFXO0FBQ2Y7SUFDSTtJQUNBLHdCQUF3QjtJQUN4QixxQkFBNEI7SUFDNUI7SUFDQTtJQUNBLHdCQUF3QjtJQUN4QixlQUFlO0lBQ2YsV0FBVztJQUNYLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmO0lBRUE7SUFDQSxtQkFBbUI7SUFDbkIsd0JBQStCO0lBQy9CO0lBQ0E7UUFDSSxtQkFBbUI7SUFDdkI7SUFDQTtRQUNJLHdCQUF3QjtRQUN4QixpQkFBaUI7UUFDakIsbUJBQW1CO0lBQ3ZCO0lBQ0E7UUFDSSxpQkFBaUI7UUFDakIsbUJBQW1CO1FBQ25CLGdCQUFnQjtJQUNwQjtJQUNBO0lBQ0EsMkNBQW1EO0lBQ25ELDJCQUEyQjtJQUMzQixpQkFBaUI7SUFDakI7SUFDQTtRQUNJLFdBQVc7UUFDWCxXQUFXO1FBQ1gsMkJBQTJCO1FBQzNCLHNCQUFzQjtRQUN0QixXQUFXO1FBQ1gsWUFBWTtRQUNaLGtCQUFrQjtRQUNsQixpQkFBaUI7SUFDckI7SUFDQTtRQUNJLFVBQVU7UUFDVixhQUFhO1FBQ2IsaUJBQWlCO1FBQ2pCLGNBQWM7UUFDZCxtQkFBbUI7UUFDbkIsYUFBYTtRQUNiLGtDQUFrQztRQUNsQywyQkFBMkI7UUFDM0IsbUJBQW1CO0lBQ3ZCO0lBRUE7UUFDSSxTQUFTO1FBQ1QsVUFBVTtRQUNWLFVBQVU7UUFDVixrQkFBa0I7UUFDbEIsbUJBQW1CO1FBQ25CLGVBQWU7SUFDbkI7SUFFQTtRQUNJLGNBQWM7UUFDZCxxQkFBcUI7UUFDckIsbUJBQW1CO1FBQ25CLGVBQWU7SUFDbkI7SUFDQTtRQUNJLDZCQUE2QjtRQUM3QixTQUFTO1FBQ1QsV0FBVztRQUNYLFNBQVM7UUFDVCxjQUFjO1FBQ2QsbUJBQW1CO1FBQ25CLFlBQVk7UUFDWixlQUFlO1FBQ2YsWUFBWTtNQUNkO0lBRUEsa0VBQWtFO0lBQ2xFO1FBQ0Usa0JBQWtCO1FBQ2xCLHFCQUFxQjtNQUN2QjtJQUNBO1FBQ0Usa0JBQWtCO1FBQ2xCLHFCQUFxQjtRQUNyQixnQkFBZ0I7UUFDaEIsaUJBQWlCO01BQ25CO0lBRUEseUNBQXlDO0lBQ3pDO1FBQ0UsYUFBYTtRQUNiLGtCQUFrQjtRQUNsQix5QkFBeUI7UUFDekIsbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQiw0Q0FBNEM7UUFDNUMsVUFBVTtNQUNaO0lBRUEsOEJBQThCO0lBQzlCO1FBQ0UsY0FBYztRQUNkLGFBQWE7UUFDYixxQkFBcUI7UUFDckIsY0FBYztNQUNoQjtJQUVBO1FBQ0UsMEJBQTBCO1FBQzFCLFNBQVM7UUFDVCxVQUFVO1FBQ1YsU0FBUztRQUNULGtCQUFrQjtRQUNsQixtQkFBbUI7SUFDdkI7SUFFRSw0Q0FBNEM7SUFDNUMsMkJBQTJCLGlDQUFpQyxDQUFDO0lBRTdELG9DQUFvQztJQUNwQyxtQ0FBbUMsY0FBYyxDQUFDO0lBRWxELDBGQUEwRjtJQUMxRiwwQkFBMEIseUJBQXlCLENBQUM7SUFJdEQ7UUFDSSxVQUFVO1FBQ1Ysa0JBQWtCO1FBQ2xCLGNBQWM7UUFDZCxjQUFjO1FBQ2QsWUFBWTtRQUNaLHFEQUFxRDtRQUNyRCxTQUFTO1FBQ1QsYUFBYTtRQUNiLGtCQUFrQjtRQUNsQixtQkFBbUI7UUFDbkIsZ0JBQWdCO0lBQ3BCO0lBQ0E7UUFDSSxvREFBb0Q7SUFDeEQ7SUFFQTtJQUNBLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZjtJQUVBO1FBQ0kscUJBQXFCO1FBQ3JCLHVCQUF1QjtRQUN2QixvQkFBb0I7UUFDcEIsbUJBQW1CO1FBQ25CLGtCQUFrQjtRQUNsQixnQkFBZ0I7UUFDaEIsVUFBVTtRQUNWLG1CQUFtQjs7SUFFdkI7SUFDQTtRQUNJLFdBQVc7UUFDWCxrQkFBa0I7UUFDbEIsU0FBUztRQUNULE9BQU87UUFDUCxXQUFXO1FBQ1gsWUFBWTtRQUNaLDBCQUEwQjtRQUMxQixvQkFBb0I7UUFDcEIsV0FBVztJQUNmO0lBQ0E7UUFDSSxXQUFXO1FBQ1gsa0JBQWtCO1FBQ2xCLFNBQVM7UUFDVCxPQUFPO1FBQ1AsU0FBUztRQUNULFlBQVk7UUFDWiwwQkFBMEI7UUFDMUIsbUJBQW1CO1FBQ25CLG9CQUFvQjtRQUNwQixXQUFXO0lBQ2Y7SUFDQTtJQUNBLFdBQVc7SUFDWDtJQUNBO0lBQ0EsV0FBVztJQUNYO0lBQ0E7UUFDSSxtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLFlBQVk7UUFDWixlQUFlO0lBQ25CO0lBRUE7UUFDSSxjQUFjO1FBQ2QsZUFBZTtRQUNmLG1CQUFtQjtJQUN2QjtJQUNBO1FBQ0ksY0FBYztRQUNkLHFCQUFxQjtRQUNyQixtQkFBbUI7UUFDbkIsZUFBZTtJQUNuQiIsImZpbGUiOiJzcmMvYXBwL2xpc3QtdHJhY2tzL2xpc3QtdHJhY2tzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJodG1sLGJvZHlcbntcbiAgICBtYXJnaW46IDBweDtcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbiAgICBAZm9udC1mYWNlIHtcbiAgICBmb250LWZhbWlseTogbXlGaXJzdEZvbnQ7XG4gICAgc3JjOiB1cmwoLi4vZm9udHMvZm9udDMudHRmKTtcbiAgICB9XG4gICAgaDEge1xuICAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udDtcbiAgICBmb250LXNpemU6IDg1cHg7XG4gICAgYm90dG9tOiA1cHg7XG4gICAgdGV4dC1hbGlnbiA6IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgY29sb3IgOiAjRjA4MDgwO1xuICAgIH1cbiAgICBcbiAgICBAZm9udC1mYWNlIHtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIHNyYzogdXJsKC4uL2ZvbnRzL2ZvbnRGb3JtLnR0Zik7XG4gICAgfSAgXG4gICAgaDJ7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgfVxuICAgIGgze1xuICAgICAgICBjb2xvcjpyZ2IoMjQzLCAyMzQsIDIyNCk7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIH1cbiAgICBoNHtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogbGFyZ2U7XG4gICAgfVxuICAgIGJvZHl7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uL2ltYWdlcy92YW5pbGxhX2Nsb3Vkcy5qcGcpO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgICB9XG4gICAgLmhlcm97XG4gICAgICAgIGhlaWdodDoxMDAlO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgICAgcGFkZGluZzogMHB4O1xuICAgICAgICBvdmVyZmxvdy14OiBoaWRkZW47XG4gICAgICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgIH0gXG4gICAgLnRpdGxlLWJveHtcbiAgICAgICAgd2lkdGg6IDk3JTtcbiAgICAgICAgaGVpZ2h0OiAxMTVweDtcbiAgICAgICAgcG9zaXRpb246cmVsYXRpdmU7XG4gICAgICAgIG1hcmdpbjo1JSBhdXRvO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjRkZFNEUxO1xuICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICBib3gtc2hhZG93OiAwIDAgMjBweCA5cHggI2ZmNjEyNDFmO1xuICAgICAgICBib3JkZXI6IDEwcHggb3V0c2V0ICNCQzhGOEY7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDQwcHg7XG4gICAgfVxuICAgICAgICAgXG4gICAgLmJhY2stdG8tc2VhcmNoIHtcbiAgICAgICAgdG9wOiAxMXB4O1xuICAgICAgICBsZWZ0OiAxMHB4O1xuICAgICAgICByaWdodDogODclO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICB9XG4gICAgXG4gICAgLmF7XG4gICAgICAgIGNvbG9yOiAjRjA4MDgwO1xuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICB9XG4gICAgLmRyb3BidG4ge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgICAgdG9wOiAxMXB4O1xuICAgICAgICByaWdodDogMTBweDtcbiAgICAgICAgbGVmdDogODclO1xuICAgICAgICBjb2xvcjogI0YwODA4MDtcbiAgICAgICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICAgICAgcGFkZGluZzogNHB4O1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgLyogVGhlIGNvbnRhaW5lciA8ZGl2PiAtIG5lZWRlZCB0byBwb3NpdGlvbiB0aGUgZHJvcGRvd24gY29udGVudCAqL1xuICAgICAgLmRyb3Bkb3duIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICB9XG4gICAgICAuZHJvcGRvd24yIHtcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICAgICAgfVxuICAgICAgXG4gICAgICAvKiBEcm9wZG93biBDb250ZW50IChIaWRkZW4gYnkgRGVmYXVsdCkgKi9cbiAgICAgIC5kcm9wZG93bi1jb250ZW50IHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZFNEUxO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgICAgICBtaW4td2lkdGg6IDE2MHB4O1xuICAgICAgICBib3gtc2hhZG93OiAwcHggOHB4IDE2cHggMHB4IHJnYmEoMCwwLDAsMC4yKTtcbiAgICAgICAgei1pbmRleDogMTtcbiAgICAgIH1cbiAgICAgIFxuICAgICAgLyogTGlua3MgaW5zaWRlIHRoZSBkcm9wZG93biAqL1xuICAgICAgLmRyb3Bkb3duLWNvbnRlbnQgYSB7XG4gICAgICAgIGNvbG9yOiAjRjA4MDgwO1xuICAgICAgICBwYWRkaW5nOiAxNnB4O1xuICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgfVxuICAgIFxuICAgICAgLmxvZ291dCB7XG4gICAgICAgIC8qIGJhY2tncm91bmQ6ICAjQ0Q1QzVDOyAqL1xuICAgICAgICB0b3A6IDExcHg7XG4gICAgICAgIHJpZ2h0OiAwcHg7XG4gICAgICAgIGxlZnQ6IDg3JTtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIH1cbiAgICAgIFxuICAgICAgLyogQ2hhbmdlIGNvbG9yIG9mIGRyb3Bkb3duIGxpbmtzIG9uIGhvdmVyICovXG4gICAgICAuZHJvcGRvd24tY29udGVudCBhOmhvdmVyIHtiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoODMsIDYwLCA2MCk7fVxuICAgICAgXG4gICAgICAvKiBTaG93IHRoZSBkcm9wZG93biBtZW51IG9uIGhvdmVyICovXG4gICAgICAuZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge2Rpc3BsYXk6IGJsb2NrO31cbiAgICAgIFxuICAgICAgLyogQ2hhbmdlIHRoZSBiYWNrZ3JvdW5kIGNvbG9yIG9mIHRoZSBkcm9wZG93biBidXR0b24gd2hlbiB0aGUgZHJvcGRvd24gY29udGVudCBpcyBzaG93biAqL1xuICAgICAgLmRyb3Bkb3duOmhvdmVyIC5kcm9wYnRuIHtiYWNrZ3JvdW5kLWNvbG9yOiAjRkZFNEUxO31cbiAgICBcbiAgICBcbiAgICBcbiAgICAuc3VibWl0LWJ0bntcbiAgICAgICAgd2lkdGg6IDIwJTtcbiAgICAgICAgcGFkZGluZzogMTBweCAzMHB4O1xuICAgICAgICBjdXJzb3I6cG9pbnRlcjtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgYmFja2dyb3VuZDpsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNDRDVDNUMsI0ZGRTRDNCk7XG4gICAgICAgIGJvcmRlcjogMDtcbiAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czozMHB4O1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIH1cbiAgICAuc3VibWl0LWJ0bjpob3ZlcntcbiAgICAgICAgYmFja2dyb3VuZDpsaW5lYXItZ3JhZGllbnQodG8gbGVmdCwgI0NENUM1QywjRkZFNEM0KTtcbiAgICB9XG4gICAgXG4gICAgLmE0e1xuICAgIGNvbG9yOiBiaXNxdWU7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIH1cblxuICAgIC5idG57XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgcGFkZGluZzogLjc1cmVtIDEuMjVyZW07XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcmVtO1xuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIHotaW5kZXg6IDE7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgICAgIFxuICAgIH1cbiAgICAuYnRuOjphZnRlciB7XG4gICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGJvdHRvbTogMDtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogICNjNGE2ZTA7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcmVtO1xuICAgICAgICB6LWluZGV4OiAtMjtcbiAgICB9XG4gICAgLmJ0bjo6YmVmb3JlIHtcbiAgICAgICAgY29udGVudDogJyc7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgYm90dG9tOiAwO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICB3aWR0aDogMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogICM3NTRjOWI7XG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcmVtO1xuICAgICAgICB6LWluZGV4OiAtMTtcbiAgICB9XG4gICAgLmJ0bjpob3ZlciB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgfVxuICAgIC5idG46aG92ZXI6OmJlZm9yZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuICAgIC5ib3R7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMzVweDtcbiAgICAgICAgYm90dG9tIDogNjAlO1xuICAgICAgICBjb2xvciA6ICNGMDgwODA7XG4gICAgfVxuICAgIFxuICAgIC5hMntcbiAgICAgICAgY29sb3I6ICM0OTQ4NDg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICAgICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICB9XG4gICAgLmEze1xuICAgICAgICBjb2xvcjogI0YwODA4MDtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgfSJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ListTracksComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-list-tracks',
          templateUrl: './list-tracks.component.html',
          styleUrls: ['./list-tracks.component.css']
        }]
      }], function () {
        return [{
          type: _service_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]
        }, {
          type: _service_deezer_service__WEBPACK_IMPORTED_MODULE_2__["DeezerApi"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/notfound/notfound.component.ts":
  /*!************************************************!*\
    !*** ./src/app/notfound/notfound.component.ts ***!
    \************************************************/

  /*! exports provided: NotfoundComponent */

  /***/
  function srcAppNotfoundNotfoundComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NotfoundComponent", function () {
      return NotfoundComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js"); //Ukoliko nismo pronasli sta korisnik trazi


    var NotfoundComponent = function NotfoundComponent() {
      _classCallCheck(this, NotfoundComponent);
    };

    NotfoundComponent.ɵfac = function NotfoundComponent_Factory(t) {
      return new (t || NotfoundComponent)();
    };

    NotfoundComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: NotfoundComponent,
      selectors: [["app-notfound"]],
      decls: 16,
      vars: 0,
      consts: [[1, "error-container"], [1, "four"], [1, "screen-reader-text"], [1, "zero"], [2, "color", "#de7e85"], [1, "link-container"], ["target", "_self", "href", "http://localhost:4200/", 1, "more-link"]],
      template: function NotfoundComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "body");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "span", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "0");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "span", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "span", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "h3", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " The page you've requested for doesn't exist. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Return to log in page");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      styles: ["@import url('https://fonts.googleapis.com/css?family=Montserrat:400,600,700');\n@import url('https://fonts.googleapis.com/css?family=Catamaran:400,800');\n.error-container[_ngcontent-%COMP%] {\n  text-align: center;\n  font-size: 106px;\n  font-family: 'Catamaran', sans-serif;\n  font-weight: 800;\n  margin: 150px 180px 35px;\n  background-color:rgb(228, 227, 224);\n  border: 2px solid rgba(0, 0, 0, 0.4);\n  box-shadow: 0 0 25px 0 rgba(0, 0, 0, 0.781);\n  padding: 20px;\n }\n.error-container[_ngcontent-%COMP%]    > span[_ngcontent-%COMP%] {\n  display: inline-block;\n  position: relative;\n}\n.error-container[_ngcontent-%COMP%]    > span.four[_ngcontent-%COMP%] {\n  width: 136px;\n  height: 43px;\n  border-radius: 999px;\n  background:\n    linear-gradient(140deg, rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.07) 43%, transparent 44%, transparent 100%),\n    linear-gradient(105deg, transparent 0%, transparent 40%, rgba(0, 0, 0, 0.06) 41%, rgba(0, 0, 0, 0.07) 76%, transparent 77%, transparent 100%),\n    linear-gradient(to right, #d89ca4, #e27b7e);\n}\n.error-container[_ngcontent-%COMP%]    > span.four[_ngcontent-%COMP%]:before, .error-container[_ngcontent-%COMP%]    > span.four[_ngcontent-%COMP%]:after {\n  content: '';\n  display: block;\n  position: absolute;\n  border-radius: 999px;\n}\n.error-container[_ngcontent-%COMP%]    > span.four[_ngcontent-%COMP%]:before {\n  width: 43px;\n  height: 156px;\n  left: 60px;\n  bottom: -43px;\n  background:\n    linear-gradient(128deg, rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.07) 40%, transparent 41%, transparent 100%),\n    linear-gradient(116deg, rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.07) 50%, transparent 51%, transparent 100%),\n    linear-gradient(to top, #99749D, #B895AB, #CC9AA6, #D7969E, #E0787F);\n}\n.error-container[_ngcontent-%COMP%]    > span.four[_ngcontent-%COMP%]:after {\n  width: 137px;\n  height: 43px;\n  transform: rotate(-49.5deg);\n  left: -18px;\n  bottom: 36px;\n  background: linear-gradient(to right, #99749D, #B895AB, #CC9AA6, #D7969E, #E0787F);\n}\n.error-container[_ngcontent-%COMP%]    > span.zero[_ngcontent-%COMP%] {\n  vertical-align: text-top;\n  width: 156px;\n  height: 156px;\n  border-radius: 999px;\n  background: linear-gradient(-45deg, transparent 0%, rgba(0, 0, 0, 0.06) 50%,  transparent 51%, transparent 100%),\n    linear-gradient(to top right, #99749D, #99749D, #B895AB, #CC9AA6, #D7969E, #ED8687, #ED8687);\n  overflow: hidden;\n  -webkit-animation: bgshadow 5s infinite;\n          animation: bgshadow 5s infinite;\n}\n.error-container[_ngcontent-%COMP%]    > span.zero[_ngcontent-%COMP%]:before {\n  content: '';\n  display: block;\n  position: absolute;\n  transform: rotate(45deg);\n  width: 90px;\n  height: 90px;\n  background-color: transparent;\n  left: 0px;\n  bottom: 0px;\n  background:\n    linear-gradient(95deg, transparent 0%, transparent 8%, rgba(0, 0, 0, 0.07) 9%, transparent 50%, transparent 100%),\n    linear-gradient(85deg, transparent 0%, transparent 19%, rgba(0, 0, 0, 0.05) 20%, rgba(0, 0, 0, 0.07) 91%, transparent 92%, transparent 100%);\n}\n.error-container[_ngcontent-%COMP%]    > span.zero[_ngcontent-%COMP%]:after {\n  content: '';\n  display: block;\n  position: absolute;\n  border-radius: 999px;\n  width: 70px;\n  height: 70px;\n  left: 43px;\n  bottom: 43px;\n  background: rgb(228, 227, 224);\n  box-shadow: -2px 2px 2px 0px rgba(0, 0, 0, 0.1);\n}\n.screen-reader-text[_ngcontent-%COMP%] {\n    position: absolute;\n    top: -9999em;\n    left: -9999em;\n}\n@-webkit-keyframes bgshadow {\n  0% {\n    box-shadow: inset -160px 160px 0px 5px rgba(0, 0, 0, 0.4);\n  }\n  45% {\n    box-shadow: inset 0px 0px 0px 0px rgba(0, 0, 0, 0.1);\n  }\n  55% {\n    box-shadow: inset 0px 0px 0px 0px rgba(0, 0, 0, 0.1);\n  }\n  100% {\n    box-shadow: inset 160px -160px 0px 5px rgba(0, 0, 0, 0.4);\n  }\n}\n@keyframes bgshadow {\n  0% {\n    box-shadow: inset -160px 160px 0px 5px rgba(0, 0, 0, 0.4);\n  }\n  45% {\n    box-shadow: inset 0px 0px 0px 0px rgba(0, 0, 0, 0.1);\n  }\n  55% {\n    box-shadow: inset 0px 0px 0px 0px rgba(0, 0, 0, 0.1);\n  }\n  100% {\n    box-shadow: inset 160px -160px 0px 5px rgba(0, 0, 0, 0.4);\n  }\n}\n\n*[_ngcontent-%COMP%] {\n    box-sizing: border-box;\n    padding: 10px;\n}\nbody[_ngcontent-%COMP%] {\n    height: 100%;\n    width: 100%;\n    margin-top: 15px;\n    background-image: url('vanilla_clouds.jpg');\n    background-repeat: no-repeat;\n    background-size: 1200px 1200px;\n}\nhtml[_ngcontent-%COMP%], button[_ngcontent-%COMP%], input[_ngcontent-%COMP%], select[_ngcontent-%COMP%], textarea[_ngcontent-%COMP%] {\n    font-family: 'Montserrat', Helvetica, sans-serif;\n    color: #bbb;\n}\nh1[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 30px 15px;\n}\n.zoom-area[_ngcontent-%COMP%] { \n  max-width: 490px;\n  margin: 30px auto 30px;\n  font-size: 19px;\n  text-align: center;\n}\n.link-container[_ngcontent-%COMP%] {\n  text-align: center;\n}\na.more-link[_ngcontent-%COMP%] {\n  text-transform: uppercase;\n  font-size: 13px;\n    background-color: #de7e85;\n    padding: 10px 15px;\n    border-radius: 0;\n    color: #fff;\n    display: inline-block;\n    margin-right: 5px;\n    margin-bottom: 40px;\n    line-height: 1.5;\n    text-decoration: none;\n  margin-top: 20px;\n  letter-spacing: 1px;\n  border: 2px solid rgba(0, 0, 0, 0.4);\n  box-shadow: 3px 3px 10px rgba(0, 0, 0, 0.651);\n}\n.more-link[_ngcontent-%COMP%]:hover{\n  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.781);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90Zm91bmQvbm90Zm91bmQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSw2RUFBNkU7QUFDN0Usd0VBQXdFO0FBQ3hFO0VBQ0Usa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixvQ0FBb0M7RUFDcEMsZ0JBQWdCO0VBQ2hCLHdCQUF3QjtFQUN4QixtQ0FBbUM7RUFDbkMsb0NBQW9DO0VBQ3BDLDJDQUEyQztFQUMzQyxhQUFhO0NBQ2Q7QUFDRDtFQUNFLHFCQUFxQjtFQUNyQixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osb0JBQW9CO0VBQ3BCOzs7K0NBRzZDO0FBQy9DO0FBQ0E7O0VBRUUsV0FBVztFQUNYLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsb0JBQW9CO0FBQ3RCO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsYUFBYTtFQUNiLFVBQVU7RUFDVixhQUFhO0VBQ2I7Ozt3RUFHc0U7QUFDeEU7QUFDQTtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osMkJBQTJCO0VBQzNCLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0ZBQWtGO0FBQ3BGO0FBRUE7RUFDRSx3QkFBd0I7RUFDeEIsWUFBWTtFQUNaLGFBQWE7RUFDYixvQkFBb0I7RUFDcEI7Z0dBQzhGO0VBQzlGLGdCQUFnQjtFQUNoQix1Q0FBK0I7VUFBL0IsK0JBQStCO0FBQ2pDO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLGtCQUFrQjtFQUNsQix3QkFBd0I7RUFDeEIsV0FBVztFQUNYLFlBQVk7RUFDWiw2QkFBNkI7RUFDN0IsU0FBUztFQUNULFdBQVc7RUFDWDs7Z0pBRThJO0FBQ2hKO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsV0FBVztFQUNYLFlBQVk7RUFDWixVQUFVO0VBQ1YsWUFBWTtFQUNaLDhCQUE4QjtFQUM5QiwrQ0FBK0M7QUFDakQ7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osYUFBYTtBQUNqQjtBQUVBO0VBQ0U7SUFDRSx5REFBeUQ7RUFDM0Q7RUFDQTtJQUNFLG9EQUFvRDtFQUN0RDtFQUNBO0lBQ0Usb0RBQW9EO0VBQ3REO0VBQ0E7SUFDRSx5REFBeUQ7RUFDM0Q7QUFDRjtBQWJBO0VBQ0U7SUFDRSx5REFBeUQ7RUFDM0Q7RUFDQTtJQUNFLG9EQUFvRDtFQUN0RDtFQUNBO0lBQ0Usb0RBQW9EO0VBQ3REO0VBQ0E7SUFDRSx5REFBeUQ7RUFDM0Q7QUFDRjtBQUVBLGVBQWU7QUFDZjtJQUdJLHNCQUFzQjtJQUN0QixhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osV0FBVztJQUNYLGdCQUFnQjtJQUNoQiwyQ0FBbUQ7SUFDbkQsNEJBQTRCO0lBQzVCLDhCQUE4QjtBQUNsQztBQUNBO0lBQ0ksZ0RBQWdEO0lBQ2hELFdBQVc7QUFDZjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGlCQUFpQjtBQUNuQjtBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLHNCQUFzQjtFQUN0QixlQUFlO0VBQ2Ysa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixlQUFlO0lBQ2IseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLHFCQUFxQjtJQUNyQixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixxQkFBcUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixvQ0FBb0M7RUFDcEMsNkNBQTZDO0FBQy9DO0FBQ0E7RUFDRSx3Q0FBd0M7QUFDMUMiLCJmaWxlIjoic3JjL2FwcC9ub3Rmb3VuZC9ub3Rmb3VuZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Nb250c2VycmF0OjQwMCw2MDAsNzAwJyk7XG5AaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PUNhdGFtYXJhbjo0MDAsODAwJyk7XG4uZXJyb3ItY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDEwNnB4O1xuICBmb250LWZhbWlseTogJ0NhdGFtYXJhbicsIHNhbnMtc2VyaWY7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIG1hcmdpbjogMTUwcHggMTgwcHggMzVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjpyZ2IoMjI4LCAyMjcsIDIyNCk7XG4gIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgYm94LXNoYWRvdzogMCAwIDI1cHggMCByZ2JhKDAsIDAsIDAsIDAuNzgxKTtcbiAgcGFkZGluZzogMjBweDtcbiB9XG4uZXJyb3ItY29udGFpbmVyID4gc3BhbiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmVycm9yLWNvbnRhaW5lciA+IHNwYW4uZm91ciB7XG4gIHdpZHRoOiAxMzZweDtcbiAgaGVpZ2h0OiA0M3B4O1xuICBib3JkZXItcmFkaXVzOiA5OTlweDtcbiAgYmFja2dyb3VuZDpcbiAgICBsaW5lYXItZ3JhZGllbnQoMTQwZGVnLCByZ2JhKDAsIDAsIDAsIDAuMSkgMCUsIHJnYmEoMCwgMCwgMCwgMC4wNykgNDMlLCB0cmFuc3BhcmVudCA0NCUsIHRyYW5zcGFyZW50IDEwMCUpLFxuICAgIGxpbmVhci1ncmFkaWVudCgxMDVkZWcsIHRyYW5zcGFyZW50IDAlLCB0cmFuc3BhcmVudCA0MCUsIHJnYmEoMCwgMCwgMCwgMC4wNikgNDElLCByZ2JhKDAsIDAsIDAsIDAuMDcpIDc2JSwgdHJhbnNwYXJlbnQgNzclLCB0cmFuc3BhcmVudCAxMDAlKSxcbiAgICBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNkODljYTQsICNlMjdiN2UpO1xufVxuLmVycm9yLWNvbnRhaW5lciA+IHNwYW4uZm91cjpiZWZvcmUsXG4uZXJyb3ItY29udGFpbmVyID4gc3Bhbi5mb3VyOmFmdGVyIHtcbiAgY29udGVudDogJyc7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvcmRlci1yYWRpdXM6IDk5OXB4O1xufVxuLmVycm9yLWNvbnRhaW5lciA+IHNwYW4uZm91cjpiZWZvcmUge1xuICB3aWR0aDogNDNweDtcbiAgaGVpZ2h0OiAxNTZweDtcbiAgbGVmdDogNjBweDtcbiAgYm90dG9tOiAtNDNweDtcbiAgYmFja2dyb3VuZDpcbiAgICBsaW5lYXItZ3JhZGllbnQoMTI4ZGVnLCByZ2JhKDAsIDAsIDAsIDAuMSkgMCUsIHJnYmEoMCwgMCwgMCwgMC4wNykgNDAlLCB0cmFuc3BhcmVudCA0MSUsIHRyYW5zcGFyZW50IDEwMCUpLFxuICAgIGxpbmVhci1ncmFkaWVudCgxMTZkZWcsIHJnYmEoMCwgMCwgMCwgMC4xKSAwJSwgcmdiYSgwLCAwLCAwLCAwLjA3KSA1MCUsIHRyYW5zcGFyZW50IDUxJSwgdHJhbnNwYXJlbnQgMTAwJSksXG4gICAgbGluZWFyLWdyYWRpZW50KHRvIHRvcCwgIzk5NzQ5RCwgI0I4OTVBQiwgI0NDOUFBNiwgI0Q3OTY5RSwgI0UwNzg3Rik7XG59XG4uZXJyb3ItY29udGFpbmVyID4gc3Bhbi5mb3VyOmFmdGVyIHtcbiAgd2lkdGg6IDEzN3B4O1xuICBoZWlnaHQ6IDQzcHg7XG4gIHRyYW5zZm9ybTogcm90YXRlKC00OS41ZGVnKTtcbiAgbGVmdDogLTE4cHg7XG4gIGJvdHRvbTogMzZweDtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjOTk3NDlELCAjQjg5NUFCLCAjQ0M5QUE2LCAjRDc5NjlFLCAjRTA3ODdGKTtcbn1cblxuLmVycm9yLWNvbnRhaW5lciA+IHNwYW4uemVybyB7XG4gIHZlcnRpY2FsLWFsaWduOiB0ZXh0LXRvcDtcbiAgd2lkdGg6IDE1NnB4O1xuICBoZWlnaHQ6IDE1NnB4O1xuICBib3JkZXItcmFkaXVzOiA5OTlweDtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KC00NWRlZywgdHJhbnNwYXJlbnQgMCUsIHJnYmEoMCwgMCwgMCwgMC4wNikgNTAlLCAgdHJhbnNwYXJlbnQgNTElLCB0cmFuc3BhcmVudCAxMDAlKSxcbiAgICBsaW5lYXItZ3JhZGllbnQodG8gdG9wIHJpZ2h0LCAjOTk3NDlELCAjOTk3NDlELCAjQjg5NUFCLCAjQ0M5QUE2LCAjRDc5NjlFLCAjRUQ4Njg3LCAjRUQ4Njg3KTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYW5pbWF0aW9uOiBiZ3NoYWRvdyA1cyBpbmZpbml0ZTtcbn1cbi5lcnJvci1jb250YWluZXIgPiBzcGFuLnplcm86YmVmb3JlIHtcbiAgY29udGVudDogJyc7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgd2lkdGg6IDkwcHg7XG4gIGhlaWdodDogOTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGxlZnQ6IDBweDtcbiAgYm90dG9tOiAwcHg7XG4gIGJhY2tncm91bmQ6XG4gICAgbGluZWFyLWdyYWRpZW50KDk1ZGVnLCB0cmFuc3BhcmVudCAwJSwgdHJhbnNwYXJlbnQgOCUsIHJnYmEoMCwgMCwgMCwgMC4wNykgOSUsIHRyYW5zcGFyZW50IDUwJSwgdHJhbnNwYXJlbnQgMTAwJSksXG4gICAgbGluZWFyLWdyYWRpZW50KDg1ZGVnLCB0cmFuc3BhcmVudCAwJSwgdHJhbnNwYXJlbnQgMTklLCByZ2JhKDAsIDAsIDAsIDAuMDUpIDIwJSwgcmdiYSgwLCAwLCAwLCAwLjA3KSA5MSUsIHRyYW5zcGFyZW50IDkyJSwgdHJhbnNwYXJlbnQgMTAwJSk7XG59XG4uZXJyb3ItY29udGFpbmVyID4gc3Bhbi56ZXJvOmFmdGVyIHtcbiAgY29udGVudDogJyc7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvcmRlci1yYWRpdXM6IDk5OXB4O1xuICB3aWR0aDogNzBweDtcbiAgaGVpZ2h0OiA3MHB4O1xuICBsZWZ0OiA0M3B4O1xuICBib3R0b206IDQzcHg7XG4gIGJhY2tncm91bmQ6IHJnYigyMjgsIDIyNywgMjI0KTtcbiAgYm94LXNoYWRvdzogLTJweCAycHggMnB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG59XG5cbi5zY3JlZW4tcmVhZGVyLXRleHQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IC05OTk5ZW07XG4gICAgbGVmdDogLTk5OTllbTtcbn1cbiAgICBcbkBrZXlmcmFtZXMgYmdzaGFkb3cge1xuICAwJSB7XG4gICAgYm94LXNoYWRvdzogaW5zZXQgLTE2MHB4IDE2MHB4IDBweCA1cHggcmdiYSgwLCAwLCAwLCAwLjQpO1xuICB9XG4gIDQ1JSB7XG4gICAgYm94LXNoYWRvdzogaW5zZXQgMHB4IDBweCAwcHggMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgfVxuICA1NSUge1xuICAgIGJveC1zaGFkb3c6IGluc2V0IDBweCAwcHggMHB4IDBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gIH1cbiAgMTAwJSB7XG4gICAgYm94LXNoYWRvdzogaW5zZXQgMTYwcHggLTE2MHB4IDBweCA1cHggcmdiYSgwLCAwLCAwLCAwLjQpO1xuICB9XG59XG5cbi8qIGRlbW8gc3R1ZmYgKi9cbioge1xuICAgIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAtbW96LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBwYWRkaW5nOiAxMHB4O1xufVxuYm9keSB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uL2ltYWdlcy92YW5pbGxhX2Nsb3Vkcy5qcGcpO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMjAwcHggMTIwMHB4O1xufVxuaHRtbCwgYnV0dG9uLCBpbnB1dCwgc2VsZWN0LCB0ZXh0YXJlYSB7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0JywgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xuICAgIGNvbG9yOiAjYmJiO1xufVxuaDEge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMzBweCAxNXB4O1xufVxuLnpvb20tYXJlYSB7IFxuICBtYXgtd2lkdGg6IDQ5MHB4O1xuICBtYXJnaW46IDMwcHggYXV0byAzMHB4O1xuICBmb250LXNpemU6IDE5cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5saW5rLWNvbnRhaW5lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmEubW9yZS1saW5rIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxM3B4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkZTdlODU7XG4gICAgcGFkZGluZzogMTBweCAxNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgYm94LXNoYWRvdzogM3B4IDNweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC42NTEpO1xufVxuLm1vcmUtbGluazpob3ZlcntcbiAgYm94LXNoYWRvdzogMCAwIDAgMCByZ2JhKDAsIDAsIDAsIDAuNzgxKTtcbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotfoundComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-notfound',
          templateUrl: './notfound.component.html',
          styleUrls: ['notfound.component.css']
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/register/register.component.ts":
  /*!************************************************!*\
    !*** ./src/app/register/register.component.ts ***!
    \************************************************/

  /*! exports provided: RegisterComponent */

  /***/
  function srcAppRegisterRegisterComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterComponent", function () {
      return RegisterComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _service_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../service/user.service */
    "./src/app/service/user.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    var _c0 = ["userSign"];
    var _c1 = ["userLog"];

    function RegisterComponent_p_16_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Username is not valid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function RegisterComponent_p_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Password is not valid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function RegisterComponent_p_24_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Username already exists");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function RegisterComponent_p_26_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "E-mail already exists");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function RegisterComponent_p_27_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "E-mail is not valid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function RegisterComponent_p_29_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Password must be at least 5 characters long");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var RegisterComponent = /*#__PURE__*/function () {
      function RegisterComponent(formBuilder, router, http, userService) {
        _classCallCheck(this, RegisterComponent);

        this.formBuilder = formBuilder;
        this.router = router;
        this.http = http;
        this.userService = userService;
        this.title = 'Find your Jam';
        this.signUpEmail = true;
        this.signUpUsername = true;
        this.logInUsername = true;
        this.logInPassword = true;
        this.user = null;
        this.logInForm = this.formBuilder.group({
          username: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
          password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]]
        });
        this.signUpForm = this.formBuilder.group({
          username: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
          email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
          password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5)]]
        });
      }

      _createClass(RegisterComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "signup",
        value: function signup() {
          var x = document.getElementById("login");
          var y = document.getElementById("signup");
          var z = document.getElementById("btn");
          x.style.left = "-400px";
          y.style.left = "50px";
          z.style.left = "110px";
        }
      }, {
        key: "login",
        value: function login() {
          var x = document.getElementById("login");
          var y = document.getElementById("signup");
          var z = document.getElementById("btn");
          x.style.left = "50px";
          y.style.left = "450px";
          z.style.left = "0";
        }
      }, {
        key: "submitSign",
        value: function submitSign(data) {
          var _this12 = this;

          if (!this.signUpForm.valid) {
            window.alert("Not valid!");
            return;
          }

          this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/register', data).subscribe(function (data) {
            var user = _this12.userSignInput.nativeElement.value;

            _this12.userService.setUser(user);

            _this12.router.navigateByUrl("/search");

            _this12.signUpForm.reset();
          }, function (err) {
            console.log(err.error);
            _this12.signUpEmail = true;
            _this12.signUpUsername = true;

            if (err.error == "Email already exists") {
              _this12.signUpEmail = false;
            }

            if (err.error == "Username already exists") {
              _this12.signUpUsername = false;
            }
          });
        }
      }, {
        key: "submitLogin",
        value: function submitLogin(data) {
          var _this13 = this;

          if (!this.logInForm.valid) {
            window.alert("Not valid!");
            return;
          }

          this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiBaseUrl + '/login', data).subscribe(function (data) {
            var user = _this13.userLogInput.nativeElement.value;

            _this13.userService.setUser(user);

            _this13.router.navigateByUrl("/search");

            _this13.logInForm.reset();
          }, function (err) {
            console.log(err.error);
            _this13.logInUsername = true;
            _this13.logInPassword = true;

            if (err.error == "Incorrect username") {
              _this13.logInUsername = false;
            }

            if (err.error == "Password is not correct") {
              _this13.logInPassword = false;
            }
          });
        }
      }, {
        key: "logUsername",
        value: function logUsername() {
          return this.logInForm.get('username');
        }
      }, {
        key: "logPassword",
        value: function logPassword() {
          return this.logInForm.get('password');
        }
      }, {
        key: "signUsername",
        value: function signUsername() {
          return this.signUpForm.get('username');
        }
      }, {
        key: "signEmail",
        value: function signEmail() {
          return this.signUpForm.get('email');
        }
      }, {
        key: "signPassword",
        value: function signPassword() {
          return this.signUpForm.get('password');
        }
      }]);

      return RegisterComponent;
    }();

    RegisterComponent.ɵfac = function RegisterComponent_Factory(t) {
      return new (t || RegisterComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]));
    };

    RegisterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: RegisterComponent,
      selectors: [["app-register"]],
      viewQuery: function RegisterComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.userSignInput = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.userLogInput = _t.first);
        }
      },
      decls: 32,
      vars: 10,
      consts: [[1, "hero"], [1, "title-box"], [1, "form-box"], [1, "button-box"], ["id", "btn"], ["type", "button", 1, "toggle-btn", 3, "click"], ["id", "login", 1, "input-group", 3, "formGroup", "ngSubmit"], ["type", "text", "name", "logUsername", "formControlName", "username", "placeholder", "username", "required", "", 1, "input-field"], ["userLog", ""], ["class", "error-msg", 4, "ngIf"], ["type", "password", "name", "logPassword", "formControlName", "password", "placeholder", "password", "required", "", 1, "input-field"], ["type", "submit", 1, "submit-btn", 3, "disabled"], ["id", "signup", "name", "signup", "target", "_self", 1, "input-group", 3, "formGroup", "ngSubmit"], ["type", "text", "name", "signUsername", "formControlName", "username", "placeholder", "username", "required", "", 1, "input-field"], ["userSign", ""], ["type", "email", "name", "signEmail", "formControlName", "email", "placeholder", "email", "required", "", 1, "input-field"], ["type", "password", "name", "signPassword", "formControlName", "password", "placeholder", "password", "required", "", 1, "input-field"], [1, "error-msg"]],
      template: function RegisterComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "body");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Find your jam");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RegisterComponent_Template_button_click_9_listener() {
            return ctx.login();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Log in");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RegisterComponent_Template_button_click_11_listener() {
            return ctx.signup();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Sign up");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "form", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function RegisterComponent_Template_form_ngSubmit_13_listener() {
            return ctx.submitLogin(ctx.logInForm.value);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "input", 7, 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, RegisterComponent_p_16_Template, 2, 0, "p", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, RegisterComponent_p_18_Template, 2, 0, "p", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "button", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Log in");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "form", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function RegisterComponent_Template_form_ngSubmit_21_listener() {
            return ctx.submitSign(ctx.signUpForm.value);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "input", 13, 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](24, RegisterComponent_p_24_Template, 2, 0, "p", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, RegisterComponent_p_26_Template, 2, 0, "p", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, RegisterComponent_p_27_Template, 2, 0, "p", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "input", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, RegisterComponent_p_29_Template, 2, 0, "p", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Sign up");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.logInForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.logInUsername);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.logInPassword);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.logInForm.valid);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.signUpForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.signUpUsername);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.signUpEmail);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.signEmail().errors == null ? null : ctx.signEmail().errors.email);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.signPassword().errors == null ? null : ctx.signPassword().errors.minlength);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.signUpForm.valid);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
      styles: ["html[_ngcontent-%COMP%], body[_ngcontent-%COMP%]\n{\n    margin: 0px;\n    overflow-x: hidden;\n    padding-top: 0px;\n    width: 100%;\n}\n@font-face {\n    font-family: myFirstFont;\n    src: url('font3.ttf');\n}\nh1[_ngcontent-%COMP%] {\n    font-family: myFirstFont;\n    font-size: 100px;\n    text-align : center;\n    color : #F08080;\n    margin-top:20px;\n\n}\n@font-face {\n    font-family: myFont;\n    src: url('fontForm.ttf');\n}\nbody[_ngcontent-%COMP%]{\n    background-image: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)),url('note.jpg');\n    background-position: center;\n    background-size: auto;\n    position:relative;\n    margin: 0;\n}\n.hero[_ngcontent-%COMP%]{\n    height:100%;\n    width: 100%;\n    background-position: center;\n    \n    position:relative;\n}\n.title-box[_ngcontent-%COMP%]{\n    width: 500px;\n    height: 150px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    \n    box-shadow: 0 0 20px 9px #ff61241f;\n    border: 10px outset #BC8F8F;\n    border-radius: 55px;\n}\n.form-box[_ngcontent-%COMP%]{\n    width: 430px;\n    height: 380px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    padding: 5px;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    font-family: myFont;\n    background-size: auto;\n    border-radius: 25px;\n    overflow: hidden;\n}\n.button-box[_ngcontent-%COMP%]{\n    width: 220px;\n    margin: 35px auto;\n    position: relative;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    border-radius: 30px;\n    font-family: myFont;\n}\n.toggle-btn[_ngcontent-%COMP%]{\n    padding: 10px 30px;\n    cursor: pointer;\n    background:transparent;\n    border: 0;\n    outline: none;\n    position:relative;\n    font-family: myFont;\n}\n#btn[_ngcontent-%COMP%]{\n    top: 0;\n    left:0;\n    position:absolute;\n    width:110px;\n    height:100%;\n    background:linear-gradient(to right, #CD5C5C,#FFE4C4);\n    border-radius:30px;\n    transition: .5s;\n}\n.input-group[_ngcontent-%COMP%]{\n    top:100px;\n    width:280px;\n    position: absolute;\n    transition: .5s;\n    font-family: myFont;\n\n}\n.input-field[_ngcontent-%COMP%]{\n    width: 100%;\n    padding:10px 0;\n    margin: 5px 0;\n    border-left:0;\n    border-right:0;\n    border-top: 0;\n    border-bottom: 1px solid #999;\n    outline: none;\n    background: transparent;\n    font-family: myFont;\n}\n.submit-btn[_ngcontent-%COMP%]{\n    width: 40%;\n    padding: 10px 30px;\n    cursor:pointer;\n    display: block;\n    margin: auto;\n    background:linear-gradient(to right, #CD5C5C,#FFE4C4);\n    border: 0;\n    outline: none;\n    border-radius:30px;\n    font-family: myFont;\n    margin-top: 20px;\n}\n.submit-btn[_ngcontent-%COMP%]:hover{\n    background:linear-gradient(to left, #CD5C5C,#FFE4C4);\n}\n.error-msg[_ngcontent-%COMP%]{\n    color:firebrick;\n    font-size: 10px;\n}\n#login[_ngcontent-%COMP%]{\n    left:50px;\n}\n#signup[_ngcontent-%COMP%]{\n    left:450px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7SUFFSSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixXQUFXO0FBQ2Y7QUFDQTtJQUNJLHdCQUF3QjtJQUN4QixxQkFBNEI7QUFDaEM7QUFDQTtJQUNJLHdCQUF3QjtJQUN4QixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixlQUFlOztBQUVuQjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLHdCQUErQjtBQUNuQztBQUNBO0lBQ0ksa0ZBQTBGO0lBQzFGLDJCQUEyQjtJQUMzQixxQkFBcUI7SUFDckIsaUJBQWlCO0lBQ2pCLFNBQVM7QUFDYjtBQUNBO0lBQ0ksV0FBVztJQUNYLFdBQVc7SUFDWCwyQkFBMkI7SUFDM0IsMEJBQTBCO0lBQzFCLGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsa0NBQWtDO0lBQ2xDLDJCQUEyQjtJQUMzQixtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLFlBQVk7SUFDWixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGtDQUFrQztJQUNsQyxtQkFBbUI7SUFDbkIscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGtDQUFrQztJQUNsQyxtQkFBbUI7SUFDbkIsbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixTQUFTO0lBQ1QsYUFBYTtJQUNiLGlCQUFpQjtJQUNqQixtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLE1BQU07SUFDTixNQUFNO0lBQ04saUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxXQUFXO0lBQ1gscURBQXFEO0lBQ3JELGtCQUFrQjtJQUNsQixlQUFlO0FBQ25CO0FBQ0E7SUFDSSxTQUFTO0lBQ1QsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsbUJBQW1COztBQUV2QjtBQUNBO0lBQ0ksV0FBVztJQUNYLGNBQWM7SUFDZCxhQUFhO0lBQ2IsYUFBYTtJQUNiLGNBQWM7SUFDZCxhQUFhO0lBQ2IsNkJBQTZCO0lBQzdCLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSSxVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxjQUFjO0lBQ2QsWUFBWTtJQUNaLHFEQUFxRDtJQUNyRCxTQUFTO0lBQ1QsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSxvREFBb0Q7QUFDeEQ7QUFDQTtJQUNJLGVBQWU7SUFDZixlQUFlO0FBQ25CO0FBQ0E7SUFDSSxTQUFTO0FBQ2I7QUFDQTtJQUNJLFVBQVU7QUFDZCIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJodG1sLGJvZHlcbntcbiAgICBtYXJnaW46IDBweDtcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbkBmb250LWZhY2Uge1xuICAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udDtcbiAgICBzcmM6IHVybCguLi9mb250cy9mb250My50dGYpO1xufVxuaDEge1xuICAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udDtcbiAgICBmb250LXNpemU6IDEwMHB4O1xuICAgIHRleHQtYWxpZ24gOiBjZW50ZXI7XG4gICAgY29sb3IgOiAjRjA4MDgwO1xuICAgIG1hcmdpbi10b3A6MjBweDtcblxufVxuQGZvbnQtZmFjZSB7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBzcmM6IHVybCguLi9mb250cy9mb250Rm9ybS50dGYpO1xufVxuYm9keXtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQocmdiYSgwLDAsMCwwLjQpLHJnYmEoMCwwLDAsMC40KSksdXJsKC4uL2ltYWdlcy9ub3RlLmpwZyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogYXV0bztcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgICBtYXJnaW46IDA7XG59XG4uaGVyb3tcbiAgICBoZWlnaHQ6MTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgLypiYWNrZ3JvdW5kLXNpemU6IGNvdmVyOyovXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XG59XG4udGl0bGUtYm94e1xuICAgIHdpZHRoOiA1MDBweDtcbiAgICBoZWlnaHQ6IDE1MHB4O1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgIG1hcmdpbjo1JSBhdXRvO1xuICAgIGJhY2tncm91bmQ6ICNGRkU0RTE7XG4gICAgLypwYWRkaW5nOiAxMHB4OyovXG4gICAgYm94LXNoYWRvdzogMCAwIDIwcHggOXB4ICNmZjYxMjQxZjtcbiAgICBib3JkZXI6IDEwcHggb3V0c2V0ICNCQzhGOEY7XG4gICAgYm9yZGVyLXJhZGl1czogNTVweDtcbn1cbi5mb3JtLWJveHtcbiAgICB3aWR0aDogNDMwcHg7XG4gICAgaGVpZ2h0OiAzODBweDtcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgICBtYXJnaW46NSUgYXV0bztcbiAgICBiYWNrZ3JvdW5kOiAjRkZFNEUxO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3gtc2hhZG93OiAwIDAgMjBweCA5cHggI2ZmNjEyNDFmO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBhdXRvO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5idXR0b24tYm94e1xuICAgIHdpZHRoOiAyMjBweDtcbiAgICBtYXJnaW46IDM1cHggYXV0bztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgYm94LXNoYWRvdzogMCAwIDIwcHggOXB4ICNmZjYxMjQxZjtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG59XG4udG9nZ2xlLWJ0bntcbiAgICBwYWRkaW5nOiAxMHB4IDMwcHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJhY2tncm91bmQ6dHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyOiAwO1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgcG9zaXRpb246cmVsYXRpdmU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbn1cbiNidG57XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6MDtcbiAgICBwb3NpdGlvbjphYnNvbHV0ZTtcbiAgICB3aWR0aDoxMTBweDtcbiAgICBoZWlnaHQ6MTAwJTtcbiAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI0NENUM1QywjRkZFNEM0KTtcbiAgICBib3JkZXItcmFkaXVzOjMwcHg7XG4gICAgdHJhbnNpdGlvbjogLjVzO1xufVxuLmlucHV0LWdyb3Vwe1xuICAgIHRvcDoxMDBweDtcbiAgICB3aWR0aDoyODBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdHJhbnNpdGlvbjogLjVzO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG5cbn1cbi5pbnB1dC1maWVsZHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOjEwcHggMDtcbiAgICBtYXJnaW46IDVweCAwO1xuICAgIGJvcmRlci1sZWZ0OjA7XG4gICAgYm9yZGVyLXJpZ2h0OjA7XG4gICAgYm9yZGVyLXRvcDogMDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzk5OTtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG59XG4uc3VibWl0LWJ0bntcbiAgICB3aWR0aDogNDAlO1xuICAgIHBhZGRpbmc6IDEwcHggMzBweDtcbiAgICBjdXJzb3I6cG9pbnRlcjtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtYXJnaW46IGF1dG87XG4gICAgYmFja2dyb3VuZDpsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNDRDVDNUMsI0ZGRTRDNCk7XG4gICAgYm9yZGVyOiAwO1xuICAgIG91dGxpbmU6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czozMHB4O1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbn1cbi5zdWJtaXQtYnRuOmhvdmVye1xuICAgIGJhY2tncm91bmQ6bGluZWFyLWdyYWRpZW50KHRvIGxlZnQsICNDRDVDNUMsI0ZGRTRDNCk7XG59XG4uZXJyb3ItbXNne1xuICAgIGNvbG9yOmZpcmVicmljaztcbiAgICBmb250LXNpemU6IDEwcHg7XG59XG4jbG9naW57XG4gICAgbGVmdDo1MHB4O1xufVxuI3NpZ251cHtcbiAgICBsZWZ0OjQ1MHB4O1xufVxuXG4iXX0= */"]
    });
    RegisterComponent.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: RegisterComponent,
      factory: RegisterComponent.ɵfac
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RegisterComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-register',
          templateUrl: './register.component.html',
          styleUrls: ['./register.component.css']
        }]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
      }], function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]
        }, {
          type: _service_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]
        }];
      }, {
        userSignInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['userSign', {
            "static": false
          }]
        }],
        userLogInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['userLog', {
            "static": false
          }]
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/search/search.component.ts":
  /*!********************************************!*\
    !*** ./src/app/search/search.component.ts ***!
    \********************************************/

  /*! exports provided: SearchComponent */

  /***/
  function srcAppSearchSearchComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SearchComponent", function () {
      return SearchComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _service_deezer_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../service/deezer.service */
    "./src/app/service/deezer.service.ts");
    /* harmony import */


    var _service_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../service/user.service */
    "./src/app/service/user.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function SearchComponent_div_33_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h4");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "h4", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var res_r3 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("routerLink", "/artist/", res_r3.artist.id, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](res_r3.artist.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](res_r3.title);
      }
    }

    function SearchComponent_div_33_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SearchComponent_div_33_div_1_Template, 10, 3, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.searchRes);
      }
    }

    function SearchComponent_div_35_div_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var a_r5 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("routerLink", "/album/", a_r5.id, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", a_r5.cover_medium, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("routerLink", "/album/", a_r5.id, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](a_r5.title);
      }
    }

    function SearchComponent_div_35_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Recently viewed");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, SearchComponent_div_35_div_4_Template, 6, 4, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.alb);
      }
    }

    var _c0 = function _c0() {
      return ["/"];
    };

    var _c1 = function _c1() {
      return ["/change/username"];
    };

    var _c2 = function _c2() {
      return ["/change/password"];
    };

    var _c3 = function _c3() {
      return ["/change/email"];
    };

    var _c4 = function _c4() {
      return ["/myList"];
    };

    var SearchComponent = /*#__PURE__*/function () {
      function SearchComponent(_deezerApi, userService) {
        var _this14 = this;

        _classCallCheck(this, SearchComponent);

        this._deezerApi = _deezerApi;
        this.userService = userService; // Username trenutnog korisnika

        this.user = null;
        this.alb = [];
        this.user = userService.getUser();
        this.listHistory = this.userService.getHistory(this.user);
        this.listHistory.subscribe(function (l) {
          l.forEach(function (p) {
            _this14._deezerApi.getAlbum(String(p)).subscribe(function (al) {
              _this14.alb.push(al);
            });
          });
        });
      }

      _createClass(SearchComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "searchMusic",
        value: function searchMusic() {
          var _this15 = this;

          this._deezerApi.searchMusic(this.searchStr).subscribe(function (results) {
            _this15.searchRes = results;
          });
        }
      }]);

      return SearchComponent;
    }();

    SearchComponent.ɵfac = function SearchComponent_Factory(t) {
      return new (t || SearchComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_deezer_service__WEBPACK_IMPORTED_MODULE_1__["DeezerApi"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]));
    };

    SearchComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: SearchComponent,
      selectors: [["app-search"]],
      features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([_service_deezer_service__WEBPACK_IMPORTED_MODULE_1__["DeezerApi"]])],
      decls: 36,
      vars: 14,
      consts: [[1, "hero"], [1, "title-box"], [1, "logout"], [1, "dropdown"], [1, "dropbtn"], [1, "dropdown-content"], [3, "routerLink"], ["target", "_self", 1, "dropbtn", 3, "routerLink"], [1, "search-div"], ["id", "search-bar", 1, "form-group"], ["id", "searchbox", "name", "searchStr", "type", "text", "placeholder", "Enter name of artist or song...", 1, "form-control", 2, "color", "bisque", 3, "ngModel", "ngModelChange", "keyup"], [1, "search"], [4, "ngIf"], [4, "ngFor", "ngForOf"], [1, "row"], [1, "col-md-12"], [1, "well", 2, "background", "#F08080", "border-radius", "0.5rem", "padding", "1em"], ["target", "_self", 1, "a2", 3, "routerLink"], [2, "color", "bisque"], [1, "card", 2, "width", "128px", "margin", "5px"], ["target", "_self", 1, "img", "img-thumbnail", "album-thumb", 2, "width", "128px", "height", "128px", 3, "routerLink", "src"], [1, "card-body"], ["target", "_self", 1, "btn", "a", 3, "routerLink"]],
      template: function SearchComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "body");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Find your jam");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Log out");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Change username");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Change password");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Change e-mail");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "hr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "My list");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "input", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function SearchComponent_Template_input_ngModelChange_24_listener($event) {
            return ctx.searchStr = $event;
          })("keyup", function SearchComponent_Template_input_keyup_24_listener() {
            return ctx.searchMusic();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, SearchComponent_div_33_Template, 2, 1, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](35, SearchComponent_div_35_Template, 5, 1, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.user, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](9, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c3));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c4));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.searchStr);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.searchRes);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.alb !== null);
        }
      },
      directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLink"]],
      styles: ["html[_ngcontent-%COMP%], body[_ngcontent-%COMP%]\n{\n    margin: 0px;\n    overflow-x: hidden;\n    padding-top: 0px;\n    width: 100%;\n}\n@font-face {\n    font-family: myFirstFont;\n    src: url('font3.ttf');\n}\nh1[_ngcontent-%COMP%] {\n    font-family: myFirstFont;\n    font-size: 100px;\n    text-align : center;\n    position: relative;\n    \n    color : #F08080;\n    margin-top:-10px; \n}\nh3[_ngcontent-%COMP%]{\n    color:rgb(243, 234, 224);\n    font-weight: bold;\n    font-family: myFont;\n}\n@font-face {\n    font-family: myFont;\n    src: url('fontForm.ttf');\n}\nbody[_ngcontent-%COMP%]{\n    background-image: linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)),url('note.jpg');\n    background-position: center;\n    background-size: auto;\n    position:relative;\n   \n}\n.hero[_ngcontent-%COMP%]{\n    height:100%;\n    width: 100%;\n    background-position: center;\n    background-size: cover;\n    position: relative;\n}\n.title-box[_ngcontent-%COMP%]{\n    \n    width: 97%;\n    height: 130px;\n    position:relative;\n    margin:5% auto;\n    background: #FFE4E1;\n    padding: 10px;\n    box-shadow: 0 0 20px 9px #ff61241f;\n    border: 10px outset #BC8F8F;\n    \n    border-radius: 40px;\n    \n}\n\n\n.dropbtn[_ngcontent-%COMP%] {\n    background-color: transparent;\n    top: 11px;\n    right: 10px;\n    left: 87%;\n    color: #F08080;\n    font-family: myFont;\n    padding: 4px;\n    font-size: 20px;\n    border: none;\n  }\n\n.dropdown[_ngcontent-%COMP%] {\n    position: relative;\n    display: inline-block;\n  }\n.dropdown2[_ngcontent-%COMP%] {\n    position: relative;\n    display: inline-block;\n  \n    \n  }\n\n.dropdown-content[_ngcontent-%COMP%] {\n    display: none;\n    position: absolute;\n    background-color: #FFE4E1;\n    font-family: myFont;\n    min-width: 160px;\n    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n    z-index: 1;\n  }\n\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    color: #F08080;\n    padding: 16px;\n    text-decoration: none;\n    display: block;\n  }\n.logout[_ngcontent-%COMP%] {\n    \n    top: 11px;\n    right: 0px;\n    left: 87%;\n    position: absolute;\n    font-family: myFont;\n}\n\n.dropdown-content[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {background-color: rgb(83, 60, 60);}\n\n.dropdown[_ngcontent-%COMP%]:hover   .dropdown-content[_ngcontent-%COMP%] {display: block;}\n\n.dropdown[_ngcontent-%COMP%]:hover   .dropbtn[_ngcontent-%COMP%] {background-color: #FFE4E1;}\n\n#btn[_ngcontent-%COMP%]{\n    top: 0;\n    left:0;\n    position:absolute;\n    width:110px;\n    height:100%;\n    background:linear-gradient(to right, #CD5C5C,#FFE4C4);\n    border-radius:30px;\n    transition: .5s;\n}\n.submit-btn[_ngcontent-%COMP%]{\n    width: 40%;\n    padding: 10px 30px;\n    cursor:pointer;\n    display: block;\n    margin: auto;\n    background:linear-gradient(to right, #CD5C5C,#FFE4C4);\n    border: 0;\n    outline: none;\n    border-radius:30px;\n    font-family: myFont;\n    margin-top: 20px;\n}\n.submit-btn[_ngcontent-%COMP%]:hover{\n    background:linear-gradient(to right, #696969,#C0C0C0);\n\n}\n.search-div[_ngcontent-%COMP%]{ \n    width: 500px;\n    height: 900px;\n    position:relative;\n    padding: 10px;\n    \n}\n[_ngcontent-%COMP%]::-webkit-input-placeholder{\n    color: bisque;\n    opacity: 0.5;\n}\n[_ngcontent-%COMP%]::-moz-placeholder{\n    color: bisque;\n    opacity: 0.5;\n}\n[_ngcontent-%COMP%]::-ms-input-placeholder{\n    color: bisque;\n    opacity: 0.5;\n}\n[_ngcontent-%COMP%]::placeholder{\n    color: bisque;\n    opacity: 0.5;\n}\n\n#search-bar[_ngcontent-%COMP%]{\n    margin-top: 80px;\n    margin-left: 50px;\n    position: absolute;\n}\n.search[_ngcontent-%COMP%] {\n    position: absolute;\n    margin: auto;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    left: 0;\n    width: 80px;\n    height: 80px;\n    background: #CD5C5C;\n    border-radius: 50%;\n    transition: all 1s;\n    z-index: 4;\n    box-shadow: 0 0 25px 0 rgba(0, 0, 0, 0.4);\n    \n}\n.search[_ngcontent-%COMP%]:hover {\n    cursor: pointer;\n}\n.search[_ngcontent-%COMP%]::before {\n    content: \"\";\n    position: absolute;\n    margin: auto;\n    top: 22px;\n    right: 0;\n    bottom: 0;\n    left: 22px;\n    width: 12px;\n    height: 2px;\n    background: white;\n    transform: rotate(45deg);\n    transition: all .5s;\n}\n.search[_ngcontent-%COMP%]::after {\n    content: \"\";\n    position: absolute;\n    margin: auto;\n    top: -5px;\n    right: 0;\n    bottom: 0;\n    left: -5px;\n    width: 25px;\n    height: 25px;\n    border-radius: 50%;\n    border: 2px solid white;\n    transition: all .5s;\n    \n}\ninput[_ngcontent-%COMP%] {\n    font-family: myFont;\n    position: absolute;\n    margin: auto;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    left: 0;\n    width: 50px;\n    height: 50px;\n    outline: none;\n    border: none;\n    background:  #CD5C5C;\n    color: black;\n    text-shadow: 0 0 10px #CD5C5C;\n    padding: 0 80px 0 20px;\n    border-radius: 30px;\n    box-shadow: 0 0 25px 0 #CD5C5C,\n                0 20px 25px 0 rgba(0, 0, 0, 0.2);\n   \n    transition: all 1s;\n    opacity: 0;\n    z-index: 5;\n    letter-spacing: 0.1em;\n}\ninput[_ngcontent-%COMP%]:hover {\n    cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:focus {\n    width: 250px;\n    opacity: 1;\n    cursor: text;\n}\ninput[_ngcontent-%COMP%]:focus    ~ .search[_ngcontent-%COMP%] {\n    left: 300px ;\n    background: #151515;\n    z-index: 6;\n}\ninput[_ngcontent-%COMP%]:focus    ~ .search[_ngcontent-%COMP%]::before {\n    top: 0;\n    left: 0;\n    width: 25px;\n}\ninput[_ngcontent-%COMP%]:focus    ~ .search[_ngcontent-%COMP%]::after {\n    top: 0;\n    left: 0;\n    width: 25px;\n    height: 2px;\n    border: none;\n    background: white;\n    border-radius: 0%;\n    transform: rotate(-45deg);\n}\ninput[_ngcontent-%COMP%]:focus    ~ .search[_ngcontent-%COMP%]::-webkit-input-placeholder {\n    color: white;\n    opacity: 0.5;\n    font-weight: bolder;\n}\ninput[_ngcontent-%COMP%]:focus    ~ .search[_ngcontent-%COMP%]::-moz-placeholder {\n    color: white;\n    opacity: 0.5;\n    font-weight: bolder;\n}\ninput[_ngcontent-%COMP%]:focus    ~ .search[_ngcontent-%COMP%]::-ms-input-placeholder {\n    color: white;\n    opacity: 0.5;\n    font-weight: bolder;\n}\ninput[_ngcontent-%COMP%]:focus    ~ .search[_ngcontent-%COMP%]::placeholder {\n    color: white;\n    opacity: 0.5;\n    font-weight: bolder;\n}\n\n\n.logout[_ngcontent-%COMP%] {\n    \n    top: 11px;\n    right: 0px;\n    left: 87%;\n    position: absolute;\n    font-family: myFont;\n}\n.a[_ngcontent-%COMP%]{\n    color: #F08080;\n    text-decoration: none;\n    font-family: myFont;\n    font-size: 18px;\n\n}\n.a2[_ngcontent-%COMP%]{\n    color: #8f5bc0;\n    text-decoration: none;\n    font-family: myFont;\n    font-size: 18px;\n    margin-top:-10px; \n    \n}\n.a3[_ngcontent-%COMP%]{\n    color: #8f5bc0;\n    text-decoration: none;\n    font-family: myFont;\n    font-size: 18px;\n    margin-right: 500px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VhcmNoL3NlYXJjaC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLFdBQVc7QUFDZjtBQUNBO0lBQ0ksd0JBQXdCO0lBQ3hCLHFCQUE0QjtBQUNoQztBQUNBO0lBQ0ksd0JBQXdCO0lBQ3hCLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsa0JBQWtCOztJQUVsQixlQUFlO0lBQ2YsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSx3QkFBd0I7SUFDeEIsaUJBQWlCO0lBQ2pCLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLHdCQUErQjtBQUNuQztBQUNBO0lBQ0ksa0ZBQTBGO0lBQzFGLDJCQUEyQjtJQUMzQixxQkFBcUI7SUFDckIsaUJBQWlCOztBQUVyQjtBQUNBO0lBQ0ksV0FBVztJQUNYLFdBQVc7SUFDWCwyQkFBMkI7SUFDM0Isc0JBQXNCO0lBQ3RCLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0k7b0JBQ2dCO0lBQ2hCLFVBQVU7SUFDVixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLGtDQUFrQztJQUNsQywyQkFBMkI7SUFDM0IseUJBQXlCO0lBQ3pCLG1CQUFtQjs7QUFFdkI7QUFHQSxtQkFBbUI7QUFHbkIsb0JBQW9CO0FBQ3BCO0lBQ0ksNkJBQTZCO0lBQzdCLFNBQVM7SUFDVCxXQUFXO0lBQ1gsU0FBUztJQUNULGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGVBQWU7SUFDZixZQUFZO0VBQ2Q7QUFFQSxrRUFBa0U7QUFDbEU7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCO0VBQ3ZCO0FBQ0E7SUFDRSxrQkFBa0I7SUFDbEIscUJBQXFCOzs7RUFHdkI7QUFFQSx5Q0FBeUM7QUFDekM7SUFDRSxhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLDRDQUE0QztJQUM1QyxVQUFVO0VBQ1o7QUFFQSw4QkFBOEI7QUFDOUI7SUFDRSxjQUFjO0lBQ2QsYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixjQUFjO0VBQ2hCO0FBRUE7SUFDRSwwQkFBMEI7SUFDMUIsU0FBUztJQUNULFVBQVU7SUFDVixTQUFTO0lBQ1Qsa0JBQWtCO0lBQ2xCLG1CQUFtQjtBQUN2QjtBQUVFLDRDQUE0QztBQUM1QywyQkFBMkIsaUNBQWlDLENBQUM7QUFFN0Qsb0NBQW9DO0FBQ3BDLG1DQUFtQyxjQUFjLENBQUM7QUFFbEQsMEZBQTBGO0FBQzFGLDBCQUEwQix5QkFBeUIsQ0FBQztBQUl0RCxtQkFBbUI7QUFHbkI7SUFDSSxNQUFNO0lBQ04sTUFBTTtJQUNOLGlCQUFpQjtJQUNqQixXQUFXO0lBQ1gsV0FBVztJQUNYLHFEQUFxRDtJQUNyRCxrQkFBa0I7SUFDbEIsZUFBZTtBQUNuQjtBQUVBO0lBQ0ksVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsY0FBYztJQUNkLFlBQVk7SUFDWixxREFBcUQ7SUFDckQsU0FBUztJQUNULGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0kscURBQXFEOztBQUV6RDtBQUNBO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsYUFBYTs7QUFFakI7QUFDQTtJQUNJLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBSEE7SUFDSSxhQUFhO0lBQ2IsWUFBWTtBQUNoQjtBQUhBO0lBQ0ksYUFBYTtJQUNiLFlBQVk7QUFDaEI7QUFIQTtJQUNJLGFBQWE7SUFDYixZQUFZO0FBQ2hCO0FBQ0EsWUFBWTtBQUNaO0lBQ0ksZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixrQkFBa0I7QUFDdEI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osTUFBTTtJQUNOLFFBQVE7SUFDUixTQUFTO0lBQ1QsT0FBTztJQUNQLFdBQVc7SUFDWCxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLHlDQUF5Qzs7QUFFN0M7QUFDQTtJQUNJLGVBQWU7QUFDbkI7QUFDQTtJQUNJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLFNBQVM7SUFDVCxRQUFRO0lBQ1IsU0FBUztJQUNULFVBQVU7SUFDVixXQUFXO0lBQ1gsV0FBVztJQUNYLGlCQUFpQjtJQUNqQix3QkFBd0I7SUFDeEIsbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixTQUFTO0lBQ1QsUUFBUTtJQUNSLFNBQVM7SUFDVCxVQUFVO0lBQ1YsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjs7QUFFdkI7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLE1BQU07SUFDTixRQUFRO0lBQ1IsU0FBUztJQUNULE9BQU87SUFDUCxXQUFXO0lBQ1gsWUFBWTtJQUNaLGFBQWE7SUFDYixZQUFZO0lBQ1osb0JBQW9CO0lBQ3BCLFlBQVk7SUFDWiw2QkFBNkI7SUFDN0Isc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQjtnREFDNEM7O0lBRTVDLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsVUFBVTtJQUNWLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0ksZUFBZTtBQUNuQjtBQUNBO0lBQ0ksWUFBWTtJQUNaLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLFVBQVU7QUFDZDtBQUNBO0lBQ0ksTUFBTTtJQUNOLE9BQU87SUFDUCxXQUFXO0FBQ2Y7QUFDQTtJQUNJLE1BQU07SUFDTixPQUFPO0lBQ1AsV0FBVztJQUNYLFdBQVc7SUFDWCxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQix5QkFBeUI7QUFDN0I7QUFFQTtJQUNJLFlBQVk7SUFDWixZQUFZO0lBQ1osbUJBQW1CO0FBQ3ZCO0FBSkE7SUFDSSxZQUFZO0lBQ1osWUFBWTtJQUNaLG1CQUFtQjtBQUN2QjtBQUpBO0lBQ0ksWUFBWTtJQUNaLFlBQVk7SUFDWixtQkFBbUI7QUFDdkI7QUFKQTtJQUNJLFlBQVk7SUFDWixZQUFZO0lBQ1osbUJBQW1CO0FBQ3ZCO0FBRUEsZUFBZTtBQUdmO2NBQ2M7QUFHZDtJQUNJLDBCQUEwQjtJQUMxQixTQUFTO0lBQ1QsVUFBVTtJQUNWLFNBQVM7SUFDVCxrQkFBa0I7SUFDbEIsbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSSxjQUFjO0lBQ2QscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixlQUFlOztBQUVuQjtBQUdBO0lBQ0ksY0FBYztJQUNkLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGdCQUFnQjs7QUFFcEI7QUFDQTtJQUNJLGNBQWM7SUFDZCxxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixtQkFBbUI7QUFDdkI7QUFDQTs7Ozs7Ozs7O0dBU0ciLCJmaWxlIjoic3JjL2FwcC9zZWFyY2gvc2VhcmNoLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJodG1sLGJvZHlcbntcbiAgICBtYXJnaW46IDBweDtcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XG4gICAgcGFkZGluZy10b3A6IDBweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cbkBmb250LWZhY2Uge1xuICAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udDtcbiAgICBzcmM6IHVybCguLi9mb250cy9mb250My50dGYpO1xufVxuaDEge1xuICAgIGZvbnQtZmFtaWx5OiBteUZpcnN0Rm9udDtcbiAgICBmb250LXNpemU6IDEwMHB4O1xuICAgIHRleHQtYWxpZ24gOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIFxuICAgIGNvbG9yIDogI0YwODA4MDtcbiAgICBtYXJnaW4tdG9wOi0xMHB4OyBcbn1cbmgze1xuICAgIGNvbG9yOnJnYigyNDMsIDIzNCwgMjI0KTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xufVxuQGZvbnQtZmFjZSB7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBzcmM6IHVybCguLi9mb250cy9mb250Rm9ybS50dGYpO1xufVxuYm9keXtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQocmdiYSgwLDAsMCwwLjQpLHJnYmEoMCwwLDAsMC40KSksdXJsKC4uL2ltYWdlcy9ub3RlLmpwZyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogYXV0bztcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgIFxufVxuLmhlcm97XG4gICAgaGVpZ2h0OjEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnRpdGxlLWJveHtcbiAgICAvKiB3aWR0aDogNTAwcHg7XG4gICAgaGVpZ2h0OiAxMzBweDsgKi9cbiAgICB3aWR0aDogOTclO1xuICAgIGhlaWdodDogMTMwcHg7XG4gICAgcG9zaXRpb246cmVsYXRpdmU7XG4gICAgbWFyZ2luOjUlIGF1dG87XG4gICAgYmFja2dyb3VuZDogI0ZGRTRFMTtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4IDlweCAjZmY2MTI0MWY7XG4gICAgYm9yZGVyOiAxMHB4IG91dHNldCAjQkM4RjhGO1xuICAgIC8qIGJvcmRlci1yYWRpdXM6IDU1cHg7ICovXG4gICAgYm9yZGVyLXJhZGl1czogNDBweDtcbiAgICBcbn1cblxuXG4vKiBkcm9wLWRvd24gbWVudSAqL1xuXG5cbi8qIERyb3Bkb3duIEJ1dHRvbiAqL1xuLmRyb3BidG4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIHRvcDogMTFweDtcbiAgICByaWdodDogMTBweDtcbiAgICBsZWZ0OiA4NyU7XG4gICAgY29sb3I6ICNGMDgwODA7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBwYWRkaW5nOiA0cHg7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgfVxuICBcbiAgLyogVGhlIGNvbnRhaW5lciA8ZGl2PiAtIG5lZWRlZCB0byBwb3NpdGlvbiB0aGUgZHJvcGRvd24gY29udGVudCAqL1xuICAuZHJvcGRvd24ge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIH1cbiAgLmRyb3Bkb3duMiB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgXG4gICAgXG4gIH1cbiAgXG4gIC8qIERyb3Bkb3duIENvbnRlbnQgKEhpZGRlbiBieSBEZWZhdWx0KSAqL1xuICAuZHJvcGRvd24tY29udGVudCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRTRFMTtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIG1pbi13aWR0aDogMTYwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDhweCAxNnB4IDBweCByZ2JhKDAsMCwwLDAuMik7XG4gICAgei1pbmRleDogMTtcbiAgfVxuICBcbiAgLyogTGlua3MgaW5zaWRlIHRoZSBkcm9wZG93biAqL1xuICAuZHJvcGRvd24tY29udGVudCBhIHtcbiAgICBjb2xvcjogI0YwODA4MDtcbiAgICBwYWRkaW5nOiAxNnB4O1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5sb2dvdXQge1xuICAgIC8qIGJhY2tncm91bmQ6ICAjQ0Q1QzVDOyAqL1xuICAgIHRvcDogMTFweDtcbiAgICByaWdodDogMHB4O1xuICAgIGxlZnQ6IDg3JTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbn1cbiAgXG4gIC8qIENoYW5nZSBjb2xvciBvZiBkcm9wZG93biBsaW5rcyBvbiBob3ZlciAqL1xuICAuZHJvcGRvd24tY29udGVudCBhOmhvdmVyIHtiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoODMsIDYwLCA2MCk7fVxuICBcbiAgLyogU2hvdyB0aGUgZHJvcGRvd24gbWVudSBvbiBob3ZlciAqL1xuICAuZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLWNvbnRlbnQge2Rpc3BsYXk6IGJsb2NrO31cbiAgXG4gIC8qIENoYW5nZSB0aGUgYmFja2dyb3VuZCBjb2xvciBvZiB0aGUgZHJvcGRvd24gYnV0dG9uIHdoZW4gdGhlIGRyb3Bkb3duIGNvbnRlbnQgaXMgc2hvd24gKi9cbiAgLmRyb3Bkb3duOmhvdmVyIC5kcm9wYnRuIHtiYWNrZ3JvdW5kLWNvbG9yOiAjRkZFNEUxO31cblxuXG5cbi8qIGRyb3AtZG93biBtZW51ICovXG5cblxuI2J0bntcbiAgICB0b3A6IDA7XG4gICAgbGVmdDowO1xuICAgIHBvc2l0aW9uOmFic29sdXRlO1xuICAgIHdpZHRoOjExMHB4O1xuICAgIGhlaWdodDoxMDAlO1xuICAgIGJhY2tncm91bmQ6bGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAjQ0Q1QzVDLCNGRkU0QzQpO1xuICAgIGJvcmRlci1yYWRpdXM6MzBweDtcbiAgICB0cmFuc2l0aW9uOiAuNXM7XG59XG5cbi5zdWJtaXQtYnRue1xuICAgIHdpZHRoOiA0MCU7XG4gICAgcGFkZGluZzogMTBweCAzMHB4O1xuICAgIGN1cnNvcjpwb2ludGVyO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBiYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byByaWdodCwgI0NENUM1QywjRkZFNEM0KTtcbiAgICBib3JkZXI6IDA7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOjMwcHg7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuLnN1Ym1pdC1idG46aG92ZXJ7XG4gICAgYmFja2dyb3VuZDpsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM2OTY5NjksI0MwQzBDMCk7XG5cbn1cbi5zZWFyY2gtZGl2eyBcbiAgICB3aWR0aDogNTAwcHg7XG4gICAgaGVpZ2h0OiA5MDBweDtcbiAgICBwb3NpdGlvbjpyZWxhdGl2ZTtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIFxufVxuOjpwbGFjZWhvbGRlcntcbiAgICBjb2xvcjogYmlzcXVlO1xuICAgIG9wYWNpdHk6IDAuNTtcbn1cbi8qU2VhcmNoYmFyKi9cbiNzZWFyY2gtYmFye1xuICAgIG1hcmdpbi10b3A6IDgwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDUwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLnNlYXJjaCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICB0b3A6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgd2lkdGg6IDgwcHg7XG4gICAgaGVpZ2h0OiA4MHB4O1xuICAgIGJhY2tncm91bmQ6ICNDRDVDNUM7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIHRyYW5zaXRpb246IGFsbCAxcztcbiAgICB6LWluZGV4OiA0O1xuICAgIGJveC1zaGFkb3c6IDAgMCAyNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgIFxufVxuLnNlYXJjaDpob3ZlciB7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuLnNlYXJjaDo6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgdG9wOiAyMnB4O1xuICAgIHJpZ2h0OiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICBsZWZ0OiAyMnB4O1xuICAgIHdpZHRoOiAxMnB4O1xuICAgIGhlaWdodDogMnB4O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjVzO1xufVxuLnNlYXJjaDo6YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICB0b3A6IC01cHg7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGxlZnQ6IC01cHg7XG4gICAgd2lkdGg6IDI1cHg7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBib3JkZXI6IDJweCBzb2xpZCB3aGl0ZTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjVzO1xuICAgIFxufVxuXG5pbnB1dCB7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHRvcDogMDtcbiAgICByaWdodDogMDtcbiAgICBib3R0b206IDA7XG4gICAgbGVmdDogMDtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYmFja2dyb3VuZDogICNDRDVDNUM7XG4gICAgY29sb3I6IGJsYWNrO1xuICAgIHRleHQtc2hhZG93OiAwIDAgMTBweCAjQ0Q1QzVDO1xuICAgIHBhZGRpbmc6IDAgODBweCAwIDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3gtc2hhZG93OiAwIDAgMjVweCAwICNDRDVDNUMsXG4gICAgICAgICAgICAgICAgMCAyMHB4IDI1cHggMCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gICBcbiAgICB0cmFuc2l0aW9uOiBhbGwgMXM7XG4gICAgb3BhY2l0eTogMDtcbiAgICB6LWluZGV4OiA1O1xuICAgIGxldHRlci1zcGFjaW5nOiAwLjFlbTtcbn1cbmlucHV0OmhvdmVyIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG5pbnB1dDpmb2N1cyB7XG4gICAgd2lkdGg6IDI1MHB4O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgY3Vyc29yOiB0ZXh0O1xufVxuaW5wdXQ6Zm9jdXMgfiAuc2VhcmNoIHtcbiAgICBsZWZ0OiAzMDBweCA7XG4gICAgYmFja2dyb3VuZDogIzE1MTUxNTtcbiAgICB6LWluZGV4OiA2O1xufVxuaW5wdXQ6Zm9jdXMgfiAuc2VhcmNoOjpiZWZvcmUge1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAyNXB4O1xufVxuaW5wdXQ6Zm9jdXMgfiAuc2VhcmNoOjphZnRlciB7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgd2lkdGg6IDI1cHg7XG4gICAgaGVpZ2h0OiAycHg7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDAlO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG59XG4gICAgXG5pbnB1dDpmb2N1cyB+IC5zZWFyY2g6OnBsYWNlaG9sZGVyIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgb3BhY2l0eTogMC41O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi8qIC9TZWFyY2hiYXIgKi9cblxuXG4vKiAuYXtcbi8qIFNlYXJjaGJhciAqL1xuXG5cbi5sb2dvdXQge1xuICAgIC8qIGJhY2tncm91bmQ6ICAjQ0Q1QzVDOyAqL1xuICAgIHRvcDogMTFweDtcbiAgICByaWdodDogMHB4O1xuICAgIGxlZnQ6IDg3JTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgZm9udC1mYW1pbHk6IG15Rm9udDtcbn1cbi5he1xuICAgIGNvbG9yOiAjRjA4MDgwO1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIGZvbnQtc2l6ZTogMThweDtcblxufSBcblxuXG4uYTJ7XG4gICAgY29sb3I6ICM4ZjViYzA7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGZvbnQtZmFtaWx5OiBteUZvbnQ7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIG1hcmdpbi10b3A6LTEwcHg7IFxuICAgIFxufVxuLmEze1xuICAgIGNvbG9yOiAjOGY1YmMwO1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDUwMHB4O1xufVxuLyogLnVzZXJuYW1le1xuICAgIHRvcDogMnB4O1xuICAgIHJpZ2h0OiAwcHg7XG4gICAgbGVmdDogMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGNvbG9yOiAjRjA4MDgwO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBmb250LWZhbWlseTogbXlGb250O1xuICAgIFxufSAqL1xuXG5cblxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SearchComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-search',
          templateUrl: './search.component.html',
          styleUrls: ['./search.component.css'],
          providers: [_service_deezer_service__WEBPACK_IMPORTED_MODULE_1__["DeezerApi"]]
        }]
      }], function () {
        return [{
          type: _service_deezer_service__WEBPACK_IMPORTED_MODULE_1__["DeezerApi"]
        }, {
          type: _service_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/service/album.service.ts":
  /*!******************************************!*\
    !*** ./src/app/service/album.service.ts ***!
    \******************************************/

  /*! exports provided: AlbumService */

  /***/
  function srcAppServiceAlbumServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AlbumService", function () {
      return AlbumService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var AlbumService = /*#__PURE__*/function () {
      function AlbumService(http) {
        _classCallCheck(this, AlbumService);

        this.http = http;
        this.url = 'http://localhost:3003/api';
      }

      _createClass(AlbumService, [{
        key: "addComment",
        value: function addComment(id, user, comment) {
          return this.http.patch(this.url + '/album/addComment', {
            id: id,
            user: user,
            comment: comment
          });
        }
      }, {
        key: "firstComment",
        value: function firstComment(id, user, comment) {
          return this.http.post(this.url + '/album/firstComment', {
            id: id,
            user: user,
            comment: comment
          });
        }
      }, {
        key: "showComments",
        value: function showComments(id) {
          return this.http.get(this.url + '/album/' + id);
        }
      }, {
        key: "addRating",
        value: function addRating(id, stars) {
          return this.http.patch(this.url + '/album/addRating', {
            id: id,
            stars: stars
          });
        }
      }]);

      return AlbumService;
    }();

    AlbumService.ɵfac = function AlbumService_Factory(t) {
      return new (t || AlbumService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
    };

    AlbumService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: AlbumService,
      factory: AlbumService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AlbumService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/service/deezer.service.ts":
  /*!*******************************************!*\
    !*** ./src/app/service/deezer.service.ts ***!
    \*******************************************/

  /*! exports provided: DeezerApi */

  /***/
  function srcAppServiceDeezerServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DeezerApi", function () {
      return DeezerApi;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs/add/operator/map */
    "./node_modules/rxjs-compat/_esm2015/add/operator/map.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var DeezerApi = /*#__PURE__*/function () {
      function DeezerApi(http) {
        _classCallCheck(this, DeezerApi);

        this.http = http;
      }

      _createClass(DeezerApi, [{
        key: "searchMusic",
        value: function searchMusic(str) {
          var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'artist';
          var searchUrl = "https://cors-anywhere.herokuapp.com/api.deezer.com/search?q=".concat(str, "&offset=0&limit=6&type=").concat(type);
          return this.http.get(searchUrl).map(function (res) {
            return res.data;
          });
        }
      }, {
        key: "getArtist",
        value: function getArtist(id) {
          this.artistUrl = "https://cors-anywhere.herokuapp.com/api.deezer.com/artist/".concat(id);
          return this.http.get(this.artistUrl).map(function (res) {
            return res;
          });
        }
      }, {
        key: "getAlbums",
        value: function getAlbums(artistId) {
          this.albumsUrl = "https://cors-anywhere.herokuapp.com/api.deezer.com/artist/".concat(artistId, "/albums");
          return this.http.get(this.albumsUrl).map(function (res) {
            return res.data;
          });
        }
      }, {
        key: "getAlbum",
        value: function getAlbum(albumId) {
          this.albumUrl = "https://cors-anywhere.herokuapp.com/api.deezer.com/album/".concat(albumId);
          return this.http.get(this.albumUrl).map(function (res) {
            return res;
          });
        }
      }, {
        key: "getTracks",
        value: function getTracks(albumId) {
          this.tracksUrl = "https://cors-anywhere.herokuapp.com/api.deezer.com/album/".concat(albumId, "/tracks");
          return this.http.get(this.tracksUrl).map(function (res) {
            return res.data;
          });
        }
      }, {
        key: "getTrack",
        value: function getTrack(trackId) {
          this.tracksUrl = "https://cors-anywhere.herokuapp.com/api.deezer.com/track/".concat(trackId);
          return this.http.get(this.tracksUrl).map(function (res) {
            return res;
          });
        }
      }]);

      return DeezerApi;
    }();

    DeezerApi.ɵfac = function DeezerApi_Factory(t) {
      return new (t || DeezerApi)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
    };

    DeezerApi.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: DeezerApi,
      factory: DeezerApi.ɵfac
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DeezerApi, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/service/user.service.ts":
  /*!*****************************************!*\
    !*** ./src/app/service/user.service.ts ***!
    \*****************************************/

  /*! exports provided: UserService */

  /***/
  function srcAppServiceUserServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserService", function () {
      return UserService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _deezer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./deezer.service */
    "./src/app/service/deezer.service.ts");

    var UserService = /*#__PURE__*/function () {
      function UserService(http, _deezerApi) {
        _classCallCheck(this, UserService);

        this.http = http;
        this._deezerApi = _deezerApi; // Cuvamo username korisnika koji koristi aplikaciju

        this.user = "user";
        this.url = 'http://localhost:3003/api';
      }

      _createClass(UserService, [{
        key: "getUser",
        value: function getUser() {
          return this.user;
        }
      }, {
        key: "setUser",
        value: function setUser(username) {
          this.user = username;
        }
      }, {
        key: "getList",
        value: function getList() {
          return this.http.get(this.url + '/user/listTracks/' + this.user);
        }
      }, {
        key: "addToList",
        value: function addToList(track) {
          var res = this.http.patch(this.url + "/user/addToList", {
            username: this.user,
            track: track
          });
          res.subscribe(function (p) {
            return p;
          });
        }
      }, {
        key: "removeFromList",
        value: function removeFromList(listIds) {
          var res = this.http.patch(this.url + "/user/removeFromList", {
            username: this.user,
            listTracks: listIds
          });
          res.subscribe(function (p) {
            return p;
          });
        }
      }, {
        key: "addToHistory",
        value: function addToHistory(user, id) {
          var res = this.http.patch(this.url + '/user/history', {
            user: user,
            id: id
          });
          res.subscribe(function (p) {
            return p;
          });
        }
      }, {
        key: "getHistory",
        value: function getHistory(user) {
          return this.http.get(this.url + '/user/listHistory/' + user);
        }
      }]);

      return UserService;
    }();

    UserService.ɵfac = function UserService_Factory(t) {
      return new (t || UserService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_deezer_service__WEBPACK_IMPORTED_MODULE_2__["DeezerApi"]));
    };

    UserService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: UserService,
      factory: UserService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UserService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }, {
          type: _deezer_service__WEBPACK_IMPORTED_MODULE_2__["DeezerApi"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    }); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false,
      apiBaseUrl: 'http://localhost:3003/api'
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
    }

    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
      return console.error(err);
    });
    /***/

  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! /home/ivana/Documents/PZVprojekat/3/27-find_your_jam/front-end/src/main.ts */
    "./src/main.ts");
    /***/
  },

  /***/
  1:
  /*!********************!*\
    !*** ws (ignored) ***!
    \********************/

  /*! no static exports found */

  /***/
  function _(module, exports) {
    /* (ignored) */

    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map