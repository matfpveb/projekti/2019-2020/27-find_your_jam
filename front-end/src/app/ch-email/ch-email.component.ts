import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../service/user.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-ch-email',
  templateUrl: './ch-email.component.html',
  styleUrls: ['./ch-email.component.css']
})
export class ChEmailComponent implements OnInit {

  public changeForm: FormGroup;
  public user: string = null;
  public password_: boolean = true; 
  public newEmail_: boolean = true;


  constructor(
    private userService: UserService, 
    private formBuilder: FormBuilder, 
    private router: Router, 
    private http: HttpClient) {

      this.user = userService.getUser();

      this.changeForm = this.formBuilder.group({
        newEmail: ['', [Validators.required, Validators.email]], 
        password: ['', [Validators.required]]
      });
    }


  ngOnInit(): void { 
  }

  public change(data) {
     
    if(!this.changeForm.valid) {
      window.alert("Not valid!");
      return;
    }
    let d = {
      newEmail: data.newEmail,
      password: data.password,
      user:this.user
    };

    this.http.post<any>(environment.apiBaseUrl + '/change/email', d).subscribe(data => {
      
      this.changeForm.reset();
      this.router.navigateByUrl("/search");
    },
    err => {
      console.log(err.error);
      this.password_ = true;
      this.newEmail_ = true;

      if (err.error == "Email already exists") {
          this.newEmail_ = false;
      }
      if (err.error == "Password is not correct") {
          this.password_ = false;
      }
    });
  }

  

  public newEmail() {
    return this.changeForm.get('newEmail');
  }

  

}
