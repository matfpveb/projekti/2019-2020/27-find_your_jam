import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChEmailComponent } from './ch-email.component';

describe('ChEmailComponent', () => {
  let component: ChEmailComponent;
  let fixture: ComponentFixture<ChEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
