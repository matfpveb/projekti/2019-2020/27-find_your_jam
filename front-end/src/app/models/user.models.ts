export interface User{
    _id: String,
    username: String,
    email: String,
    password: String,
    lists: [{
        name: String,
        songs: Array<String>,
        private: boolean
        }],
    history: Array<String>,
    hearted: [{
        name: String,
        songs: Array<String>
        }]
};