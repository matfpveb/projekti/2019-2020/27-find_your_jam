export interface Album{
    _id: String,
    ratings:  Array<Number>,
    comments: [{
        user: String,
        comment: String
        }]
};