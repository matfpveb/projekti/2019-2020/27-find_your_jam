import { Injectable } from '@angular/core';
import { User } from '../models/user.models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DeezerApi } from './deezer.service';
import { ITrack } from '../interfaces/itrack';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  // Cuvamo username korisnika koji koristi aplikaciju

  private user: string = "user";
  private url='http://localhost:3003/api';
  private listTracks:Array<String>;
  constructor(public http:HttpClient,
              private _deezerApi: DeezerApi) { }

  public getUser(): string {
    return this.user;
  } 

  public setUser(username: string){
    this.user = username;
  }

  

public removeSongFromList(user: String, id: number, list: String){
  const res=this.http.patch<any>(this.url+"/user/removeSongFromList",{user, id, list});
  res.subscribe(p=>{
    return p;
  });
}

  public addToHistory(user:String, id: String){
    const res=this.http.patch<User>(this.url + '/user/history', {user, id}); 
    res.subscribe(p=>{
      return p;
    })
  }

  public getHistory(user: String):Observable<Array<String>>{
    return this.http.get<Array<String>>(this.url + '/user/listHistory/'+ user);
  }

  public removeHistory(user: String){
    let p = [];
    const res = this.http.patch<User>(this.url + '/user/removeHistory', {user,p});
    res.subscribe(k=>{
      return k;
    });
  }

  public getLiked(user: String):Observable<Array<String>>{
    return this.http.get<Array<String>>(this.url + '/user/likedHistory/'+ user);
  }

  public addToLiked(user: String, id: String){
    const res = this.http.patch<User>(this.url + '/user/liked', {user, id});
    res.subscribe(t => {
      return t;
    })
  } 


  public makeNewList(user: String, name: String): Observable<any>{
    return this.http.post<any>(this.url + '/makeList', {user, name});
  }

  public getLists(user: String): Observable<any>{
    return this.http.get<any>(this.url + '/getLists/' + user);
  }

  public addSongToList(user: String, id: String, list: String){
    const res = this.http.patch<User>(this.url + '/addSongToList', {user, id, list});
    res.subscribe(t => {
      return t;
    })
  }

  public changeListPrivateField(user: String, list: String){
    const res = this.http.patch<any>(this.url + '/changeListPrivateField', {user, list});
    res.subscribe(t => {
      return t;
    })
  }

  public getUsers(): Observable<Array<User>>{
    return this.http.get<Array<User>>(this.url + '/users');
  }

  public deleteList(user: String, list: String){
    const res = this.http.patch<User>(this.url + '/deleteList', {user, list});
    res.subscribe(k=>{
      return k;
    });
  }

  public deleteAccount(user: String){
    const res = this.http.delete<User>(this.url + '/deleteUser/'+ user);
    res.subscribe(k=>{
      return k;
    });
  }

  public addToHearted(user: String, list){
    const res = this.http.patch<User>(this.url + '/user/addToHearted', {user, list});
    res.subscribe(k=>{
      return k;
    });
  }

  public removeFromHearted(user: String, list){
    const res = this.http.patch<User>(this.url + '/user/removeFromHearted', {user, list});
    res.subscribe(k=>{
      return k;
    });
  }

  public getHearted(user: String): Observable<any>{
    return this.http.get<any>(this.url + '/getHearted/' + user);
  }
}
