import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Album } from '../models/album.models';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  private url='http://localhost:3003/api';
  constructor( private http: HttpClient) { }

  //dodaje komentar u listu
  public addComment(id, user, comment): Observable<Album>{
    return this.http.patch<Album>(this.url + '/album/addComment', {id, user, comment});
  }

  //uzima informacije o albumu
  public showComments(id): Observable<Album>{
    return this.http.get<Album>(this.url + '/album/' + id);
  }

  //dodaje ocenu u listu
  public addRating(id, stars): Observable<Album>{
    return this.http.patch<Album>(this.url + '/album/addRating', {id, stars});
  }

}
