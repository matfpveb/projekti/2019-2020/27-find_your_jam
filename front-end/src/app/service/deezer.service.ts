import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { IResult } from '../interfaces/iresult';
import { Observable } from 'rxjs/Observable';
import { IArtist } from '../interfaces/iartist';
import { IAlbum } from '../interfaces/ialbum';
import { ITrack } from '../interfaces/itrack';

@Injectable()
export class DeezerApi {
  

// private searchUrl = 'https://api.deezer.com/user/2529';
private artistUrl: string;
private albumsUrl: string;
private albumUrl: string;
private tracksUrl: string;

  constructor(private http: HttpClient) {
  }

  searchMusic(str: string, type = 'artist'): Observable<IResult[]> {
    const searchUrl = `https://cors-anywhere.herokuapp.com/api.deezer.com/search?q=${str}&offset=0&limit=6&type=${type}`;
    return this.http.get(searchUrl).map((res: any) => <IResult[]>res.data);
  }

  getArtist(id: string): Observable<IArtist> { //dohvatamo izvodjaca ciji je id = id
    this.artistUrl = `https://cors-anywhere.herokuapp.com/api.deezer.com/artist/${id}`;
    return this.http.get(this.artistUrl).map(res => <IArtist> res);
  }

  getAlbums(artistId: string): Observable<IAlbum[]> { //dohvatamo albume izvodjaca ciji je id = artistId
    this.albumsUrl = `https://cors-anywhere.herokuapp.com/api.deezer.com/artist/${artistId}/albums`;
    return this.http.get(this.albumsUrl)
    .map((res: any) => <IAlbum[]> res.data);
  }

  getAlbum(albumId: string): Observable<IAlbum> { //dohvatamo album ciji je id = albumId
    this.albumUrl = `https://cors-anywhere.herokuapp.com/api.deezer.com/album/${albumId}`;
    return this.http.get(this.albumUrl)
    .map((res: any) => <IAlbum> res);
  }

  getTracks(albumId: string): Observable<ITrack[]> { //dohvatamo trake iz albuma ciji je id = albumId
    this.tracksUrl = `https://cors-anywhere.herokuapp.com/api.deezer.com/album/${albumId}/tracks`;
    return this.http.get(this.tracksUrl)
    .map((res: any) => <ITrack[]> res.data);
  }

  getTrack(trackId: string): Observable<ITrack> { //dohvatamo jednu pesmu ciji je id = trackId
    this.tracksUrl = `https://cors-anywhere.herokuapp.com/api.deezer.com/track/${trackId}`;
    return this.http.get(this.tracksUrl)
    .map((res: any) => <ITrack> res);
  }

  getRelatedArtist(artistId: string): Observable<IArtist[]> { //dohvatamo slicne izvodjaca ciji je id = artistId
    this.artistUrl = `https://cors-anywhere.herokuapp.com/api.deezer.com/artist/${artistId}/related`;
    return this.http.get(this.artistUrl)
    .map((res: any) => <IArtist[]> res.data);
  }


  public getLyric(artist: string, song: string): Observable<any>{ //dohvatamo tekst pesme
     return this.http.get(`https://cors-anywhere.herokuapp.com/http://api.chartlyrics.com/apiv1.asmx/SearchLyricDirect?artist=` + artist + `&song=` + song, {responseType: 'text'});
  }

}
