import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';


@Component({
  selector: 'app-delete-account',
  templateUrl: './delete-account.component.html',
  styleUrls: ['./delete-account.component.css']
})
export class DeleteAccountComponent implements OnInit {

  public user: string = null;

  constructor(private userService:UserService) {
    this.user=this.userService.getUser();
  }

  ngOnInit(): void {}

  public deleteAccount(){
    this.userService.deleteAccount(this.user);
  }

}
