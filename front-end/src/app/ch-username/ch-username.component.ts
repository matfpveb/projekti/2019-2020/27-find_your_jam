import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../service/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-ch-username',
  templateUrl: './ch-username.component.html',
  styleUrls: ['./ch-username.component.css']
})
export class ChUsernameComponent implements OnInit {

  public changeForm: FormGroup;
  public user: string = null;
  public password_: boolean = true;
  public newUsername_: boolean = true;

  @ViewChild('newUsername', {static:false})
  private newUsernameInput: ElementRef;

  constructor(
    private userService: UserService, 
    private formBuilder: FormBuilder,  
    private router: Router, 
    private http: HttpClient) {

      this.user = userService.getUser();

      this.changeForm = this.formBuilder.group({
        newUsername: ['', [Validators.required]], 
        password: ['', [Validators.required]]
      });
    }


  ngOnInit(): void {
  }

  public change(data) {
     
    if(!this.changeForm.valid) {
      window.alert("Not valid!");
      return;
    }
    let d = {
      newUsername: data.newUsername,
      password: data.password,
      user:this.user
    };
    this.http.post<any>(environment.apiBaseUrl + '/change/username', d).subscribe(data => {
      
      const user = (this.newUsernameInput.nativeElement as HTMLInputElement).value;
      this.userService.setUser(user);
      this.router.navigateByUrl("/search");
      this.changeForm.reset();
    },
    err => {
      console.log(err.error);
      this.password_ = true;
      this.newUsername_ = true;

      if (err.error == "Username already exists") {
          this.newUsername_ = false;
      }
      if (err.error == "Password is not correct") {
          this.password_ = false;
      }
    });

  }

  public newUsername()
  {
    return this.changeForm.get('newUsername'); 
  }

  public password()
  {
    return this.changeForm.get('password'); 
  }

}
