import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChUsernameComponent } from './ch-username.component';

describe('ChUsernameComponent', () => {
  let component: ChUsernameComponent;
  let fixture: ComponentFixture<ChUsernameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChUsernameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChUsernameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
