import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {HttpModule} from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { RegisterComponent } from './register/register.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { DeezerApi } from './service/deezer.service';
import { ArtistComponent } from './artist/artist.component';
import { AlbumComponent } from './album/album.component';
import { ChUsernameComponent } from './ch-username/ch-username.component';
import { ChEmailComponent } from './ch-email/ch-email.component';
import { ChPasswordComponent } from './ch-password/ch-password.component';
import { ListTracksComponent } from './list-tracks/list-tracks.component';
import { SocketioService } from './service/socketio.service';
import { AllListsComponent } from './all-lists/all-lists.component';
import { DeleteAccountComponent } from './delete-account/delete-account.component';
import { NavigationComponent } from './navigation/navigation.component';



@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    RegisterComponent,
    NotfoundComponent,
    ArtistComponent,
    AlbumComponent,
    ChUsernameComponent,
    ChEmailComponent,
    ChPasswordComponent,
    ListTracksComponent,
    AllListsComponent,
    DeleteAccountComponent,
    NavigationComponent
    ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    RouterModule.forRoot([
      {path: '', component: RegisterComponent},
      {path: 'search', component: SearchComponent},
      {path: 'artist/:id', component: ArtistComponent},
      {path: 'album/:id', component: AlbumComponent},
      {path: 'change/username', component: ChUsernameComponent},
      //http://localhost:4200/change/email
      {path: 'change/email', component: ChEmailComponent},
      //http://localhost:4200/change/password
      {path: 'change/password', component: ChPasswordComponent}, 
      {path:'myList', component:ListTracksComponent},
      {path: 'deleteAccount', component: DeleteAccountComponent},
      {path: 'allLists', component: AllListsComponent},
      {path: '**', component: NotfoundComponent}
    ])
  ],
  providers: [DeezerApi, SocketioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
