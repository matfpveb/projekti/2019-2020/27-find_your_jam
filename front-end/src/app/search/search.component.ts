import { Component, OnInit } from '@angular/core';
import { DeezerApi } from '../service/deezer.service';
import { IResult } from '../interfaces/iresult';
import { IAlbum } from '../interfaces/ialbum';
import { UserService } from '../service/user.service';
import { Observable } from 'rxjs';
import { SocketioService } from '../service/socketio.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [DeezerApi]
})
export class SearchComponent implements OnInit {


  public searchStr: string;
  public searchRes: IResult[];
  public id: string;

  // Username trenutnog korisnika
  public user: string = null;
  public listHistory:Observable<Array<String>>;
  public alb:IAlbum[]=[];
  public show_history = true;

  public liked: IAlbum[]=[];
  public listLiked:Observable<Array<String>>;

  constructor(private _deezerApi: DeezerApi,
              private userService: UserService,
              private socketService: SocketioService) {
                this.user = userService.getUser();

                
                this.socketService.socket.on('empty', ()=>{
                  this.show_history = false;
                });
              }

              

  ngOnInit(): void { this.getHistory(); 
                     this.getLiked();}

  public getHistory(){
    this.listHistory=this.userService.getHistory(this.user);
    this.listHistory.subscribe(l=>{
      l.forEach(p=>{
        this._deezerApi.getAlbum(String(p)).subscribe(al=>{
          this.alb.push(al);
        });
      });
    });
  }

  public getLiked(){
    this.listLiked = this.userService.getLiked(this.user);
    this.listLiked.subscribe(l=>{
      l.forEach(p=>{
        this._deezerApi.getAlbum(String(p)).subscribe(al=>{
          this.liked.push(al);
          })
       })
    });
  }

  public searchMusic():void {
    this._deezerApi.searchMusic(this.searchStr)
  .subscribe(results => {
    this.searchRes = results;
  }); 
   
  }

  public deleteRecently(){
    this.socketService.socket.emit("delete");
    this.userService.removeHistory(this.user);
  }

}
