import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { AlbumComponent } from './album/album.component';
import { ArtistComponent } from './artist/artist.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { ChUsernameComponent } from './ch-username/ch-username.component';
import { ChEmailComponent } from './ch-email/ch-email.component';
import { ChPasswordComponent } from './ch-password/ch-password.component';
import { ListTracksComponent } from './list-tracks/list-tracks.component';


//Putanje
const routes: Routes = [
  //http://localhost:4200/  
  { path: '', component: RegisterComponent},
  //http://localhost:4200/search
  { path: 'search', component: SearchComponent},
  //http://localhost:4200/artist/:id
  {path: 'artist/:id', component: ArtistComponent},
  //http://localhost:4200/album/:id
  {path: 'album/:id', component: AlbumComponent},
  //http://localhost:4200/change/username
  {path: 'change/username', component: ChUsernameComponent},
  //http://localhost:4200/change/email
  {path: 'change/email', component: ChEmailComponent},
  //http://localhost:4200/change/password
  {path: 'change/password', component: ChPasswordComponent}, 
  //http://localhost:4200/myList
  {path:'myList', component: ListTracksComponent},
   //Nema rezultata pretrage
  {path: '**', component: NotfoundComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
