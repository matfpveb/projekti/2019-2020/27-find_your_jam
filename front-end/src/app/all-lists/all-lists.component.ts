import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { ITrack } from '../interfaces/itrack';
import { DeezerApi } from '../service/deezer.service';
import { Observable } from 'rxjs';
import { User } from '../models/user.models';
import { Howl, Howler } from 'howler';

@Component({
  selector: 'app-all-lists',
  templateUrl: './all-lists.component.html',
  styleUrls: ['./all-lists.component.css']
})
export class AllListsComponent implements OnInit {


  public user: string = null;
  public users:Observable<Array<User>>;
  public lists = [];
  public tracks: ITrack[] = [];
  public nameOfList: String = '';
  public nameOfLists = [];
  public heartedLists = [];


  public parser = new DOMParser();
  public xmlDoc;
  public reci;
  public lyrics;
  public show_lyrics: String [] = [];
  public artist_name = null;

  public sound=null;


  constructor(private userService:UserService,
    private _deezerApi: DeezerApi) { 
    this.user=this.userService.getUser();
  }

  ngOnInit(): void {
    this.users=this.userService.getUsers();
    this.users.subscribe(l=>{
      //izdvajamo nazive i pesme svih javnih listi iz baze
      l.forEach(p=>{
        p.lists.forEach(e=> {
          if(e.private==false){
            let list = e.name;
            let songs = e.songs;
            this.lists.push({name: list, songs: songs});
            this.nameOfLists.push(list);
          }
          if(p.username==this.user)
            p.hearted.forEach(h=>{
              this.heartedLists.push(h.name);
            });
        });
      });
    });
  }

  
  public getSongs(list: String){
    if(this.nameOfList==list){
      this.nameOfList='';
      this.tracks=[];
      if(this.sound!=null && this.sound.playing()){
        this.sound.stop();
  }
    }else{
    
      this.nameOfList = list;

      this.tracks=[];
      //uzimamo pesme odgovarajuce liste
      let songs = this.lists.filter(e=>e.name==list);
      let ids = songs.map(e=>e.songs);

      //dodajemo pesme u listu
      ids['0'].forEach(s=>{
        this._deezerApi.getTrack(String(s)).subscribe(tr=>{
          this.tracks.push(tr);
        });
      });
    }
  }


  getLyric(song: String, trackId: String, artist: String){
    
    this.artist_name = artist;

    //priprema niski za upit
    this.reci = song.split('(');
    this.reci[0].trim();
    this.reci[0] = this.reci[0].replace(/\s/g, "%20").replace("'", '').toLowerCase();

    this.artist_name = this.artist_name.replace(/\s/g, "%20").toLowerCase();

    //preuzimanje teksta pesme
    this._deezerApi.getLyric(this.artist_name, this.reci[0]).subscribe((p)=>{
    this.xmlDoc = this.parser.parseFromString(p, "text/xml");
      
    //kada se pritisne dugme prvi put prikaze se teskt
    //sledeci put se uvuce 
    if(!this.show_lyrics.includes(trackId))
      this.show_lyrics.push(trackId);
    else{
      this.show_lyrics = this.show_lyrics.filter(e => e!=trackId);
    }
    let idd = +trackId; 

    //prikaz teksta, ako postoji u bazi, u suprotnom odgovarajuce poruke
    if(this.xmlDoc.getElementsByTagName("Lyric")[0].length == 0 || this.xmlDoc.getElementsByTagName("Lyric")[0].childNodes[0] == undefined){
      this.tracks.filter(e => e.id===idd)[0].lyrics="We don't have lyrics for this song...";
    }else{ 
      this.tracks.filter(e => e.id===idd)[0].lyrics=this.xmlDoc.getElementsByTagName("Lyric")[0].childNodes[0].nodeValue;
    }

    });
  }

  //pustanje pesme
  public play(url: String){
    
    if(this.sound!=null && this.sound.playing()){
          this.sound.stop();
    }

    this.sound = new Howl({
      src: [url]
    });

    this.sound.play();
    
  }

  //lajkovanje liste
  public heartList(list: String){
    if(this.heartedLists.includes(list)){
      this.heartedLists = this.heartedLists.filter(e=>e!=list);
      this.userService.removeFromHearted(this.user, list);
    }
    else{
      let findList = this.lists.filter(e=>e.name==list)[0];
      let l = { "list":{
                "name": findList.name,
                "songs": findList.songs
              }};
      this.heartedLists.push(list);
      this.userService.addToHearted(this.user, l);
    }
  }

}
