import { Observable } from 'rxjs/Observable';
import { DeezerApi } from '../service/deezer.service';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { IArtist } from '../interfaces/iartist';
import { IResult } from '../interfaces/iresult';
import { IAlbum } from '../interfaces/ialbum';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/map';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['artist.component.css']
})
export class ArtistComponent implements OnInit {

  id: string;
  artist: IArtist;
  related_artists: IArtist[] =[];
  result: IResult;
  albums: IAlbum[] = [];
  album: IAlbum;
  loader: boolean;
  public user: string = null;

  constructor(private _deezerApi: DeezerApi, private _route: ActivatedRoute, private userService:UserService) {
    this.user=this.userService.getUser();
  }

  ngOnInit() {
    this._route.params
      .map(params => params['id'])
      .subscribe((id) => {
        this.loader = true;

        Observable.forkJoin([this._deezerApi.getArtist(id), this._deezerApi.getAlbums(id)])
        .subscribe(results => {
          this.artist = results[0];
          this.albums = results[1];
          this.loader = false;
        }, error => {
          console.log(error);
          this.loader = false;
        });

        this._deezerApi.getRelatedArtist(id)
        .subscribe(res => {
          this.related_artists = res;
        });
  });
}

public dodaj_u_istoriju(alb_id: String){

  this.userService.addToHistory(this.user, alb_id);
}

}
