import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { ITrack } from '../interfaces/itrack';
import { DeezerApi } from '../service/deezer.service';
import { Observable } from 'rxjs';
import { Howl, Howler } from 'howler';

@Component({
  selector: 'app-list-tracks',
  templateUrl: './list-tracks.component.html',
  styleUrls: ['./list-tracks.component.css']
})
export class ListTracksComponent implements OnInit {

  public user: string = null;
  public tr:ITrack[]=[];
  public tracks = [];
  public nameOfList: String='';
  public private_list: boolean;
  public priv: String;
  public songsId=[];

  public parser = new DOMParser();
  public xmlDoc;
  public reci;
  public lyrics;
  public show_lyrics: String [] = [];
  public artist_name = null;

  public newListName='';
  public pressedMakeList = false;
  public allLists = [];
  public hearted = [];
  public nameOfLists = [];
  public sound=null;


  constructor(private userService:UserService,
    private _deezerApi: DeezerApi) { 

    this.user=this.userService.getUser();
  }

  ngOnInit(): void {
    //ucitavanje svih listi pesama trenutnog korisnika
    this.userService.getLists(this.user)
    .subscribe((lists)=>{
      this.allLists=lists;
    });
    //ucitavanje lajkovanih listi
    this.userService.getHearted(this.user)
    .subscribe((lists)=>{
      this.hearted=lists;
    });
  }

  //uzimanje pesama iz izabrane liste
  public getSongs(list: String){

    this.nameOfList = list;
    
    this.tr=[];
    this.songsId=[];
    let songs = this.allLists.filter(e=>e.name==list);
    this.nameOfLists = this.allLists.map(e=>e.name);
    this.private_list = songs.map(e=>e.private)['0'];

    //postavljamo promeljivu priv u zavisnosti da li nam je lista javna ili privatna
    if(this.private_list)
      this.priv = 'private';
    else
      this.priv = 'public';
    let ids = songs.map(e=>e.songs);

    //dodajemo u listu pesme
    ids['0'].forEach(s=>{
      this._deezerApi.getTrack(String(s)).subscribe(tr=>{
        this.songsId.push(s);
        this.tr.push(tr);
      });
    });
  }

  public getSongsOfHearted(list: String){
    if(this.nameOfList==list){
      this.nameOfList='';
      this.tracks=[];
      if(this.sound!=null && this.sound.playing()){
        this.sound.stop();
  }
    }else{
    
      this.nameOfList = list;

      this.tracks=[];
      //uzimamo pesme odgovarajuce liste
      let songs = this.hearted.filter(e=>e.name==list);
      let ids = songs.map(e=>e.songs);

      //dodajemo pesme u listu
      ids['0'].forEach(s=>{
        this._deezerApi.getTrack(String(s)).subscribe(tr=>{
          this.tracks.push(tr);
        });
      });
    }
  }


  public removeTrack(id: number, list: String){
    this.songsId = this.songsId.filter(e=>e!=id);
    this.userService.removeSongFromList(this.user, id, list);
  }
    

  //pravljenje nove liste
  public newList(){
    this.newListName = (<HTMLInputElement>document.getElementById("list")).value;
    this.pressedMakeList=true;
    if(this.newListName!=''){
      this.pressedMakeList=false;     
      this.userService.makeNewList(this.user, this.newListName)
      .subscribe((u)=>this.allLists=u);
      (<HTMLInputElement>document.getElementById("list")).value='';
    }
  }

  getLyric(song: String, trackId: String, artist: String){
    
    this.artist_name = artist;

    //priprema niski za upit
    this.reci = song.split('(');
    this.reci[0] = this.reci[0].trim();
    this.reci[0] = this.reci[0].replace(/\s/g, "%20").replace("'", '').toLowerCase();

    this.artist_name = this.artist_name.replace(/\s/g, "%20").toLowerCase();

    //preuzimanje teksta pesme
    this._deezerApi.getLyric(this.artist_name, this.reci[0]).subscribe((p)=>{
    this.xmlDoc = this.parser.parseFromString(p, "text/xml");
      
    //kada se pritisne dugme prvi put prikaze se teskt
    //sledeci put se uvuce 
    if(!this.show_lyrics.includes(trackId))
      this.show_lyrics.push(trackId);
    else{
      this.show_lyrics = this.show_lyrics.filter(e => e!=trackId);
    }
    let idd = +trackId; 

    //prikaz teksta, ako postoji u bazi, u suprotnom odgovarajuce poruke
    if(this.xmlDoc.getElementsByTagName("Lyric")[0].length == 0 || this.xmlDoc.getElementsByTagName("Lyric")[0].childNodes[0] == undefined){
      this.tr.filter(e => e.id===idd)[0].lyrics="We don't have lyrics for this song...";
    }else{ 
      this.tr.filter(e => e.id===idd)[0].lyrics=this.xmlDoc.getElementsByTagName("Lyric")[0].childNodes[0].nodeValue;
    }

    });

  }

  public changePrivateField(){
    
    //pritiskom na dugme menjamo da li je lista privatna ili javna
    //u zavisnosti od toga lista ce biti ili nece biti vidljiva na strani All lists
    this.userService.changeListPrivateField(this.user, this.nameOfList);
    console.log('a');
    if(this.private_list){
      document.getElementById("private").innerHTML = 'public';
      this.private_list=false;
    }else{
     document.getElementById("private").innerHTML = 'private';
     this.private_list=true;
    }
    
  }

  //pustamo pesmu
  public play(url: String){
    if(this.sound!=null && this.sound.playing()){
          this.sound.stop();
    }

    this.sound = new Howl({
      src: [url]
    });
    this.sound.play();
  }

  //brisanje liste i izbacivanje iz lista za prikazivanje
  public deleteList(list: String){

    this.allLists = this.allLists.filter(e=>e.name!=list);
    this.nameOfLists = this.nameOfLists.filter(e=>e!=list);
    this.userService.deleteList(this.user, list);
  }


}
