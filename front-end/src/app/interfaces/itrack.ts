import { IArtist } from './iartist';
import { IAlbum } from './ialbum';

export interface ITrack {
    id: number;
    title: string;
    artist: IArtist;
    album: IAlbum;
    link: string;
    duration: number;
    track_position: string;
    preview: string;
    lyrics: string;
}
  