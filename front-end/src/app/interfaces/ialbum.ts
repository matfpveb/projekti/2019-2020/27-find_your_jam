//Sadrzaj jednog albuma

import { IArtist } from './iartist';

export interface IAlbum {
    artist: IArtist;
    id: number;
    title: string;
    cover: string;
    cover_small: string;
    cover_medium: string;
    cover_big: string;
    cover_xl: string;
    link: string;
    nb_tracks: string;
    label: string;
    duration: number;
    fans: string;
    rating: string;
    release_date: string;
}
  
  