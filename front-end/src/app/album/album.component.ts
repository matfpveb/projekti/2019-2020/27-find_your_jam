import { ITrack } from './../interfaces/itrack';
import { IArtist } from '../interfaces/iartist';
import { Component, OnInit } from '@angular/core';
import { DeezerApi } from '../service/deezer.service';
import { ActivatedRoute } from '@angular/router';
import { IAlbum } from '../interfaces/ialbum';
import { UserService } from '../service/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlbumService } from '../service/album.service';
import { Album } from '../models/album.models'
import { map, switchMap } from 'rxjs/operators';
import { SocketioService } from '../service/socketio.service';
import { Howl, Howler } from 'howler';

//Prikaz albuma

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['album.component.css']
})
export class AlbumComponent implements OnInit {

  id: String;
  album: IAlbum;
  artist: IArtist;
  tracks: ITrack[] = [];
  loader: boolean;
  objectkeys = Object.keys;

  public user: string;
  public alb: Album = null;
  public alb3: Album;
  public alb4: Album;
  public stars: number;
  public rating: any = 0;
  public score: number;
  public parser = new DOMParser();
  public xmlDoc;
  public artist_name = null;
  public reci;
  public lyrics;
  public show_lyrics: String [] = [];
  public myLists = [];

  public sound=null;

  public forma: FormGroup;
  constructor(private _deezerApi: DeezerApi, private _route: ActivatedRoute,
    private userService: UserService, private albumService: AlbumService,
    private builder: FormBuilder, private socketService: SocketioService) {
      
      this.user = userService.getUser(); 

      this.forma = this.builder.group({
        comment: ['', [Validators.required, Validators.maxLength(100)]]
      });

      this.socketService.socket.on('newCom', () =>{
        this.getCom();
      });

     }

  ngOnInit() {
    this._route.params
      .map(params => params['id'])
      .subscribe((id) => {
        this.loader = true;

        this._deezerApi.getArtist(id)
            .subscribe(artist => {
              this.artist = artist;
              this.loader = false;
            });

            this._deezerApi.getTracks(id)
            .subscribe(tracks => {
              this.tracks = tracks;
              this.loader = false;

            });

        this._deezerApi.getAlbum(id)
            .subscribe(album => {
              this.album = album;
              this.loader = false;
              this.artist_name = this.album.artist.name;
            }, error => {
              this.loader = false;
              console.error(error);
            });
          });

          

          this.getCom();
          //pratimo da li je server emitovao signal za novi komentar,
          //ako jeste opet ucitavamo komentare
          this.socketService.socket.on('newCom', () =>{
            this.getCom();
          });

          this.getScore();
          //pratimo da li je server emitovao signal da se promenila ocena
          //ako jeste ucitavamo novu ocenu
          this.socketService.socket.on('newScore', () =>{
            this.getScore();
          });


    this.userService.getLists(this.user)
    .subscribe((lists)=>this.myLists=lists);

  }

  public hack(val) {
    return Array.from(val);
  }

  public rate(event: Event){
    //ucitavamo ocenu korisnika
    this.stars =  +(<HTMLInputElement>document.querySelector('input[name="rating"]:checked')).value;
    if(this.stars == 4 || this.stars == 5 )
      this.userService.addToLiked(this.user, new String(this.album.id));
      

    //saljemo signal serveru da je dodata ocena
    this.socketService.socket.emit('addRate');

    //saljemo serveru ocenu kako bi je dodao na listu
    const sub = this._route.paramMap
      .pipe(
      map((param)=>param.get('id')),
      switchMap((pId)=>(this.albumService.addRating(pId, this.stars)))
    ).subscribe((alb4)=>(this.alb4=alb4));
    
    //ispisujemo ocenu
    this.getScore();
  }

  public setCom(){
    
      if(!this.forma.valid){
        window.alert("Comment is not valid!");
        return;
      }

      //emitujemo signal serveru da je doslo do dodavanja komentara
      this.socketService.socket.emit('addCom');

      //dodajemo komentar
        const sub = this._route.paramMap
        .pipe(
        map((param)=>param.get('id')),
        switchMap((pId)=>(this.albumService.addComment(pId, this.user, this.forma.get('comment').value)))
      ).subscribe((alb3)=>(this.alb3=alb3)); 
      

    
    this.forma.reset();
    }

  public getCom(){

    //dohvatanje komentara
    const sub = this._route.paramMap
    .pipe(
    map((param)=>param.get('id')),
    switchMap((pId)=>(this.albumService.showComments(pId)))
  ).subscribe((alb)=>{
    this.alb=alb; 
  });

  }


  public getScore(){

    //dohvatanje liste ocena i racunanje proseka
    const sub = this._route.paramMap
    .pipe(
    map((param)=>param.get('id')),
    switchMap((pId)=>(this.albumService.showComments(pId)))
  ).subscribe((alb)=>{
    this.alb=alb; 
    if(this.alb!==null){
      this.alb.ratings.forEach(element => {
        this.rating= this.rating + element;
      });
      this.score = +(Math.round(this.rating/this.alb.ratings.length * 100) / 100).toFixed(2);
      this.rating=0;
    }
  });
  }


  getLyric(song: String, trackId: String){
    
    //priprema niski za upit
    this.reci = song.split('(');
    this.reci[0].trim();
    this.reci[0] = this.reci[0].replace(/\s/g, "%20").replace("'", '').toLowerCase();

    this.artist_name = this.artist_name.replace(/\s/g, "%20").toLowerCase();

    //preuzimanje teksta pesme
    this._deezerApi.getLyric(this.artist_name, this.reci[0]).subscribe((p)=>{
    this.xmlDoc = this.parser.parseFromString(p, "text/xml");
      
    //kada se pritisne dugme prvi put prikaze se teskt
    //sledeci put se uvuce 
    if(!this.show_lyrics.includes(trackId))
      this.show_lyrics.push(trackId);
    else{
      this.show_lyrics = this.show_lyrics.filter(e => e!=trackId);
    }
    let idd = +trackId; 

    //prikaz teksta, ako postoji u bazi, u suprotnom odgovarajuce poruke
    if(this.xmlDoc.getElementsByTagName("Lyric")[0].length == 0 || this.xmlDoc.getElementsByTagName("Lyric")[0].childNodes[0] == undefined){
      this.tracks.filter(e => e.id===idd)[0].lyrics="We don't have lyrics for this song...";
    }else{ 
      this.tracks.filter(e => e.id===idd)[0].lyrics=this.xmlDoc.getElementsByTagName("Lyric")[0].childNodes[0].nodeValue;
    }

    });

  }

  //dodavanje pesme u odgovarajucu listu
  public addSongToList(id: String, list: String){
    this.userService.addSongToList(this.user, id, list);
  }


  public play(url: String){
    
    //ako se neka pesma pusta zaustavljamo je
    if(this.sound!=null && this.sound.playing()){
          this.sound.stop();
    }

    //pustamo novu pesmu
    this.sound = new Howl({
      src: [url]
    });

    this.sound.play();
  }

}









