import { Component, OnInit, Injectable, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { UserService } from '../service/user.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

@Injectable()
export class RegisterComponent implements OnInit {

  title = 'Find your Jam';
  public logInForm: FormGroup;
  public signUpForm: FormGroup;

  public signUpEmail = true;
  public signUpUsername = true;
  public logInUsername = true;
  public logInPassword = true;
  public user: string = null;

  @ViewChild('userSign', {static:false})
  private userSignInput: ElementRef;

  @ViewChild('userLog', {static:false})
  private userLogInput: ElementRef;

  ngOnInit(): void {}
  
  constructor(
    private formBuilder: FormBuilder, 
    private router: Router,
    private http : HttpClient,
    private userService: UserService
    ) {

    this.logInForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

    this.signUpForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });

    }

  public signup():void {
    let x=document.getElementById("login");
    let y=document.getElementById("signup");
    let z=document.getElementById("btn");
  
    x.style.left = "-400px";
    y.style.left = "50px";
    z.style.left = "110px";
  }
  public login(): void{
    let x=document.getElementById("login");
    let y=document.getElementById("signup");
    let z=document.getElementById("btn");
  
    x.style.left = "50px";
    y.style.left = "450px";
    z.style.left = "0";
  }

  public submitSign(data){

    if(!this.signUpForm.valid) {
      window.alert("Not valid!");
      return;
    }

    this.http.post<any>(environment.apiBaseUrl + '/register', data).subscribe(data => {
      
      const user = (this.userSignInput.nativeElement as HTMLInputElement).value;
      this.userService.setUser(user);
      this.router.navigateByUrl("/search");
      this.signUpForm.reset();
    },
    err => {
      console.log(err.error);
      this.signUpEmail = true;
      this.signUpUsername = true;

      if (err.error == "Email already exists") {
          this.signUpEmail = false;
      }
      if (err.error == "Username already exists") {
          this.signUpUsername = false;
      }
    });
  }


  public submitLogin(data){
    if (!this.logInForm.valid) {
      window.alert( "Not valid!" ); 
      return;
    }

    this.http.post<any>(environment.apiBaseUrl + '/login', data).subscribe(data => {
      
      const user = (this.userLogInput.nativeElement as HTMLInputElement).value;
      this.userService.setUser(user);
      this.router.navigateByUrl("/search");
      this.logInForm.reset();
    },
    err => {
      console.log(err.error);
      this.logInUsername = true;
      this.logInPassword = true;

      if (err.error == "Incorrect username") {
          this.logInUsername = false;
      }
      if (err.error == "Password is not correct") {
          this.logInPassword = false;
      }
      
      });
  }  
  

  public logUsername(){
    return this.logInForm.get('username');
  }

  public logPassword(){
    return this.logInForm.get('password');
  }

  public signUsername(){
    return this.signUpForm.get('username');
  }

  public signEmail(){
    return this.signUpForm.get('email');
  } 

  public signPassword(){
    return this.signUpForm.get('password');
  }




}
