import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SocketioService } from './service/socketio.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  title = 'Find your Jam';

  constructor(private socketService: SocketioService){}

  ngOnInit() {
    this.socketService.setupSocketConnection();
  }

  
}
