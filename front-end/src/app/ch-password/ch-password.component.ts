import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-ch-password',
  templateUrl: './ch-password.component.html',
  styleUrls: ['./ch-password.component.css']
})
export class ChPasswordComponent implements OnInit {

  public changeForm: FormGroup;
  public user: string = null;
  public newPassword_: boolean = true;
  public oldPassword_: boolean = true;

  constructor(
    private userService: UserService, 
    private formBuilder: FormBuilder, 
    private router: Router, 
    private http: HttpClient) { 

      this.user = userService.getUser();

      this.changeForm = this.formBuilder.group({
        newPassword: ['', [Validators.required, Validators.minLength(5)]], 
        oldPassword: ['', [Validators.required]]
      });
    }

  ngOnInit(): void {
  }

  public change(data) {
   
    if(!this.changeForm.valid) {
      window.alert("Not valid!");
      return;
    }
    let d = {
      newPassword: data.newPassword,
      password: data.oldPassword,
      user:this.user
    };

    this.http.post<any>(environment.apiBaseUrl + '/change/password', d).subscribe(data => {
      
      this.router.navigateByUrl("/search");
      this.changeForm.reset();
    },
    err => {
      console.log(err.error);
      this.newPassword_ = true;
      this.oldPassword_ = true;

      if (err.error == "Password is not correct") {
          this.oldPassword_ = false;
      }
    });

 }

  public oldPassword(){
    return this.changeForm.get('oldPassword');
  }

  public newPassword(){
    return this.changeForm.get('newPassword');
  }

}
