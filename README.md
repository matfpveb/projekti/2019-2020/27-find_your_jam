# Project 27-find_your_jam :musical_note:

Find your jam je sajt sa muzikom koji omogućava korisnicima
da naprave svoj nalog za logovanje kako bi mogli da pretražuju i otkrivaju novu muziku.
Korisnik ima mogućnost da ocenjuje albume izvođača, odsluša deo pesme, pogleda tekst pesme,
napravi svoje liste pesama, pogleda srodne izvođače, dodaje komentare i slično.
Takođe korisnik ima mogućnost da menja korisničko ime, šifru i e-mail koji koristi, kao i da
obriše svoj nalog.
Postoji mogućnost i pregleda albuma na Deezer-u

# How to clone project

git clone https://gitlab.com/matfpveb/projekti/2019-2020/27-find_your_jam.git

# Run client
Pozicionirati se u folder front-end i 
pokrenuti klijentsku stranu komandom: `ng serve`

# Run server 
Pozicionirati se u folder back-end i 
pokrenuti server komandom: `node server.js`

## Developers

- [Anđela Janošević, 315/2016](https://gitlab.com/andj97)
- [Ivana Ivanović, 120/2016](https://gitlab.com/ivanaivanovic)
- [Ivana Cvetkoski, 65/2016](https://gitlab.com/ivanacvetkoski)
- [Katarina Savičić, 261/2016](https://gitlab.com/savicick)
